USE `care_works`;

-- -----------------------------------------------------
-- Data for table `care_works`.`school_category`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;

alter table location drop primary key;
alter table medium drop primary key;
alter table school_category drop primary key;
alter table school_type drop primary key;

alter table medium
    add seq bigint auto_increment primary key;

alter table location
    add seq bigint auto_increment primary key;

alter table school_category
    add seq bigint auto_increment primary key;

alter table school_type
    add seq bigint auto_increment primary key;
