USE `care_works`;

-- -----------------------------------------------------
-- Data for table `care_works`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;

alter table section_info
    drop foreign key section_info_medium_medium_fk;

alter table school_profile
    add `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP;

DELIMITER $$
CREATE TRIGGER after_insert_school_class
    AFTER INSERT
    ON school_profile
    FOR EACH ROW
BEGIN
    INSERT INTO section_info (`standard`, `school_id`, `section_name`)
    VALUES ('', NEW.school_id, '');

    INSERT INTO school_club_details (contact_number, formation_year, total_members,
                                     school_id, spoc_name, name_of_the_club)
    VALUES (null, null, 0, NEW.school_id, '', '');

END$$

DELIMITER ;

DELIMITER $$
CREATE TRIGGER after_insert_school_class_student
    AFTER INSERT
    ON section_info
    FOR EACH ROW
BEGIN
    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Boys', 'SC', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Girls', 'SC', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Boys', 'ST', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Girls', 'ST', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Boys', 'Minorities', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Girls', 'Minorities', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Boys', 'Other', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Girls', 'Other', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Boys', 'CWSN', 0, NEW.section_id);

    INSERT INTO student_info (`gender`, `social_division`, `student_count`, `section_id`)
    VALUES ('Girls', 'CWSN', 0, NEW.section_id);

END$$

DELIMITER ;
