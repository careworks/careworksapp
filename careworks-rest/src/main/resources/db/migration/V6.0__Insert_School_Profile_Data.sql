USE `care_works`;

-- -----------------------------------------------------
-- Data for table `care_works`.`school_category`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;

INSERT INTO care_works.school_type (type, description)
VALUES ('Boys', 'Boys School');
INSERT INTO care_works.school_type (type, description)
VALUES ('Co-education', '');
INSERT INTO care_works.school_type (type, description)
VALUES ('Girls', 'Girls School');

INSERT INTO care_works.medium (medium, description)
VALUES ('English', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Kannada', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Tamil', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Telugu', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Urdu', null);

INSERT INTO care_works.school_category (category, description)
VALUES ('GHPS', 'Govt. Higher Primary School (1-7/8th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GHS', 'Govt. High School (8-10th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GLPS', 'Govt. Lower Primary School (1-5th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GMPS', 'Govt. Model Primary School (1-7/8th)');

INSERT INTO care_works.location (location, description)
VALUES ('Rural', 'Rural Village');
INSERT INTO care_works.location (location, description)
VALUES ('Urban', 'Urban City');
INSERT INTO care_works.location (location, description)
VALUES ('Tribal', 'Tribal');

create table school_profile_address
(
    address_id     bigint auto_increment
        primary key,
    address_line1  text   null,
    street_address text   null,
    address_line2  text   null,
    city           text   null,
    state          text   null,
    postal_code    text   null,
    country        text   null,
    school_id      bigint null,
    constraint school_profile_address_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);

create table school_club_details
(
    school_club_details_id bigint auto_increment PRIMARY KEY,
    contact_number         text        null,
    formation_year         varchar(10) null,
    total_members          bigint      null,
    school_id              bigint      not null,
    spoc_name              text        null,
    name_of_the_club       text        null,
    constraint school_club_details_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);

alter table teacher_details
    drop column gender;

alter table teacher_details
    add male bigint default 0 null;

alter table teacher_details
    add female bigint default 0 null;

DELIMITER $$
CREATE TRIGGER after_insert_school
    AFTER INSERT
    ON school_profile
    FOR EACH ROW
BEGIN

    INSERT INTO teacher_details (employment_type, subject, teacher_count, school_id, male,
                                 female)
    VALUES ('Total Full Time Teachers', '', 0, NEW.school_id, 0, 0);
    INSERT INTO teacher_details (employment_type, subject, teacher_count, school_id, male,
                                 female)
    VALUES ('Part Time Teachers', '', 0, NEW.school_id, 0, 0);
    INSERT INTO teacher_details (employment_type, subject, teacher_count, school_id, male,
                                 female)
    VALUES ('Volunteer Teachers', '', 0, NEW.school_id, 0, 0);
    INSERT INTO teacher_details (employment_type, subject, teacher_count, school_id, male,
                                 female)
    VALUES ('Non-Teaching Staff', '', 0, NEW.school_id, 0, 0);

    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'HM', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'In charge HM', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'CRP', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'BRP', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'ECO', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'BRC', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'BEO / CEO', '', '', NEW.school_id);
    INSERT INTO school_education_department (contact_details, designation, email_id,
                                             name, school_id)
    VALUES (null, 'DyPC', '', '', NEW.school_id);

    INSERT INTO stakeholder_info (contact_number, formation_year, member_count, school_id, spoc_name,
                                  stake_holder_name)
    VALUES (null, null, 0, NEW.school_id, '', 'SDMC/SMC/SBC');
    INSERT INTO stakeholder_info (contact_number, formation_year, member_count, school_id, spoc_name,
                                  stake_holder_name)
    VALUES (null, null, 0, NEW.school_id, '', 'Alumni Association');
    INSERT INTO stakeholder_info (contact_number, formation_year, member_count, school_id, spoc_name,
                                  stake_holder_name)
    VALUES (null, null, 0, NEW.school_id, '', 'Parents Teachers Association');
    INSERT INTO stakeholder_info (contact_number, formation_year, member_count, school_id, spoc_name,
                                  stake_holder_name)
    VALUES (null, null, 0, NEW.school_id, '', 'CPC');
END$$

DELIMITER ;
