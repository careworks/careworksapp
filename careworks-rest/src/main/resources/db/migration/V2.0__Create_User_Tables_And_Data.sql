CREATE TABLE `roles`
(
    `id`          bigint      NOT NULL AUTO_INCREMENT,
    `name`        varchar(60) NOT NULL,
    `description` varchar(200),
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_roles_name` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
;

CREATE TABLE `users`
(
    `id`         bigint       NOT NULL AUTO_INCREMENT,
    `name`       varchar(40)  NOT NULL,
    `username`   varchar(15)  NOT NULL,
    `email`      varchar(40)  NOT NULL,
    `password`   varchar(100) NOT NULL,
    `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_users_username` (`username`),
    UNIQUE KEY `uk_users_email` (`email`)
) ENGINE = InnoDB
;

CREATE TABLE `user_roles`
(
    `user_id` bigint NOT NULL,
    `role_id` bigint NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`),
    KEY `fk_user_roles_role_id` (`role_id`),
    CONSTRAINT `fk_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
    CONSTRAINT `fk_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
;

INSERT IGNORE INTO roles(name) VALUES ('ROLE_USER');
INSERT IGNORE INTO roles(name) VALUES ('ROLE_ADMIN');