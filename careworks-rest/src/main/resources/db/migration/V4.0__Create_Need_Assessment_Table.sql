CREATE TABLE `care_works`.`form_fields` (
    `SEQ` BIGINT NOT NULL primary key AUTO_INCREMENT,
    `CATEGORY` VARCHAR(20) NOT NULL,
    `SUB_CATEGORY` VARCHAR(100),
    `FIELD_SECTION` VARCHAR(100),
    `FIELD_SECTION_HEADER` VARCHAR(100),
    `FIELD_ID` VARCHAR(100) NOT NULL,
    `FIELD_LABEL` VARCHAR(500) NOT NULL,
    `FIELD_TYPE` VARCHAR(50) NOT NULL,
    `FIELD_VALIDATION` VARCHAR(100),
    `FIELD_REQUIRED` VARCHAR(10),
    `PARENT_FIELD` VARCHAR(100),
    `CHILD_SEQ` BIGINT,
    `FIELD_OPTIONS` BLOB,
    `IS_ACTIVE` VARCHAR(10) default 'Y'
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `care_works`.`need_assessment_response` (
    `SCHOOL_ID` BIGINT NOT NULL,
    `FIELD_ID` VARCHAR(100),
    `FIELD_VALUE` VARCHAR(500),
    `CATEGORY` VARCHAR(20) NOT NULL
)
ENGINE = InnoDB;