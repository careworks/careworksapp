USE `care_works`;

create table quotation
(
    quot_id             bigint auto_increment
        primary key,
    updated_at          datetime       null,
    bo_q_no             text           null,
    action_id           bigint         null,
    amount              decimal(19, 3) null,
    breadth             decimal(19, 3) null,
    company_name        text           null,
    depth               decimal(19, 3) null,
    description_of_work text           null,
    length              decimal(19, 3) null,
    number              decimal(19, 3) null,
    quantity            decimal(19, 3) null,
    rate                decimal(19, 3) null,
    school_id           bigint         null,
    status              text           null,
    unit                text           null
);

create table codes
(
    seq         bigint auto_increment primary key,
    type        varchar(10) not null,
    code        varchar(10) not null,
    description text
);

insert into care_works.codes (seq, type, code, description)
values (1, 'QT_UNIT', 'NA', 'Not Applicable');
insert into care_works.codes (seq, type, code, description)
values (2, 'QT_UNIT', 'RFT', 'RFT');
insert into care_works.codes (seq, type, code, description)
values (3, 'QT_UNIT', 'Set', 'Set');
insert into care_works.codes (seq, type, code, description)
values (4, 'QT_UNIT', 'SFT', 'SFT');
insert into care_works.codes (seq, type, code, description)
values (5, 'QT_UNIT', 'L/S', 'L/S');
insert into care_works.codes (seq, type, code, description)
values (6, 'QT_UNIT', 'Nos', 'Nos');
insert into care_works.codes (seq, type, code, description)
values (7, 'QT_UNIT', 'Kg', 'Kg');
insert into care_works.codes (seq, type, code, description)
values (8, 'QT_UNIT', 'CFT', 'CFT');