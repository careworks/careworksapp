create table location
(
    location    varchar(25) not null PRIMARY KEY,
    description text        null
);


create table medium
(
    medium      varchar(25) not null
        primary key,
    description text        null
);

create table school_category
(
    category    varchar(25) not null
        primary key,
    description text        null
);

create table school_type
(
    type        varchar(25) not null
        primary key,
    description text        null
);

create table school_profile
(
    school_id             bigint auto_increment PRIMARY KEY,
    school_name           text                   null,
    established_date      date                   not null,
    location              text            null,
    school_managed_by     text                   null,
    school_cluster        text                   null,
    block                 text                   null,
    mediums               text                   null,
    category              text            null,
    type                  text            null,
    dise_code             text            null,
    email_id              text                   null,
    student_count         bigint                 not null,
    na_flag               varchar(1) default 'N' not null,
    na_action_date        date                   null,
    any_other_information text                   null
);

alter table school_profile
    add school_status varchar(255) default 'CREATED' null;

create table section_info
(
    section_id   bigint auto_increment PRIMARY KEY,
    standard     varchar(25) null,
    school_id    bigint      not null,
    section_name varchar(25) null,
    medium       varchar(25) null,
    constraint section_info_section_id_uindex
        unique (section_id),
    constraint section_info___fk2
        foreign key (school_id) references school_profile (school_id),
    constraint section_info_medium_medium_fk
        foreign key (medium) references medium (medium)
);

create table student_info
(
    student_details_id bigint auto_increment PRIMARY KEY,
    gender             varchar(25)      null,
    social_division    varchar(25)      null,
    student_count      bigint default 0 not null,
    section_id         bigint           null
);

create unique index student_info_student_details_id_uindex
    on student_info (student_details_id);

alter table student_info
    add constraint student_info___fk2
        foreign key (section_id) references section_info (section_id)
            on update cascade on delete cascade;

create table school_education_department
(
    education_department_id bigint auto_increment PRIMARY KEY,
    contact_details         text   null,
    designation             text   null,
    email_id                text   null,
    name                    text   null,
    school_id               bigint not null
);

alter table school_education_department
    add constraint school_education_department_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade;

create table stakeholder_info
(
    id                bigint auto_increment PRIMARY KEY,
    contact_number    text        null,
    formation_year    varchar(4) null,
    member_count      bigint      null,
    school_id         bigint      not null,
    spoc_name         text        null,
    stake_holder_name text        null,
    constraint stakeholder_info_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);

create table teacher_details
(
    teacher_group_id bigint auto_increment PRIMARY KEY,
    employment_type  text        null,
    gender           varchar(25) null,
    subject          text        null,
    teacher_count    bigint      null,
    school_id        bigint      not null,
    constraint teacher_details_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);


