-- Changes as asked by Tanveer (Careworks) on  28th May 2020
USE `care_works`;

-- -----------------------------------------------------
-- Create table `care_works`.`category_master`
-- -----------------------------------------------------
create table category_master
(
    category_id bigint not null auto_increment,
    category_details varchar(255),
    primary key (category_id)
);

-- -----------------------------------------------------
-- Data for table `care_works`.`category_master`
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `care_works`.`category_master` (`category_id`, `category_details`) VALUES ('1',  'School Upgradation');
INSERT INTO `care_works`.`category_master` (`category_id`, `category_details`) VALUES ('2',  'Student Enrichment');
INSERT INTO `care_works`.`category_master` (`category_id`, `category_details`) VALUES ('3',  'Health & Wellbeing');
INSERT INTO `care_works`.`category_master` (`category_id`, `category_details`) VALUES ('4',  'Teachers Mentoring');
INSERT INTO `care_works`.`category_master` (`category_id`, `category_details`) VALUES ('5',  'Stakeholder Engagement');

COMMIT;


-- -----------------------------------------------------
-- Create table `care_works`.`activity_master`
-- -----------------------------------------------------
create table activity_master
(
    activity_id bigint not null auto_increment,
    activity_detail varchar(255),
    activity_remarks varchar(255),
    category_id bigint not null,
    primary key (activity_id)
);

-- -----------------------------------------------------
-- Data for table `care_works`.`activity_master`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;

--'School Upgradation'

INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Toilets & Urinal Renovation / New Toilet & Urinal Construction');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Hand wash area');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Electrification and plumbing');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Compound wall, fencing & Main gate');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Retro fitting');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'UG Sump, Bore well & Overhead tank (OHT)');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Internal & external painting');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Art work');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Water proofing');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Light proof');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Syntax Tank relocation and plumbing');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Roof Repair');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Safety Grill');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Windows and Door Repair');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'School Corridor Repair');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Drainage Repair');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Increasing Height of Compound');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Black Board Painting');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Gate Repair');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Gardening');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Cleaning of water tank');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Green Board Fixing');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Lunch Hall Construction');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Office room & staffroom set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Nalikali classroom set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Classroom set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Wall Art Work');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Blackboard Repair/Replacement');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Furniture Requirement');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Dust Bin for every class room, dry waste and wet waste dustbins');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Ayah for cleaning and provide cleaning materials');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Construction of Auditorium');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Complaint and Suggestion Box');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'School Signage');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Class Room Signage');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Fencing and Road to Play Ground');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'CC TV');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Mike and Speaker set');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('1', 'Rain Water Harvesting');

--'Student Enrichment'

INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Library infra set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Computer Lab infra set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Science Lab infra set up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Projector');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Strengthening School Parliament');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Sports Materials Requirements');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Life Skill for Students - Physical Health');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Appointing a PT teacher');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'School Bag and Note Book');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Band set and Uniform');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Tie, Belt and ID Card');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Computer Lab Furniture');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'New computers for Computer lab');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Computer Lab maintenance');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Computer Teacher');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Improve reading Skills');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Study Skills');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Reproductive Health');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Positive Mental Health');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Scholarship');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Career Guidance Program');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Talent Development Program');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Anganawadi Learning Program');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('2', 'Social Health');

-- 'Health & Wellbeing'

INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Drinking water facility');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Health check-up and follow up (Dental and Eye)');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'General check-up');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'First Aid Box and medicine');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Training on hygeine and sanitation');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Medicines distributed by DOPI');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'School mental health - School mental health - Psychosocial care');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Face Mask for Maids');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'WASH awareness/Health education');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'HK Staff & Training');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Health Kit');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Masks & Gloves');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Thermal Screening facility');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Groceries Distribution');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('3', 'Meal Pockets Distribution');

-- 'Teachers Mentoring'

INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Teaching Aids');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Teachers training - Computer');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Teachers training - English');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'School Mapping');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Stress Management & Counselling for Teachers');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Vision Building');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('4', 'Subject Matter Specialists');

-- 'Stakeholder Engagement'

INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('5', 'Strengthening SDMC');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('5', 'Parents meeting');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('5', 'Livelihood Program');
INSERT INTO `care_works`.`activity_master` (`category_id`, `activity_detail`) VALUES ('5', 'Volunteering & Fundraising');


COMMIT;

