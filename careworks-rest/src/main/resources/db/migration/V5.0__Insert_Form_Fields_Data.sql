-- -----------------------------------------------------
-- Data for table `care_works`.`form_fields`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FIRST_AID_BOX','Is there a first aid box in the school?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'MEDS_AVAILABLE','Is Medicines Available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'HEALTH_SCREENING','Comprehensive Health Screening',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'SCREENING_ORG','Yes Mention the Organization Name      ',null,null,'N','HEALTH_SCREENING','textbox',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'HEALTH_TREATMENT','Comprehensive Health Treatment ',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'TREATMENT_ORG','Yes Mention the Organization Name       ',null,null,'N','HEALTH_TREATMENT','textbox',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Select the Medicines Supplied by Health Departments','VitaminA','VITAMINA','Select the Medicines Supplied by Health Departments ','VitaminA',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Select the Medicines Supplied by Health Departments','Deworming','DEWORMING','Select the Medicines Supplied by Health Departments','Deworming',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Select the Medicines Supplied by Health Departments','Vaccination','VACCINATION','Select the Medicines Supplied by Health Departments','Vaccination',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Select the Medicines Supplied by Health Departments',null,'MED_SUPPLY_OTHER_OPT','Select the Medicines Supplied by Health Departments','Other Specify',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Select the Medicines Supplied by Health Departments',null,'MED_SUPPLIED_OTHER','Other Specify       ',null,null,'N','MED_SUPPLY_OTHER_OPT','textbox',4);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Health Related Awareness Program conducted in School? ','Wash Program','WP','Health Related Awareness Program conducted in School?','Wash Program',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Health Related Awareness Program conducted in School? ','Hand Wash Day','HWD','Health Related Awareness Program conducted in School?','Hand wash Day',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Health Related Awareness Program conducted in School? ','Other Specify','AW_PROG_OTHER_OPT','Health Related Awareness Program conducted in School?','Other Specify',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Health Related Awareness Program conducted in School? ',null,'AW_PROG_OTHER','Mention Please',null,null,'N','AW_PROG_OTHER_OPT','textbox',2);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'HAND_WASH','Is there a Hand wash area?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'RENOVATION_REQD','If yes, is renovation required?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','HAND_WASH','radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'WASH_PRACTICE','Is there a Hand wash Practice among children?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'SOAP_AVAILABILITY','Availability of Soaps and Liquid soaps for Hand wash?',null,'[{"key":"Available","value":"Y"},{"key":"Not Available","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'UTENSIL_AREA','Is there a separate utensil washing area?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);




INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'BOYS','Is there a separate Toilet for?','Boys',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,'Is there a separate Toilet for?','GIRLS','Is there a separate Toilet for?','Girls',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,'Is there a separate Toilet for?','CWSN','Is there a separate Toilet for?','Cwsn',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,'Is there a separate Toilet for?','TEACHER','Is there a separate Toilet for?','Teacher',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',4);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'STUDENTS','Toilet Cleaned By ','Students',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'CLEANING_STUFF','Toilet Cleaned By ','Cleaning Stuff',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'CORPORATION','Toilet Cleaned By ','Corporation',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'OTHER_TOILET_CLEAN_OPT','Toilet Cleaned By ','Other Specify',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'OTHER_TOILET_CLEANER','Toilet Cleaned By ','Mention Please',null,null,'N','OTHER_TOILET_CLEAN_OPT','textbox',4);



INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FREQ_TWICE_DAY','Frequency of Toilet Cleaning','Twice a Day',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FREQ_ONCE_DAY','Frequency of Toilet Cleaning','Once a Day',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FREQ_ONCE_WEEK','Frequency of Toilet Cleaning ','Once a Week',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FREQ_OTHER_OPT','Frequency of Toilet Cleaning ','Other Specify  ',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'FREQ_OTHER','Frequency of Toilet Cleaning','Mention Please',null,null,'N','FREQ_OTHER_OPT','textbox',4);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'SCOOL_GRANT','Consumables Supported By','School Grant',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'DONORS','Consumables Supported By','Donors',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'OTHER_SCHOOL_GRANT_OPT','Consumables Supported By','Other Specify',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'OTHER_SCHOOL_GRANT','Consumables Supported By','Mention Please',null,null,'N','OTHER_SCHOOL_GRANT_OPT','textbox',3);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'WATER_PURIFIER','Water & Storage Facility:','Is water purifier available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_TOTAL','RO Unit','Total Available  ','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_REPAIR_REQD','RO Unit','Total req. Repair  ','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_NEW_REQ','RO Unit','New Requirement  ','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_TOTAL','UV Unit','Total Available       ','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_REPAIR_REQD','UV Unit','Total req. Repair       ','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_NEW_REQ','UV Unit','New Requirement       ','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_TOTAL','Storage facility','Total Available       ','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_REPAIR_REQD','Storage facility','Total req. Repair       ','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_NEW_REQ','Storage facility','New Requirement       ','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'SRC_OF_WATER','Water & Storage Facility','Source of water :  ',null,'[{"key":"Bore well","value":"BW"},{"key":"Corporation/GP/Water Connection","value":"CP"},{"key":"Water Tanker","value":"WT"},{"key":"Open Well","value":"OW"}]','N',null,'dropdown',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'WATER_STORAGE','Water & Storage Facility','Water storage System :  ' ,null,'[{"key":"Sump","value":"SMP"},{"key":"Overhead tank","value":"OHT"},{"key":"Both","value":"SmpOht"}]','N',null,'dropdown',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'WATER_TESTED','Is water tested regularly?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'HEALTH',null,null,'DUSTBINS','Is there dustbins available for each class?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'WASTE_DISPOSABLE','Water Disposable system?',null,'[{"key":"Through corporation/GP","value":"THC"},{"key":"Having own waste management system","value":"OWM"},{"key":"None of the above","value":"WDN"}]','N',null,'checkbox',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('HEALTH',null,null,'INCINERATOR','Is there is incinerator to dispose sanitary Pads?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


-- Classroom Environment
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'SEP_COMP_LAB','Is there a separate room for computer Lab in the school?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'CURR_AVL','Is curriculum available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'E_LEARN','Is e-learning system of teaching is available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'E_LEARN_PRVDR','If yes mention the organization who provide the system  ',null,null,'N','E_LEARN','textbox',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'INTERNET_CONNECTIVITY','Does the school has Internet Connectivity',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMPS','COMPS_AVAILABLE','Computers','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMPS','COMPS_REPAIR_REQD','Computers','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMPS','COMPS_NEW_REQD','Computers','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_AVAILABLE','Laptops','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_REPAIR_REQD','Laptops','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_NEW_REQD','Laptops','New Req.  ','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','UPS','UPS_AVAILABLE','UPS','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','UPS','UPS_REPAIR_REQD','UPS','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','UPS','UPS_NEW_REQD','UPS','New Req.  ','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_AVAILABLE','Computer tables','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_REPAIR_REQD','Computer tables','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_NEW_REQD','Computer tables','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_AVAILABLE','Computer chairs','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_REPAIR_REQD','Computer chairs','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_NEW_REQD','Computer chairs','New Req.  ','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_AVAILABLE','Projector','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_REPAIR_REQD','Projector','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_NEW_REQD','Projector','New Req.  ','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_AVAILABLE','Projector Screen','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_REPAIR_REQD','Projector Screen  ','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_NEW_REQD','Projector Screen  ','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM','Details of the computer lab','PRINTER','PRINTER_AVAILABLE','Printer','Total Available  ','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PRINTER','PRINTER_REPAIR_REQD','Printer','Repair Req.  ','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Details of the computer lab','PRINTER','PRINTER_NEW_REQD','Printer','New Req.  ','number',null,'N',null,'textbox',3);



INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'LIBRARY_NGO','Is there any NGO/Org managing/supporting the library?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'LIBRARY_NGO_ORG','If yes Mention name of the organization  ',null,null,'N','LIBRARY_NGO','textbox',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM','LIB_TIME_TABLE','Is there any fixed  time table for Library usage?','LIB_TIME_TABLE','Is there any fixed  time table for Library usage?','Is there any fixed  time table for Library usage?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'LIB_ACTIVITY_PROG','Mention the library activity planned for an academic year?  ',null,null,'N',null,'textbox',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Library Room Details','Is Library arranged level wise','LIBRARY_LEVELS','Library Room Details','Is Library arranged level wise',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Library Room Details','No of books available','LIBRARY_BOOKS_COUNT','Library Room Details','No of books available',null,null,'N',null,'textbox',null);



INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Library Room Details','Is sufficient cupboards available','LIBRARY_CUPBOARDS','Library Room Details','Is sufficient cupboards available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM','Library Room Details','Is Table available','LIBRARY_TABLE','Library Room Details','Is Table available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Library Room Details','Is sufficient Chairs/Bench available','LIBRARY_CHRS_BNCH','Library Room Details','Is sufficient Chairs/Bench available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','Library Room Details','Does Library has Book Issue Register','LIB_BOOK_REGISTER','Library Room Details','Does Library has Book Issue Register?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);




INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SCIENCE_LAB','Science Club Details:','Is there a separate room for Science Lab?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','','Centre Table','SC_LAB_CNTR_TBL','Science Club Details:','Centre Table',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',2);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','','Chemicals','SC_LAB_CHMCLS','Science Club Details:','Chemicals',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',3);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','','Washbasin','SC_LAB_WSH_BASIN','Science Club Details','Washbasin',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',4);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM','','Models','SC_LAB_MODELS','Science Club Details:','Models',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',5);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM','','Any Other','SC_LAB_ANY_OTH',null,'Any Other','Science Club Details:','[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',6);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SCIENCE_NGO','Science Club Details:','Is there any Org/NGO supporting Science lab activities?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',7);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SCIENCE_NGO_ORG','Science Club Details:','If yes Mention the name of the organization',null,null,'N','SCIENCE_NGO','textbox',8);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SCIENCE_LAB_FUNCTION','Science Club Details:','Is Science lab functioning in the school?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',9);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SPORTS_ROOM','Sports,Arts,Crafts Facility:','Is there a store room dedicated for sports?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',1);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'PLAY_GRND','Sports,Arts,Crafts Facility:','Do you have play ground?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',2);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'SPRTS_MTRLS','Sports,Arts,Crafts Facility :','Do you have sufficient sports materials?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ( 'CLASSROOM',null,null,'ARTS_CRAFTS','Sports,Arts,Crafts Facility','Are you engaging students in teaching art & Craft?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'ARTS_CRAFTS_ORG','Sports,Arts,Crafts Facility','Is there any organization supporting Art & Craft?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'LRNG_MTRLS','Sports,Arts,Crafts Facility','Do you have sufficient Teacher Learning Materials?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'KITCHEN_GARDEN','Sports,Arts,Crafts Facility','Does your School has Kitchen Garden?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'DONOR_NGO_SUPPORT_FLAG',null,'List out the Donor/CSR/NGO Supporting School',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


INSERT INTO `care_works`.`form_fields` (`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES ('CLASSROOM',null,null,'DONORLIST',null,'Yes Mention the DONOR/NGO Names(comma (,)separated ) ',null,null,'N','DONOR_NGO_SUPPORT_FLAG','textbox',null);

INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'staffRoom','Staff Room','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'library','Library','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'computerLab','Computer Lab','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'hmRoomOffice','HM Room/Office','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'scienceLab','Science Lab','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'storeRoom','Store Room','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'kitchen','Kitchen','label',NULL,'Y',NULL,NULL,NULL,'Y');
INSERT INTO `care_works`.`form_fields` (`CATEGORY`,`SUB_CATEGORY`,`FIELD_SECTION`,`FIELD_SECTION_HEADER`,`FIELD_ID`,`FIELD_LABEL`,`FIELD_TYPE`,`FIELD_VALIDATION`,`FIELD_REQUIRED`,`PARENT_FIELD`,`CHILD_SEQ`,`FIELD_OPTIONS`,`IS_ACTIVE`) VALUES ('SCHOOL',NULL,NULL,NULL,'corridor','Corridor','label',NULL,'Y',NULL,NULL,NULL,'Y');

COMMIT;
