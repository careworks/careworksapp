USE `care_works`;

-- -----------------------------------------------------
-- Data for table `care_works`.`school_category`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;

alter table users
    drop key uk_users_email;

alter table users
    drop key uk_users_username;

alter table users
    modify name text not null;

alter table users
    modify username text not null;

alter table users
    modify email text not null;

alter table users
    modify password text not null;