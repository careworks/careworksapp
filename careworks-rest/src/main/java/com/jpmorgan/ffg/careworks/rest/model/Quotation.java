package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/*
 * Created By Pavan Keshav L on 15-Apr-2020
 * */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "quotation")
@JsonPropertyOrder({"quotId", "actionId", "schoolId", "companyName", "bo_Q_No", "descriptionOfWork", "unit", "number", "length", "breadth", "depth", "quantity", "rate", "amount", "status", "updatedAt"})
public class Quotation extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long quotId;
    private Long actionId;
    private Long schoolId;
    private String companyName;
    private String BO_Q_No;
    private String descriptionOfWork;
    private String unit;
    @PositiveOrZero(message = "Number should be positive")
    private BigDecimal number;
    @PositiveOrZero(message = "Length should be positive")
    private BigDecimal length;
    @PositiveOrZero(message = "Breadth should be positive")
    private BigDecimal breadth;
    @PositiveOrZero(message = "Depth should be positive")
    private BigDecimal depth;
    @PositiveOrZero(message = "Quantity should be positive")
    private BigDecimal quantity;
    @PositiveOrZero(message = "Rate should be positive")
    private BigDecimal rate;
    @PositiveOrZero(message = "Amount should be positive")
    private BigDecimal amount;
    private String status;
}
