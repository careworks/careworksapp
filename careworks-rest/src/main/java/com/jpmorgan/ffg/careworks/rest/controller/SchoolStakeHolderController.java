package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.StakeholderInfo;
import com.jpmorgan.ffg.careworks.rest.service.SchoolStakeHolderDetailsSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class SchoolStakeHolderController {

    private final SchoolStakeHolderDetailsSvc stakeHolderInfoSvc;


    @GetMapping("/school/stakeHolders/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<StakeholderInfo> GetStakeHolderDetailsForSchool(@PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return stakeHolderInfoSvc.GetStakeHolderDetailsForSchool(schoolId);
    }

    @GetMapping("/school/stakeHolder/{stakeHolderId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public StakeholderInfo GetStakeHolder(@PathVariable("stakeHolderId") Long stakeHolderId) throws ResourceNotFoundExceptionSimple {
        return stakeHolderInfoSvc.GetStakeHolderDetailsById(stakeHolderId);
    }

    @PostMapping("/school/stakeHolders/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveStakeHolders(@Valid @RequestBody List<StakeholderInfo> stakeHolderDetails, @PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(stakeHolderInfoSvc.SaveSchoolStakeHolders(stakeHolderDetails.stream().filter(Objects::nonNull).collect(Collectors.toList()), schoolId));
    }

    @PostMapping("/school/stakeHolder/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveStakeHolder(@Valid @RequestBody StakeholderInfo stakeHolderDetail, @PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(stakeHolderInfoSvc.SaveStakeHolderDetails(schoolId, stakeHolderDetail));
    }

    @DeleteMapping("/school/stakeHolders/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> deleteSchoolStakeHoldersDetails(@RequestBody List<Long> Ids) {
        Ids.forEach(id -> stakeHolderInfoSvc.DeleteSchoolStakeHolderDetails(id));
        return ResponseEntity.ok(true);
    }
}
