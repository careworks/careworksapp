package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "teacher_details")
public class TeacherGroupDetails extends DateAudit {

    //    @JsonIgnore
    private Long schoolId;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "teacher_group_id", updatable = false, nullable = false)
    private Long teacherGroupId;

    @Column(name = "employment_type")
    private String teacher;

    @Column(name = "male")
    @Min(value = 0, message = "Male Teacher count cannot be less than zero")
    private Long male;

    @Column(name = "female")
    @Min(value = 0, message = "Female Teacher count cannot be less than zero")
    private Long female;

    @Min(value = 0, message = "Teacher count cannot be less than zero")
    private Integer teacherCount;

    @JsonIgnore
    private String subject;

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

}
