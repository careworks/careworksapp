package com.jpmorgan.ffg.careworks.rest.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Arijit on 14-Dec-19.
 */
@Entity
@IdClass(NeedAssessmentResponseId.class)
@Table(name = "need_assessment_response")
public class NeedAssessmentResponse implements Serializable {
    @Id
    private String fieldId;
    @Id
    private Long schoolId;
    private String fieldValue;
    private String category;

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
