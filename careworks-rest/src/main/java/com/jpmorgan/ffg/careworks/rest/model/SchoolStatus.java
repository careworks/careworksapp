package com.jpmorgan.ffg.careworks.rest.model;

import com.jpmorgan.ffg.careworks.rest.annotation.ValidateStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "school_status")
public class SchoolStatus {

    @NotNull
    @Id
    private Long schoolId;

    @ValidateStatus
    private String schoolProfile;
    @ValidateStatus
    private String needAssessment;
    @ValidateStatus
    private String actionPlan;
    @ValidateStatus
    private String monitoring;


    public SchoolStatus() {

    }

    public static SchoolStatus defaultSchoolStatus() {
        SchoolStatus status = new SchoolStatus();
        status.actionPlan = "N/A";
        status.needAssessment = "N/A";
        status.schoolProfile = "N/A";
        status.monitoring = "N/A";

        return status;

    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolProfile() {
        return schoolProfile;
    }

    public void setSchoolProfile(String schoolProfile) {
        this.schoolProfile = schoolProfile;
    }

    public String getNeedAssessment() {
        return needAssessment;
    }

    public void setNeedAssessment(String needAssessment) {
        this.needAssessment = needAssessment;
    }

    public String getActionPlan() {
        return actionPlan;
    }

    public void setActionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
    }

    public String getMonitoring() {
        return monitoring;
    }

    public void setMonitoring(String monitoring) {
        this.monitoring = monitoring;
    }


}
