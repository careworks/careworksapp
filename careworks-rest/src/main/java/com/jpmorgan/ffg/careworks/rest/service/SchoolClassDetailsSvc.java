package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.ClassDetailsRepo;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.Section;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Service
public class SchoolClassDetailsSvc {

    private final ClassDetailsRepo classDetailsRepo;

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    public List<Section> GetSchoolClassDetails(Long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        return classDetailsRepo.findAllById(schoolId);
    }

    public Section GetClassDetail(Long schoolId, long sectionId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        return classDetailsRepo.findById(sectionId).get();
    }

    private String validateMedium(Long schoolId, String medium) throws ResourceNotFoundExceptionSimple {
        SchoolProfile schoolProfile = ValidateSchool(schoolId);
        if (!schoolProfile.getMediums().toUpperCase().contains(medium.toUpperCase())) {
            return schoolProfile.getMediums();
        }
        return "";
    }

    public List<Section> SaveSchoolSections(List<Section> schoolSections, long schoolId) throws Exception, ResourceNotFoundExceptionSimple {
        schoolSections = schoolSections.stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (Section section : schoolSections) {
            section.setSchoolId(schoolId);
            String error;
            if (StringUtils.isNotBlank(section.getMedium())) {
                section.setMedium(section.getMedium().trim());
                error = validateMedium(schoolId, section.getMedium());
            } else error = "";
            if (!StringUtils.isEmpty(error)) {
                throw new Exception("Medium " + section.getMedium() + " is invalid. It should be one of " + error);
            }
            if (section.getStudentDetails() != null) {
                section.setStudentDetails(section.getStudentDetails().stream().filter(Objects::nonNull).collect(Collectors.toSet()));
                section.getStudentDetails().stream().filter(Objects::nonNull).forEach(studentDetails -> {
                    studentDetails.setSection(section);
                    studentDetails.setSectionId(section.getSectionId());
                });
            }
        }

        return classDetailsRepo.saveAll(schoolSections);
    }

    private SchoolProfile ValidateSchool(long schoolId) throws ResourceNotFoundExceptionSimple {
        Optional<SchoolProfile> school = basicSchoolProfileRepo.findById(schoolId);
        if (!school.isPresent()) {
            throw new ResourceNotFoundExceptionSimple("No School found for given school ID");
        }
        return school.get();
    }

    public Section SaveSchoolSectionSingle(Section schoolSection, long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);

        schoolSection.setSchoolId(schoolId);
        Section res = classDetailsRepo.saveAndFlush(schoolSection);
        schoolSection.getStudentDetails().stream().filter(Objects::nonNull).forEach(studentDetails -> classDetailsRepo.SetSectionMapping(res.getSectionId(), studentDetails.getStudentDetailsId()));
        return res;
    }

    public void deleteClassSectionDetails(Long Id) {
        classDetailsRepo.findById(Id).ifPresent(section -> {
            try {
                if (GetSchoolClassDetails(section.getSchoolId()).size() > 1)
                    classDetailsRepo.delete(section);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
        });
    }
}
