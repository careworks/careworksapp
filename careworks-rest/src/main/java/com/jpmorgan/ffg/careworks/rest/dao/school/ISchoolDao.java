package com.jpmorgan.ffg.careworks.rest.dao.school;

import com.jpmorgan.ffg.careworks.rest.dto.common.DashboardSchoolStatus;
import com.jpmorgan.ffg.careworks.rest.model.*;

import java.util.List;

public interface ISchoolDao {
    SchoolProfile getSchoolProfileDataBySchoolId(long schoolId);
    List<Location> getAllAvailableLocation();
    List<SchoolCategory> getAllAvailableSchoolCategoryList();
    List<Medium> getAllAvailableMediumsList();
    List<SchoolType> getAllAvailableSchoolTypeList();
    SchoolProfile saveSchoolData(SchoolProfile schoolProfile);
    SchoolProfileAddress saveSchoolAddress(SchoolProfileAddress schoolProfileAddress);
    List<DashboardSchoolStatus> getAllSchoolStatus();
}
