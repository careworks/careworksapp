package com.jpmorgan.ffg.careworks.rest.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Arijit on 30-Dec-19.
 */
@Getter
@Setter
public class Error {
    private String code;
    private String message;

    public Error() {

    }

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
