package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "school_env")
public class SchoolEnvironment implements Serializable {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    private Long school_id;
    private String particulars;
    private String hasImplemented;
    private int totalNum;
    private String renovationRequired;
    private String flooring;
    private String plastering;
    private String waterProofing;
    private String electrification;
    private String plumbing;
    private String blackboardPainting;
    private String wallmountedTable;
    private String nalikaChapara;
    private String externalPainting;
    private String internalPainting;
    private String remarks;
    private String yesNo;
    private String safetyGrill;

    public SchoolEnvironment() {
    }

    public SchoolEnvironment(Long school_id, String particulars, String hasImplemented, int totalNum, String renovationRequired, String flooring, String plastering, String waterProofing, String electrification, String plumbing, String blackboardPainting, String wallmountedTable, String nalikaChapara, String externalPainting, String internalPainting, String remarks) {
        this.school_id = school_id;
        this.particulars = particulars;
        this.hasImplemented = hasImplemented;
        this.totalNum = totalNum;
        this.renovationRequired = renovationRequired;
        this.flooring = flooring;
        this.plastering = plastering;
        this.waterProofing = waterProofing;
        this.electrification = electrification;
        this.plumbing = plumbing;
        this.blackboardPainting = blackboardPainting;
        this.wallmountedTable = wallmountedTable;
        this.nalikaChapara = nalikaChapara;
        this.externalPainting = externalPainting;
        this.internalPainting = internalPainting;
        this.remarks = remarks;
    }

    @JsonIgnore
    public Long getSchool_id() {
        return school_id;
    }

    @JsonProperty
    public void setSchool_id(Long school_id) {
        this.school_id = school_id;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getHasImplemented() {
        return hasImplemented;
    }

    public void setHasImplemented(String hasImplemented) {
        this.hasImplemented = hasImplemented;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public String getRenovationRequired() {
        return renovationRequired;
    }

    public void setRenovationRequired(String renovationRequired) {
        this.renovationRequired = renovationRequired;
    }

    public String getFlooring() {
        return flooring;
    }

    public void setFlooring(String flooring) {
        this.flooring = flooring;
    }

    public String getPlastering() {
        return plastering;
    }

    public void setPlastering(String plastering) {
        this.plastering = plastering;
    }

    public String getWaterProofing() {
        return waterProofing;
    }

    public void setWaterProofing(String waterProofing) {
        this.waterProofing = waterProofing;
    }

    public String getElectrification() {
        return electrification;
    }

    public void setElectrification(String electrification) {
        this.electrification = electrification;
    }

    public String getPlumbing() {
        return plumbing;
    }

    public void setPlumbing(String plumbing) {
        this.plumbing = plumbing;
    }

    public String getBlackboardPainting() {
        return blackboardPainting;
    }

    public void setBlackboardPainting(String blackboardPainting) {
        this.blackboardPainting = blackboardPainting;
    }

    public String getWallmountedTable() {
        return wallmountedTable;
    }

    public void setWallmountedTable(String wallmountedTable) {
        this.wallmountedTable = wallmountedTable;
    }

    public String getNalikaChapara() {
        return nalikaChapara;
    }

    public void setNalikaChapara(String nalikaChapara) {
        this.nalikaChapara = nalikaChapara;
    }

    public String getExternalPainting() {
        return externalPainting;
    }

    public void setExternalPainting(String externalPainting) {
        this.externalPainting = externalPainting;
    }

    public String getInternalPainting() {
        return internalPainting;
    }

    public void setInternalPainting(String internalPainting) {
        this.internalPainting = internalPainting;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getYesNo() {
        return yesNo;
    }

    public void setYesNo(String yesNo) {
        this.yesNo = yesNo;
    }

    public String getSafetyGrill() {
        return safetyGrill;
    }

    public void setSafetyGrill(String safetyGrill) {
        this.safetyGrill = safetyGrill;
    }
}
