package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.FormFieldsRepo;
import com.jpmorgan.ffg.careworks.rest.model.FormFields;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Arijit on 07-Dec-19.
 */

@Service
@AllArgsConstructor
public class FormFieldsService {

    private final FormFieldsRepo formFieldsRepo;

    public List<FormFields> getAllFieldsByCategory(String category) {
        List<String> categories = Arrays.asList(category.split(","));
        return formFieldsRepo.findByCategory(categories);
    }

    public void saveQuestion(FormFields formFields, boolean action) {
        if (action)
            formFieldsRepo.saveAndFlush(formFields);
        else if (formFieldsRepo.count() > 1)
            formFieldsRepo.deleteById(formFields.getSeq());
    }

    public List<FormFields> getAllFields() {
        return formFieldsRepo.findAll();
    }
}
