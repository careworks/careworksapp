package com.jpmorgan.ffg.careworks.rest.dto.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropdownVo {
    private long id;
    private String label;
    private String value;
}
