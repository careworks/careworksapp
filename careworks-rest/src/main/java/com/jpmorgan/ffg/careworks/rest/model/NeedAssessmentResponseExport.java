package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeedAssessmentResponseExport {

    Long schoolId;
    String category;
    String subCategory;
    String fieldSection;
    String fieldSectionHeader;
    String fieldLabel;
    String fieldValue;
}
