package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.TeacherGroupDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("teacherDetailsDataRepository")
public interface TeacherDetailsDataRepo extends JpaRepository<TeacherGroupDetails, Long> {
    @Query(value = "SELECT * " +
            "FROM teacher_details " +
            "WHERE school_id = :school_id", nativeQuery = true)
    List<TeacherGroupDetails> findAllById(@Param("school_id") Long school_id);
}

