package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Building_Master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"building_id", "category_id"})})
public class BuildingMaster implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "building_id")
    private Long buildingId;

    @Column(name = "category_id")
    @JsonIgnore
    private Long category_id;

    public BuildingMaster() {
    }

    public BuildingMaster(Long id, Long buildingId, Long category_id) {
        this.id = id;
        this.buildingId = buildingId;
        this.category_id = category_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BuildingMaster)) return false;
        BuildingMaster that = (BuildingMaster) o;
        return getBuildingId().equals(that.getBuildingId()) &&
                getCategory_id().equals(that.getCategory_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBuildingId(), getCategory_id());
    }
}
