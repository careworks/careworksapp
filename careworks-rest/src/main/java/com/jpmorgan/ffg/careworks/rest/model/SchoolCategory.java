package com.jpmorgan.ffg.careworks.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "school_category")
public class SchoolCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seq;

    private String category;
    private String description;
}
