package com.jpmorgan.ffg.careworks.rest.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class JwtAuthenticationResponse {
    private String id_token;
    private final String tokenType = "Bearer";
    private String role;
}
