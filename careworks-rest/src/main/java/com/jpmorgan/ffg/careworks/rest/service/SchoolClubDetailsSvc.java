package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolClubDataRepo;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolClubDetails;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
@AllArgsConstructor
@Service
public class SchoolClubDetailsSvc {

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    private final SchoolClubDataRepo schoolClubDataRepo;


    public Collection<SchoolClubDetails> getSchoolClubDetailsBySchool(Long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        return schoolClubDataRepo.findAllClubsBySchool(schoolId);
    }

    public List<SchoolClubDetails> saveAllSchoolClub(List<SchoolClubDetails> schoolClubDetails, long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        schoolClubDetails.forEach(item -> {item.setSchoolId(schoolId); schoolClubDataRepo.saveAndFlush(item);});
        return schoolClubDetails;
    }

    public SchoolClubDetails getAllClubs(Long schoolClubId) throws ResourceNotFoundExceptionSimple {
        return schoolClubDataRepo.findById(schoolClubId).get();
    }

    public SchoolClubDetails saveSchoolClub(Long schoolId, SchoolClubDetails schoolClubDetails) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        schoolClubDetails.setSchoolId(schoolId);
        return schoolClubDataRepo.saveAndFlush(schoolClubDetails);
    }


    private void ValidateSchool(long schoolId) throws ResourceNotFoundExceptionSimple {
        Optional<SchoolProfile> school = basicSchoolProfileRepo.findById(schoolId);
        if (!school.isPresent()) {
            throw new ResourceNotFoundExceptionSimple("No School found for given school ID");
        }
    }

    public void deleteSchoolClub(Long id) {
        schoolClubDataRepo.findById(id).ifPresent(schoolClubDetails -> {
            try {
                if (getSchoolClubDetailsBySchool(schoolClubDetails.getSchoolId()).size() > 1)
                    schoolClubDataRepo.delete(schoolClubDetails);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
        });
    }
}
