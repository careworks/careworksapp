package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.*;
import com.jpmorgan.ffg.careworks.rest.model.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ActionPlanService {

    private final ActivityMasterRepo activityMasterRepo;

    private final ActionMasterRepo actionMasterRepo;

    private final ActionPlanRepo actionPlanRepo;

    private final BuildingMasterRepo buildingMasterRepo;

    private final CategoryMasterRepo categoryMasterRepo;

    public List<ActivityMaster> getActivities(Long category_id) {
        return activityMasterRepo.findAllById(category_id);
    }

    public List<ActivityMaster> getActivities() {
        return activityMasterRepo.findAll();
    }

    public void saveActivity(ActivityMaster activityMaster, Boolean action) {
        if (action) {
            activityMasterRepo.saveAndFlush(activityMaster);
        } else {
            if (actionMasterRepo.count() > 1) {
                actionMasterRepo.deleteById(activityMaster.getActivityId());
            }
        }
    }

    public List<CategoryMaster> getCategories() {
        return categoryMasterRepo.findAll();
    }
    
    public Optional<CategoryMaster> getCategoryById(Long id){
        return categoryMasterRepo.findById(id);
    }

    public Optional<ActionPlan> getActionPlanDetails(Long actionplanId) {
        return actionPlanRepo.findById(actionplanId);
    }

    public List<ActionPlan> getActionPlansBySchool(Long schoolId) {
        return actionPlanRepo.findAllBySchoolId(schoolId);
    }

    public List<ActionMaster> getActionMasterDetails(Long actionplanId) {
        return actionMasterRepo.findAllById(actionplanId);
    }

    public List<BuildingMaster> getBuildingsInfo(Long category_id) {
        List<BuildingMaster> buildingMasterList = buildingMasterRepo.findAllById(category_id);
        return buildingMasterList;
    }

    public ActionPlan saveActionPlan(ActionPlan actionPlan) {
        return actionPlanRepo.saveAndFlush(actionPlan);
    }

    public int updateActionPlanStatus(ActionPlan actionPlan) {
        return actionPlanRepo.updateActionPlanStatus(actionPlan.getStatus(), actionPlan.getActionplanId());
    }

}

