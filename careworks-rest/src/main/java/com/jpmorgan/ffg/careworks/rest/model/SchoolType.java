package com.jpmorgan.ffg.careworks.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "school_type")
public class SchoolType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seq;

    private String type;

    private String description;

}
