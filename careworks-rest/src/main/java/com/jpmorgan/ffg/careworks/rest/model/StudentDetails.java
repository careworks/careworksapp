package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Getter
@Setter
@Embeddable
@Entity
@NoArgsConstructor
@Table(name = "Student_Info")
public class StudentDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_details_id", updatable = false, nullable = false)
    private Long studentDetailsId;

    @JsonIgnore
    @Column(name = "section_id", insertable = false, updatable = false)
    private Long sectionId;

    @ManyToOne
    @JoinColumn(name = "section_id")
    @JsonIgnore
    private Section section;

    @NotNull
    @Min(value = 0, message = "Student count cannot be less than zero")
    private Integer studentCount;

    private String gender;

    @JoinColumn(name = "social_division")
    private String socialDivision;

    public StudentDetails(@NotNull int studentCount, String gender, String socialDivision) {
        this.studentCount = studentCount;
        this.gender = gender;
        this.socialDivision = socialDivision;
    }
}
