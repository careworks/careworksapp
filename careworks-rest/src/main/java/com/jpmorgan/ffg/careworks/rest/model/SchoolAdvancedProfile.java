package com.jpmorgan.ffg.careworks.rest.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SchoolAdvancedProfile {

    private List<Section> schoolClassDetails;
    private List<TeacherGroupDetails> teacherDetails;
    private List<StakeholderInfo> stakeholderInfo;
    private List<SchoolEducationDepartment> educationDepartment;

    public List<SchoolEducationDepartment> getEducationDepartment() {
        return educationDepartment;
    }

    public void setEducationDepartment(List<SchoolEducationDepartment> educationDepartment) {
        this.educationDepartment = educationDepartment;
    }

    public List<Section> getSchoolClassDetails() {
        return schoolClassDetails;
    }

    public void setSchoolClassDetails(List<Section> studentDetails) {
        this.schoolClassDetails = studentDetails;
    }

    public List<TeacherGroupDetails> getTeacherDetails() {
        return teacherDetails;
    }

    public void setTeacherDetails(List<TeacherGroupDetails> teacherDetails) {
        this.teacherDetails = teacherDetails;
    }

    public List<StakeholderInfo> getStakeholderInfo() {
        return stakeholderInfo;
    }

    public void setStakeholderInfo(List<StakeholderInfo> stakeholderInfo) {
        this.stakeholderInfo = stakeholderInfo;
    }
}
