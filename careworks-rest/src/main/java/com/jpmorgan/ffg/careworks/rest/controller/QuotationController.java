package com.jpmorgan.ffg.careworks.rest.controller;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.jpmorgan.ffg.careworks.rest.data.QuotationRepo;
import com.jpmorgan.ffg.careworks.rest.dto.Status;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.ActionMaster;
import com.jpmorgan.ffg.careworks.rest.model.ActionPlan;
import com.jpmorgan.ffg.careworks.rest.model.ActivityMaster;
import com.jpmorgan.ffg.careworks.rest.model.CategoryMaster;
import com.jpmorgan.ffg.careworks.rest.model.Quotation;
import com.jpmorgan.ffg.careworks.rest.service.ActionPlanService;
import com.jpmorgan.ffg.careworks.rest.service.QuotationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/api/quotation")
@Slf4j
public class QuotationController {

    private final LoadingCache<Long, String> actionPlanCategories = CacheBuilder.newBuilder()
            .maximumSize(50)
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .recordStats()
            .build(new CacheLoader<Long, String>() {
                @Override
                public String load(Long aLong) throws Exception {
                    return actionPlanService.getCategoryById(aLong).map(CategoryMaster::getCategoryDetails).orElse(StringUtils.EMPTY);
                }
            });

    private final QuotationRepo quotationRepo;

    private final ActionPlanService actionPlanService;

    private final QuotationService quotationService;

    @GetMapping("/getActionPlans/{schoolId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getActionPlans(@PathVariable Long schoolId) throws ResourceNotFoundExceptionSimple {
        Map<String, Map<String, List<Map<String, String>>>> fyActionPlans = new LinkedHashMap<>();
        List<ActivityMaster> activities = actionPlanService.getActivities();

        List<ActionPlan> actionPlanList = actionPlanService.getActionPlansBySchool(schoolId);

        actionPlanList.stream().filter(actionPlan -> StringUtils.equalsIgnoreCase("Approved", actionPlan.getStatus())).forEach(actionPlan -> {
            Map<String, List<Map<String, String>>> actionPlanDetails = new LinkedHashMap<>();
            actionPlan.getActionMasters().forEach(actionMaster -> {
                Map<String, String> actionDetails = new LinkedHashMap<>();
                actionDetails.put("actionId", actionMaster.getActionmasterId().toString());
                activities.stream()
                        .filter(activityMaster -> activityMaster.getActivityId() == actionMaster.getActivityId() && activityMaster.getCategoryId() == actionMaster.getCategoryId())
                        .findAny()
                        .ifPresent(activityMaster -> actionDetails.put("Activity", activityMaster.getActivityDetail()));

                actionDetails.put("status", actionPlan.getStatus());
                try {
                    String category = actionPlanCategories.get(actionMaster.getCategoryId());
                    actionPlanDetails.putIfAbsent(category, new LinkedList<>());
                    actionPlanDetails.get(category).add(actionDetails);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });
            fyActionPlans.put(actionPlan.getFinancialYear().replace(",", "-") + "," + actionPlan.getActionplanId(), actionPlanDetails);
        });
        return ResponseEntity.ok(fyActionPlans);
    }

    @GetMapping("/getQuotationBySchoolId/{schoolId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getQuotationBySchoolId(@PathVariable long schoolId) {
        return ResponseEntity.ok(quotationRepo.findQuotationsBySchoolId(schoolId));
    }

    @GetMapping("/getQuotationByActionId/{actionId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getQuotationByActionId(@PathVariable long actionId) {
        return ResponseEntity.ok(quotationRepo.findQuotationsByActionId(actionId));
    }

    @PostMapping("/saveQuotation")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> saveQuotation(@Valid @RequestBody List<Quotation> quotations) throws Exception {
        if (CollectionUtils.isNotEmpty(quotations)) {
            quotationService.validateInput(quotations);
        }
        return ResponseEntity.ok(quotationRepo.saveAll(quotations));
    }

    @PostMapping("/updateQuotation/{status}/{planId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> submitQuotation(@PathVariable Long planId, @PathVariable String status) throws ResourceNotFoundExceptionSimple, Exception {
        List<Long> actionIdList = actionPlanService.getActionMasterDetails(planId).stream().map(ActionMaster::getActionmasterId).collect(Collectors.toList());
        if (actionIdList.isEmpty()) {
            throw new Exception("No activities to " + status);
        }
        for (Long actionListId : actionIdList) {
            submitQuotationBasedOnActionId(actionListId, status);
        }

        return ResponseEntity.ok(actionIdList);
    }

    @PostMapping("/submitQuotation/{status}/{actionId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> submitQuotationBasedOnActionId(@PathVariable Long actionId, @PathVariable String status) throws Exception {
        List<Quotation> quotations = quotationRepo.findQuotationsByActionId(actionId);
        boolean trip = true;
        for (Quotation quotation : quotations) {
            if (StringUtils.equalsIgnoreCase(status, Status.REJECTED.toString()) && StringUtils.equalsIgnoreCase(quotation.getStatus(), Status.SUBMITTED.toString())) {
                quotation.setStatus(status);
                trip = false;
            } else if (StringUtils.equalsIgnoreCase(status, Status.APPROVED.toString()) && StringUtils.equalsIgnoreCase(quotation.getStatus(), Status.SUBMITTED.toString())) {
                quotation.setStatus(status);
                trip = false;
            } else if (StringUtils.equalsIgnoreCase(status, Status.SUBMITTED.toString()) && !StringUtils.equalsIgnoreCase(quotation.getStatus(), Status.APPROVED.toString()) && !StringUtils.equalsIgnoreCase(quotation.getStatus(), Status.CREATED.toString())) {
                quotation.setStatus(status);
                trip = false;
            } else {
                log.error("Unknown status update {} for quotation : {}", status, quotation);
            }

        }
        if (trip) {
            throw new Exception("No quotations to " + status);
        }
        return ResponseEntity.ok(quotationRepo.saveAll(quotations));
    }

    @DeleteMapping("/deleteQuotations")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> deleteQuotation(@Valid @RequestBody List<Long> quotations) {
        List<Quotation> quotationList = quotationRepo.findAllById(quotations);
        quotationRepo.deleteAll(quotationList);
        return ResponseEntity.ok(quotationList);
    }

    @GetMapping("/getConsQuotationBySchoolId/{schoolId}/{planId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getConsolidatedQuotationBySchoolId(@PathVariable Long schoolId, @PathVariable Long planId) throws ResourceNotFoundExceptionSimple {
        List<Quotation> quotations = quotationRepo.findQuotationsBySchoolId(schoolId);
        Map<String, Map<String, List<Map<String, String>>>> fyActionPlans = (Map<String, Map<String, List<Map<String, String>>>>) getActionPlans(schoolId).getBody();
        LinkedList<Quotation> consQuots = new LinkedList<>();

        if (fyActionPlans != null && quotations != null && !quotations.isEmpty()) {
            fyActionPlans.forEach((planName, categories) -> {
                String[] plan = planName.split(",");
                if (plan.length > 1 && StringUtils.equals(plan[1], planId.toString())) {
                    categories.forEach((category, activities) -> activities.forEach(activity -> {
                        consQuots.add(Quotation.builder().descriptionOfWork(category+" - "+activity.get("Activity")).build());
                        //consQuots.add(Quotation.builder().descriptionOfWork("  ").build());
                        consQuots.addAll(quotations.stream().filter(quotation -> StringUtils.equals(quotation.getActionId().toString(), activity.get("actionId"))).collect(Collectors.toList()));
                    }));
                }
            });
        }

        return ResponseEntity.ok(consQuots);
    }
}
