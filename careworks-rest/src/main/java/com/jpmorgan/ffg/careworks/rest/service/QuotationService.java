package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.dto.Status;
import com.jpmorgan.ffg.careworks.rest.model.Codes;
import com.jpmorgan.ffg.careworks.rest.model.Quotation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Arijit on 08-May-20.
 */
@Service
@AllArgsConstructor
public class QuotationService {

    private final ReferenceDataService referenceDataService;

    public void validateInput(List<Quotation> quotationList) throws Exception {

        List<String> validUnits = referenceDataService.getAllCodes().stream().
                filter(codes -> StringUtils.equals("QT_UNIT", codes.getType())).
                map(Codes::getCode).collect(Collectors.toList());

        for (Quotation qt : quotationList) {
            if (StringUtils.isNotBlank(qt.getUnit()) && !validUnits.contains(qt.getUnit().trim())) {
                throw new Exception(qt.getUnit() + " is invalid. Unit should be one of " + validUnits);
            }
            if (!StringUtils.equalsIgnoreCase(qt.getStatus(), Status.CREATED.toString()) && StringUtils.isEmpty(qt.getDescriptionOfWork())) {
                throw new Exception("Description for " + qt.getBO_Q_No() + " row is empty, Please fill correct details");
            }
        }
    }
}
