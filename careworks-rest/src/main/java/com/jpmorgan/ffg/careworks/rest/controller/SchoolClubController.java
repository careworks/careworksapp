package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolClubDetails;
import com.jpmorgan.ffg.careworks.rest.service.SchoolClubDetailsSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class SchoolClubController {

    private final SchoolClubDetailsSvc schoolClubDetailsSvc;


    @GetMapping("/school/clubs/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<SchoolClubDetails> getSchoolClubsBySchool(@PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return schoolClubDetailsSvc.getSchoolClubDetailsBySchool(schoolId);
    }

    @GetMapping("/school/club/{clubId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolClubDetails getSchoolClub(@PathVariable("clubId") Long clubId) throws ResourceNotFoundExceptionSimple {
        return schoolClubDetailsSvc.getAllClubs(clubId);
    }

    @PostMapping("/school/clubs/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> saveClubsBySchool(@Valid @RequestBody List<SchoolClubDetails> schoolClubDetails, @PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(schoolClubDetailsSvc.saveAllSchoolClub(schoolClubDetails.stream().filter(Objects::nonNull).collect(Collectors.toList()), schoolId));
    }

    @PostMapping("/school/club/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> saveClub(@Valid @RequestBody SchoolClubDetails schoolClubDetails, @PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(schoolClubDetailsSvc.saveSchoolClub(schoolId, schoolClubDetails));
    }

    @DeleteMapping("/school/clubs/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> deleteSchoolStakeHoldersDetails(@RequestBody List<Long> ids) {
        ids.forEach(id -> schoolClubDetailsSvc.deleteSchoolClub(id));
        return ResponseEntity.ok(true);
    }
}
