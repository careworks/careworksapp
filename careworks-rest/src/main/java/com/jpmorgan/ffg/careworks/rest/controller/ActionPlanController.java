package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.*;
import com.jpmorgan.ffg.careworks.rest.service.ActionPlanService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ActionPlanController {

    private final ActionPlanService actionPlanService;

    @GetMapping("actionplan/getActivities/{category_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<ActivityMaster> getActivitiesByCategoryId(@PathVariable("category_id") Long category_id) {
        return actionPlanService.getActivities(category_id);
    }

    @GetMapping("actionplan/getActivities")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<ActivityMaster> getActivities() {
        Map<Long, String> categoryMap = actionPlanService.getCategories().stream().collect(Collectors.toMap(CategoryMaster::getCategoryId, CategoryMaster::getCategoryDetails));
        return actionPlanService.getActivities().stream().map(activity -> {
            activity.setCategoryName(categoryMap.get(activity.getCategoryId()));
            return activity;
        }).collect(Collectors.toList());
    }

    @GetMapping("actionplan/getCategories")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<CategoryMaster> getCategories() {
        return actionPlanService.getCategories();
    }

    @GetMapping("actionplan/getActionPlansBySchool/{schoolId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public List<ActionPlan> getActionPlansBySchool(@PathVariable("schoolId") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return actionPlanService.getActionPlansBySchool(schoolId);
    }

    @GetMapping("actionplan/getBuildings/{categoryId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection getBuilding(@PathVariable("categoryId") Long categoryId) throws ResourceNotFoundExceptionSimple {
        Collection<BuildingMaster> buildingMasters = actionPlanService.getBuildingsInfo(categoryId);
        List<Long> buildingIds = new ArrayList<>();
        for (BuildingMaster buildingMaster : buildingMasters
        ) {
            buildingIds.add(buildingMaster.getBuildingId());
        }
        return buildingIds;
    }

    @PostMapping("actionplan/saveActionPlan")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> saveActionPlan(@RequestBody @Valid ActionPlan actionPlan) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(actionPlanService.saveActionPlan(actionPlan));
    }

    @PostMapping("actionplan/updateActionPlanStatus")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> updateActionPlanStatus(@RequestBody @Valid ActionPlan actionPlan) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(actionPlanService.updateActionPlanStatus(actionPlan));
    }


    private String getFinancialYear() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        if (month <= 3) {
            return (year - 1) + "-" + year;
        } else {
            return year + "-" + (year + 1);
        }
    }
}
