package com.jpmorgan.ffg.careworks.rest.util;

import org.apache.commons.lang3.StringUtils;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;

public class DateUtil {
    public static String getFormattedDate(LocalDate date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        if(date != null){
            return date.format(formatter);
        }
        return StringUtils.EMPTY;
    }

    public static LocalDate getFormattedLocaDate(String date){
        if(date.isEmpty()){
            return LocalDate.now();
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        return LocalDate.parse(date, formatter);
    }
}
