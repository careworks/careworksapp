package com.jpmorgan.ffg.careworks.rest.exception;

public class ResourceNotFoundExceptionSimple extends Throwable {

    public ResourceNotFoundExceptionSimple(String errorMessage) {
        super(errorMessage);
    }
}
