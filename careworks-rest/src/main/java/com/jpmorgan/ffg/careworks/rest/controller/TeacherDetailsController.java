package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.TeacherGroupDetails;
import com.jpmorgan.ffg.careworks.rest.service.SchoolTeacherDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class TeacherDetailsController {

    private final SchoolTeacherDetailsService teacherDetailsDataService;


    @GetMapping("school/teacherGroups/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<TeacherGroupDetails> GetSchoolTeacherDetails(@PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return teacherDetailsDataService.GetSchoolTeacherDetails(schoolId);
    }

    @GetMapping("school/teacherGroups/teacher/{teacherGroupId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public TeacherGroupDetails TeacherGroupDetails(@PathVariable("teacherGroupId") Long teacherGroupId) throws ResourceNotFoundExceptionSimple {
        return teacherDetailsDataService.GetTeacherDetails(teacherGroupId);
    }

    @PostMapping("school/teacherGroup/teacher")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveTeacherDetail(@RequestParam long schoolId, @Valid @RequestBody TeacherGroupDetails teacherGroupDetail) throws ResourceNotFoundExceptionSimple {
        TeacherGroupDetails teacherDetails = teacherDetailsDataService.SaveTeacherDetail(teacherGroupDetail, schoolId);
        return ResponseEntity.ok(teacherDetails);
    }

    @PostMapping("school/teacherGroups/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveTeacherDetails(@PathVariable("school_id") long schoolId, @Valid @RequestBody List<TeacherGroupDetails> teacherGroupDetails) throws ResourceNotFoundExceptionSimple {
        List<TeacherGroupDetails> schoolTeachers = teacherDetailsDataService.SaveSchoolTeacherDetails(teacherGroupDetails.stream().filter(Objects::nonNull).collect(Collectors.toList()), schoolId);
        return ResponseEntity.ok(schoolTeachers);
    }

    @DeleteMapping("school/teacherGroups/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> deleteSchoolTeacherDetails(@RequestBody List<Long> Ids) {
        Ids.forEach(id -> teacherDetailsDataService.deleteSchoolTeacherDetails(id));
        return ResponseEntity.ok(true);
    }
}
