package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.SchoolStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface SchoolStatusDataRepo extends JpaRepository<SchoolStatus, Long> {


    @Query(value = "SELECT ss.* FROM school_status ss WHERE ss.school_id = :school_id", nativeQuery = true)
    SchoolStatus findBySchoolId(Long school_id);


}

