package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface ClassDetailsRepo extends JpaRepository<Section, Long> {

    @Query(value = "SELECT * " +
            "FROM section_info " +
            "WHERE school_id = :school_id", nativeQuery = true)
    List<Section> findAllById(@Param("school_id") Long school_id);

    @Query(value = "SELECT * " +
            "FROM section_info " +
            "WHERE school_id = :school_id AND section_name = :name", nativeQuery = true)
    Section GetSchoolSection(@Param("school_id") Long school_id, @Param("name") String name);

    @Transactional
    @Modifying
    @Query(value = "UPDATE student_info " +
            "SET section_id = :section_Id " +
            "WHERE student_details_id = :student_id", nativeQuery = true)
    void SetSectionMapping(@Param("section_Id") Long section_Id, @Param("student_id") Long student_id);

}
