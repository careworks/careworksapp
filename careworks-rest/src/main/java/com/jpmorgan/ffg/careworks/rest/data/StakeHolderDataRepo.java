package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.StakeholderInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("StakeHolderDataRepository")
public interface StakeHolderDataRepo extends JpaRepository<StakeholderInfo, Long> {
    @Query(value = "SELECT * " +
            "FROM stakeholder_info " +
            "WHERE school_id = :school_id", nativeQuery = true)
    List<StakeholderInfo> FindAllStakeHolderForSchool(@Param("school_id") Long school_id);
}
