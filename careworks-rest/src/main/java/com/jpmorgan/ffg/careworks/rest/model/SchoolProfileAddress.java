package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@Embeddable
@Entity
@NoArgsConstructor
@Table(name = "school_profile_address")
@AllArgsConstructor
@Builder
@JsonPropertyOrder({"addressId", "addressLine1", "streetAddress", "addressLine2", "city", "state", "postalCode", "country"})
public class SchoolProfileAddress implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "address_id", updatable = false, nullable = false)
    private Long addressId;

    @JsonIgnore
    @Column(name = "school_id", insertable = false, updatable = false)
    private Long schoolId;

    @ManyToOne
    @JoinColumn(name = "school_id")
    @JsonIgnore
    private SchoolProfile schoolProfile;

    private String addressLine1;
    private String streetAddress;
    private String addressLine2;
    private String city;
    private String state;
    private String postalCode;
    private String country;

}
