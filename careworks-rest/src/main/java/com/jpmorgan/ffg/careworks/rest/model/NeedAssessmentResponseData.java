package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arijit on 30-Dec-19.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class NeedAssessmentResponseData {
    private Error errors;
    private List<NeedAssessmentResponse> savedData;
    private Map<String, String> fieldValues = new LinkedHashMap<>();
    private List<FormFields> fields;

    public Error getErrors() {
        return errors;
    }

    public void setErrors(Error errors) {
        this.errors = errors;
    }

    public Map<String, String> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Map<String, String> fieldValues) {
        this.fieldValues = fieldValues;
    }

    @JsonAnySetter
    void setDetail(String key, String value) {
        fieldValues.put(key, value);
    }

    public List<NeedAssessmentResponse> getSavedData() {
        return savedData;
    }

    public void setSavedData(List<NeedAssessmentResponse> savedData) {
        this.savedData = savedData;
    }

    public List<FormFields> getFields() {
        return fields;
    }

    public void setFields(List<FormFields> fields) {
        this.fields = fields;
    }
}
