package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.Role;
import com.jpmorgan.ffg.careworks.rest.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("RoleRepository")
public interface RoleDataRepo extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(RoleName roleName);
}

