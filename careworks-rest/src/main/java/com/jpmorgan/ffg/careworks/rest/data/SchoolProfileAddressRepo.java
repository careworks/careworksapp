package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.SchoolProfileAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolProfileAddressRepo extends JpaRepository<SchoolProfileAddress, Long> {
    SchoolProfileAddress findBySchoolId(long schoolId);
}

