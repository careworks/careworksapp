package com.jpmorgan.ffg.careworks.rest.model;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Arijit on 29-Dec-19.
 */
public class NeedAssessmentResponseId implements Serializable {
    @NotNull
    private Long schoolId;
    @NotNull
    private String fieldId;

    public NeedAssessmentResponseId() {

    }

    public NeedAssessmentResponseId(Long schoolId, String fieldId) {
        this.schoolId = schoolId;
        this.fieldId = fieldId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fieldId == null) ? 0 : fieldId.hashCode());
        result = (int) (prime * result + schoolId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        NeedAssessmentResponseId other = (NeedAssessmentResponseId) obj;
        if (fieldId == null && other.fieldId != null) {
            return false;
        } else if (!fieldId.equals(other.fieldId)) {
            return false;
        }

        if (schoolId != other.schoolId) {
            return false;
        }
        return false;
    }
}
