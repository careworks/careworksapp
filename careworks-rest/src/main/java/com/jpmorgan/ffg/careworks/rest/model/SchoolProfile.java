package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jpmorgan.ffg.careworks.rest.annotation.ValidateStatus;
import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "school_profile")
public class SchoolProfile extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "school_id", updatable = false, nullable = false)
    private Long schoolId;

    private String schoolName;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate establishedDate;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "school_id")
    @JsonIgnore
    private Set<SchoolProfileAddress> schoolProfileAddresses;
    @Transient
    private SchoolProfileAddress schoolProfileAddress;

    //    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "location")
//    private Location schoolLocation;
    @Transient
    private Set<String> medium;
    private String mediums;
    private String location;

    private Double longitude;
    private Double latitude;
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "category")
//    private SchoolCategory schoolCategory;

    //    @Transient
    private String category;

//    @ManyToOne()
//    @JoinColumn(name = "type")
//    private SchoolType schoolType;

    //@JsonIgnore
//    @ManyToMany(cascade = {
//            CascadeType.DETACH
//    })
//    @JoinTable(name = "school_profile_medium",
//            joinColumns = {@JoinColumn(name = "school_id")},
//            inverseJoinColumns = {@JoinColumn(name = "medium")})
//    private Set<Medium> schoolMediums;

    private String type;

    @Column(name = "school_managed_by")
    private String schoolManagedBy;


    private String schoolCluster;
    private String block;


    private String diseCode;
    private String emailId;

    @Min(value=0, message="Student Count must be greater than or equal to 0")
    private Integer studentCount;

    //@JsonIgnore

    @Column(name = "any_other_information")
    private String anyOtherInformation;

    //@JsonIgnore

//    public Location getSchoolLocation() {
//        return schoolLocation;
//    }

    private String naFlag;

    @CreatedDate
    private Instant creationDate;

//    public void setSchoolLocation(Location schoolLocation) {
//        this.schoolLocation = schoolLocation;
//    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate na_Action_Date;

    @ValidateStatus
    private String school_status;

    public Long getSchoolId() {
        return schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getSchoolManagedBy() {
        return schoolManagedBy;
    }

//    public SchoolCategory getSchoolCategory() {
//        return schoolCategory;
//    }
//
//    public void setSchoolCategory(SchoolCategory schoolCategory) {
//        this.schoolCategory = schoolCategory;
//    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getMedium() {
        return medium;
    }

    public void setMedium(Set<String> medium) {
        this.medium = medium;
    }

//    public SchoolType getSchoolType() {
//        return schoolType;
//    }
//
//    public void setSchoolType(SchoolType schoolType) {
//        this.schoolType = schoolType;
//    }
//
//    public Set<Medium> getSchoolMediums() {
//        return schoolMediums;
//    }
//
//    public void setSchoolMediums(Set<Medium> schoolMediums) {
//        this.schoolMediums = schoolMediums;
//    }

    public String getNaFlag() {
        return naFlag;
    }

    public void setNaFlag(String naFlag) {
        this.naFlag = naFlag;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getNa_Action_Date() {
        return na_Action_Date;
    }

    public void setNa_Action_Date(LocalDate na_Action_Date) {
        this.na_Action_Date = na_Action_Date;
    }

    public String getSchool_status() {
        return school_status;
    }

    public void setSchool_status(String school_status) {
        this.school_status = school_status;
    }
}
