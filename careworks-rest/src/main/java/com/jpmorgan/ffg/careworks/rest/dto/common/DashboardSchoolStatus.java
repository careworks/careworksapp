package com.jpmorgan.ffg.careworks.rest.dto.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DashboardSchoolStatus {

    private String type;
    private String status;
    private int count;

}
