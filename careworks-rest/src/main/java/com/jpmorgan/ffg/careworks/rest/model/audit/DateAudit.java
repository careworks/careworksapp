package com.jpmorgan.ffg.careworks.rest.model.audit;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
//@JsonIgnoreProperties(
//        value = {"updatedAt"},
//        allowGetters = false
//)
@Getter
@Setter
public abstract class DateAudit implements Serializable {

    @LastModifiedDate
    private Instant updatedAt;

}
