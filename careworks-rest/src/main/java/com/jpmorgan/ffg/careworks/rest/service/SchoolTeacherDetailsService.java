package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.TeacherDetailsDataRepo;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.TeacherGroupDetails;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
@AllArgsConstructor
@Service
public class SchoolTeacherDetailsService {

    private final TeacherDetailsDataRepo teacherDetailsDataRepo;

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;


    public Collection<TeacherGroupDetails> GetSchoolTeacherDetails(Long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        return teacherDetailsDataRepo.findAllById(schoolId);
    }

    public TeacherGroupDetails GetTeacherDetails(Long teacherDetailsId) {
        return teacherDetailsDataRepo.findById(teacherDetailsId).get();
    }

    public TeacherGroupDetails SaveTeacherDetail(TeacherGroupDetails teacherDetails, Long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        teacherDetails.setSchoolId(schoolId);
        return teacherDetailsDataRepo.saveAndFlush(teacherDetails);
    }

    public List<TeacherGroupDetails> SaveSchoolTeacherDetails(List<TeacherGroupDetails> teacherGroupDetails, long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        teacherGroupDetails.forEach(item -> item.setSchoolId(schoolId));
        return teacherDetailsDataRepo.saveAll(teacherGroupDetails);
    }

    public void ValidateSchool(long schoolId) throws ResourceNotFoundExceptionSimple {
        Optional<SchoolProfile> school = basicSchoolProfileRepo.findById(schoolId);
        if (!school.isPresent()) {
            throw new ResourceNotFoundExceptionSimple("No School found for given school ID");
        }
    }


    public void deleteSchoolTeacherDetails(Long Id) {
        teacherDetailsDataRepo.findById(Id).ifPresent(teacherGroupDetails -> {
            try {
                if (GetSchoolTeacherDetails(teacherGroupDetails.getSchoolId()).size() > 1)
                    teacherDetailsDataRepo.delete(teacherGroupDetails);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
        });
    }
}
