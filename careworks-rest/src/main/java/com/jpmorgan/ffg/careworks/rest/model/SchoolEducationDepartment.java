package com.jpmorgan.ffg.careworks.rest.model;

import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "school_education_department")
public class SchoolEducationDepartment extends DateAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "education_department_id", updatable = false, nullable = false)
    private Long educationDepartmentId;

    //    @JsonIgnore
    private Long schoolId;

    private String designation;

    @Pattern(regexp="^[a-zA-Z ]*",message="Name - Please enter valid data in alphabets only")
    private String name;

    @Min(value = 1000000000, message = "Contact Details - Please enter valid data with 10 digits")
    private Long contactDetails;

    @Email(message = "Email Id - Please enter valid data")
    private String emailId;

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
