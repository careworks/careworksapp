package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.data.RoleRepository;
import com.jpmorgan.ffg.careworks.rest.data.UserRepository;
import com.jpmorgan.ffg.careworks.rest.exception.AppException;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundException;
import com.jpmorgan.ffg.careworks.rest.model.Role;
import com.jpmorgan.ffg.careworks.rest.model.RoleName;
import com.jpmorgan.ffg.careworks.rest.model.User;
import com.jpmorgan.ffg.careworks.rest.payload.ApiResponse;
import com.jpmorgan.ffg.careworks.rest.payload.UserIdentityAvailability;
import com.jpmorgan.ffg.careworks.rest.payload.UserProfile;
import com.jpmorgan.ffg.careworks.rest.payload.UserSummary;
import com.jpmorgan.ffg.careworks.rest.security.CurrentUser;
import com.jpmorgan.ffg.careworks.rest.security.UserPrincipal;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class UserController {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        User user = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));
        List<String> roles = user.getRoles().stream().map(Role::getName).map(Enum::toString).collect(Collectors.toList());
        return new UserSummary(user.getId(), user.getUsername(), user.getName(), user.getEmail(), user.getPhone(), user.getUpdatedAt(), String.join(",", roles));
    }

    @GetMapping("/user/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean isAvailable = !userRepository.existsByUsername(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/user/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/users/{username}")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        return new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getUpdatedAt());
    }

    @PostMapping("/users/approve/{role}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> approveUser(@RequestBody List<Long> usernames, @PathVariable String role) {
        List<User> userList = userRepository.findAllById(usernames);
//        User user = userRepository.findById(username)
//                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        for (User user : userList) {
            Set<Role> userRole = user.getRoles();
            userRole.clear();
            if (role.contains("ADMIN")) {
                log.info("Registering user {} as ADMIN!!", user.getUsername());
                userRole.add(roleRepository.findByName(RoleName.ROLE_ADMIN)
                        .orElseThrow(() -> new AppException("User Role not set.")));
            } else if (role.contains("BLOCK")) {
                log.info("Blocking user {} !!", user.getUsername());
                userRole.add(roleRepository.findByName(RoleName.ROLE_UNAUTHORIZED)
                        .orElseThrow(() -> new AppException("User Role not set.")));
            } else {
                userRole.add(roleRepository.findByName(RoleName.ROLE_USER)
                        .orElseThrow(() -> new AppException("User Role not set.")));
            }

            user.setRoles(userRole);
            userRepository.save(user);
        }

        return ResponseEntity.ok(new ApiResponse(true, "Users Approved Successfully"));

    }


    @GetMapping("/user/all")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getAllUsers() {
        List<User> users = userRepository.findAll();
        return ResponseEntity.ok(users.stream().map(user -> new UserSummary(user.getId(), user.getUsername(), user.getName(), user.getEmail(), user.getPhone(), user.getUpdatedAt(), user.getRoles().stream().map(Role::getName).map(Enum::toString).collect(Collectors.joining(",")))).collect(Collectors.toList()));
    }
}
