package com.jpmorgan.ffg.careworks.rest.model;


public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_UNAUTHORIZED
}
