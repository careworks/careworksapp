package com.jpmorgan.ffg.careworks.rest.model;

import java.time.LocalDate;

public interface School {

    Long getSchool_id();


    String getSchool_Name();

    String getSchool_Managed_By();

    String getNa_Flag();

    String getLocation();

    LocalDate getNa_Action_Date();

    String getCreateCompleted();

//    String getCreated_at();

    String getUpdated_at();
}
