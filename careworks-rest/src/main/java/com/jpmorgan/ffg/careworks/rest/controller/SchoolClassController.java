package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.Section;
import com.jpmorgan.ffg.careworks.rest.service.SchoolClassDetailsSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class SchoolClassController {

    private final SchoolClassDetailsSvc schoolClassDetailSvc;

    @GetMapping("/school/ClassDetails/All/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<Section> GetSchoolClassDetails(@PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return schoolClassDetailSvc.GetSchoolClassDetails(schoolId);
    }

    @GetMapping("/school/ClassDetails/{school_id}/section/{sectionId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Section GetClassDetail(@PathVariable("school_id") Long schoolId, @PathVariable("sectionId") Long sectionId) throws ResourceNotFoundExceptionSimple {
        return schoolClassDetailSvc.GetClassDetail(schoolId, sectionId);
    }

    @PostMapping("/school/ClassDetails/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveClassDetail(@PathVariable("school_id") Long schoolId, @Valid @RequestBody Section classSection) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(schoolClassDetailSvc.SaveSchoolSectionSingle(classSection, schoolId));
    }

    @PostMapping("/school/ClassDetails/All/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveSchoolClassDetails(@Valid @RequestBody List<Section> sections, @PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple, Exception {
        return ResponseEntity.ok(schoolClassDetailSvc.SaveSchoolSections(sections, schoolId));
    }

    @DeleteMapping("/school/ClassDetails/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> DeleteClassDetails(@RequestBody List<Long> Ids) {
        Ids.forEach(id -> schoolClassDetailSvc.deleteClassSectionDetails(id));
        return ResponseEntity.ok(true);
    }
}
