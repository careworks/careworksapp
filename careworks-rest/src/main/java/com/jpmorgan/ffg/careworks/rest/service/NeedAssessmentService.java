package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.NeedAssessmentResponseRepo;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponse;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponseData;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NeedAssessmentService {

    private static final Logger logger = LoggerFactory.getLogger(NeedAssessmentService.class);

    private final NeedAssessmentResponseRepo needAssessmentResponseRepo;

    private final SchoolProfileSvc basicSchoolProfilesvc;

    public List<NeedAssessmentResponse> saveNeedAssessment(NeedAssessmentResponseData responseData, long schoolId, String category) {

        if (responseData != null && MapUtils.isEmpty(responseData.getFieldValues())) {
            logger.error("Need Assessment Response not available");
            return new ArrayList<>();
        }

        // Skip validation for category - School
        if (! StringUtils.equalsIgnoreCase("SCHOOL", category) && ! validateNeedAssessmentResponse(responseData, schoolId)) {
            logger.error("All Need Assessment Answers not Present");
            return new ArrayList<>();
        }

        logger.info("Started Saving Need Assessment Response for School Id..." + schoolId);

        List<NeedAssessmentResponse> needAssessmentResponseList = new ArrayList<>();

        List<String> fieldIds = responseData.getFieldValues().keySet().stream().collect(Collectors.toList());

        NeedAssessmentResponseData finalResponseData = responseData;

        fieldIds.stream().forEach(fieldId -> {

            NeedAssessmentResponse needAssessmentResponse = new NeedAssessmentResponse();
            needAssessmentResponse.setFieldId(fieldId);
            needAssessmentResponse.setCategory(category);
            needAssessmentResponse.setFieldValue(finalResponseData.getFieldValues().get(fieldId));
            needAssessmentResponse.setSchoolId(schoolId);

            needAssessmentResponseList.add(needAssessmentResponse);
        });

        logger.info("Started Saving Need Assessment Response..." + needAssessmentResponseList.size());

        List<NeedAssessmentResponse> savedResponse = needAssessmentResponseRepo.saveAll(needAssessmentResponseList);

        if (CollectionUtils.isNotEmpty(savedResponse)) {
            basicSchoolProfilesvc.updateNaFlag(schoolId, "I");
            basicSchoolProfilesvc.updateActionDate(schoolId);
        }

        return savedResponse;
    }

    public boolean validateNeedAssessmentResponse (NeedAssessmentResponseData responseData, long schoolId) {

        if (schoolId == 0) {
            return false;
        }

        Map<String, String> fieldValues = responseData.getFieldValues();
        Set<String> fields = fieldValues.keySet();

        Optional<String> optional = fields.stream().filter(field -> StringUtils.isBlank(fieldValues.get(field))).findFirst();

        if (optional.isPresent()) {
            return false;
        }

        return true;
    }
}