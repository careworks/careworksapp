package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.FormFields;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Arijit on 03-Dec-19.
 */
public interface FormFieldsRepo extends JpaRepository<FormFields, Long> {

    @Query(value="SELECT * FROM form_fields WHERE CATEGORY IN :categories AND IS_ACTIVE = 'Y'", nativeQuery = true)
    List<FormFields> findByCategory(@Param("categories") List<String> categories);
}
