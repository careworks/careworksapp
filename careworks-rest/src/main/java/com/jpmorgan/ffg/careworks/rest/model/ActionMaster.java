package com.jpmorgan.ffg.careworks.rest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ActionMaster implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long actionmasterId;
    private Long categoryId;
    private Long activityId;
    private String stakeholders;
    private boolean money;
    private boolean kind;
    private boolean human_resource;
    private String responsibility;
    private Date deadline;
    private Long contactNumber;
    private String status = "PENDING";
    private String remarks;
    private Date reviewDate;
    private String supportingNgoDonor;
}
