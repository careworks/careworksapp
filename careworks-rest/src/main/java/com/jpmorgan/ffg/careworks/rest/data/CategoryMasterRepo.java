package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.CategoryMaster;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kunal on 6/6/2020.
 */
public interface CategoryMasterRepo extends JpaRepository<CategoryMaster, Long> {
}
