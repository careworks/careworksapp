package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.SchoolEducationDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("EducationDepartmentRepository")
public interface EducationDepartmentRepo extends JpaRepository<SchoolEducationDepartment, Long> {

    @Query(value = "SELECT * " +
            "FROM school_education_department " +
            "WHERE school_id = :school_id", nativeQuery = true)
    List<SchoolEducationDepartment> FindSchoolEducationDepartmentDetails(@Param("school_id") Long school_id);
}
