package com.jpmorgan.ffg.careworks.rest.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StatusValidator.class)
public @interface ValidateStatus {

    String message() default "{Invalid Status Type}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
