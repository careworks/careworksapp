package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.data.ActionPlanRepo;
import com.jpmorgan.ffg.careworks.rest.data.QuotationRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolStatusDataRepo;
import com.jpmorgan.ffg.careworks.rest.dto.common.DashboardSchoolStatus;
import com.jpmorgan.ffg.careworks.rest.dto.schoolprofile.SchoolBasicDetails;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.ActionPlan;
import com.jpmorgan.ffg.careworks.rest.model.Quotation;
import com.jpmorgan.ffg.careworks.rest.model.School;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.SchoolStatus;
import com.jpmorgan.ffg.careworks.rest.service.SchoolProfileSvc;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class SchoolProfileController {


    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm a")
            .withZone(ZoneId.systemDefault());

    private final SchoolProfileSvc basicSchoolProfilesvc;

    private final SchoolStatusDataRepo statusRepo;

    private final QuotationRepo quotationRepo;

    private final ActionPlanRepo actionPlanRepo;


    @GetMapping("/school/SchoolProfiles/{Id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolProfile GetSchoolProfileBasic(@PathVariable("Id") Long schoolId) {
        return basicSchoolProfilesvc.GetSchoolProfileBasic(schoolId);
    }

    @GetMapping("/school/{Id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolBasicDetails getSchoolDetailsById(@PathVariable("Id") Long schoolId) {
        return basicSchoolProfilesvc.getSchoolProfileBasicV1(schoolId);
    }

    @GetMapping("/school/SchoolProfiles/fields/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolProfile GetSchoolProfileFields() {
        return basicSchoolProfilesvc.generateSchoolProfileFormData();
    }

    @GetMapping("/school/SchoolProfiles/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> GetAllSchoolProfileBasic(@RequestParam(required = false) String completed) {
        LinkedList<SchoolProfile> schoolProfiles = basicSchoolProfilesvc.GetAllSchoolProfileBasic();
        LinkedList<Map<String, String>> schoolResponses = new LinkedList<>();
        try {
            schoolProfiles.forEach(schoolProfile -> {
                        Map<String, String> school = new LinkedHashMap<>();
                        school.put("school_id", schoolProfile.getSchoolId() + "");
                        school.put("diseCode", schoolProfile.getDiseCode());
                        school.put("school_Name", schoolProfile.getSchoolName());
                        school.put("yearOfEstablishment", schoolProfile.getEstablishedDate() != null ? schoolProfile.getEstablishedDate().getYear() + "" : "");
                        school.put("category", schoolProfile.getCategory().split("-")[0]);
                        school.put("total", schoolProfile.getStudentCount() + "");
                        school.put("cluster", schoolProfile.getSchoolCluster());
                        school.put("block", schoolProfile.getBlock());
                        school.put("city", schoolProfile.getSchoolProfileAddress() != null ? schoolProfile.getSchoolProfileAddress().getCity() : "");
                        school.put("status", schoolProfile.getSchool_status());
                        school.put("na_Action_Date", schoolProfile.getNa_Action_Date() != null ? schoolProfile.getNa_Action_Date().toString() : "Not Started");
                        school.put("onboardingyear", (schoolProfile.getCreationDate() != null || schoolProfile.getUpdatedAt() != null) ?
                                DATE_TIME_FORMATTER.format(schoolProfile.getCreationDate() == null ? schoolProfile.getUpdatedAt() : schoolProfile.getCreationDate())
                                        .split("-")[2].split(" ")[0] : "");
                        school.put("updated_at", schoolProfile.getUpdatedAt() != null ? DATE_TIME_FORMATTER.format(schoolProfile.getUpdatedAt()) : "");
                        school.put("na_Flag", schoolProfile.getNaFlag());
                        String quotStatus = Optional.ofNullable(quotationRepo.findFirstBySchoolIdOrderByUpdatedAtDesc(schoolProfile.getSchoolId()))
                                .map(quotations -> quotations.stream().findFirst().map(Quotation::getStatus).orElse("Not Started")).orElse("Not Started");

                        String actionPlanStatus = Optional.ofNullable(actionPlanRepo.findFirstBySchoolIdOrderByUpdatedAtDesc(schoolProfile.getSchoolId()))
                                .map(actionPlans -> actionPlans.stream().findFirst().map(ActionPlan::getStatus).orElse("Not Started")).orElse("Not Started");

                        school.put("ap_status", actionPlanStatus);

                        school.put("quot_status",quotStatus);
                        // Need Assessment flow, required only fully completed school profiles
                        if (StringUtils.equals("Y", completed)) {
                            if (StringUtils.equals("ON_BOARDED", schoolProfile.getSchool_status()))
                                schoolResponses.add(school);
                        } else {
                            schoolResponses.add(school);
                        }
                    }
            );

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok(schoolResponses);
    }

    @PostMapping("/school/SchoolProfiles/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> AddSchoolProfileBasic(@Valid @RequestBody SchoolProfile schoolProfile) {
        SchoolProfile profile = basicSchoolProfilesvc.AddSchoolProfileBasic(schoolProfile);
        return ResponseEntity.ok(profile);
    }

    @GetMapping("/school/SchoolProfilesByName/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<School> GetSchoolProfileByName(@RequestParam String name) {
        return basicSchoolProfilesvc.GetSchoolProfileByName(name);
    }

    @GetMapping("/status/getSchoolStatus/{Id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolStatus getSchoolStatus(@PathVariable("Id") Long schoolId) {
        SchoolStatus status = statusRepo.findBySchoolId(schoolId);
        if (status == null) {
            status = SchoolStatus.defaultSchoolStatus();
        }
        return status;
    }

    @PostMapping("/status/saveSchoolStatus")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolStatus saveSchoolStatus(@Valid @RequestBody SchoolStatus schoolStatus) {
        return statusRepo.save(schoolStatus);
    }

    @PostMapping("/updateNaFlag/{school_id}/{naFlag}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity updateNaFlag(@PathVariable(value = "school_id") Long schoolId,
                                       @PathVariable(value = "naFlag") String naFlag) {
        int result = basicSchoolProfilesvc.updateNaFlag(schoolId, naFlag);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/school/finalsubmit/{school_id}/{status}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> finalSubmitSchoolData(@PathVariable(value = "school_id") Long schoolId,
                                                   @PathVariable(value = "status") String status) throws Exception, ResourceNotFoundExceptionSimple {

        SchoolProfile profile = basicSchoolProfilesvc.updateSchoolStatus(schoolId, status);
        return ResponseEntity.ok(profile);
    }

    @PostMapping("/school/saveschooldata")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolData(@RequestBody SchoolBasicDetails schoolBasicDetails) {
        return ResponseEntity.ok(basicSchoolProfilesvc.saveSchoolData(schoolBasicDetails));
    }

    @GetMapping("/school/getAllStatus")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Map<String, List<DashboardSchoolStatus>> getAllStatus() {

        return basicSchoolProfilesvc.getAllSchoolStatus();
    }
}
