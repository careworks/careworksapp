package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.School;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedList;

@Repository
public interface BasicSchoolProfileRepo extends JpaRepository<SchoolProfile, Long> {

    @Query(value = "SELECT school_id,school_name,school_managed_by FROM school_profile " +
            "WHERE school_name LIKE LOWER(CONCAT('%',:name, '%'))", nativeQuery = true)
    Collection<School> findByMatchingNames(@Param("name") String name);

    @Query(value = "SELECT sp.* FROM school_profile sp WHERE sp.school_id = :school_id", nativeQuery = true)
    SchoolProfile findBySchoolId(@Param("school_id") Long school_id);

    LinkedList<SchoolProfile> findAllByOrderByUpdatedAtDesc();

    @Transactional
    @Modifying
    @Query(value = "update school_profile sp set sp.na_flag = :na_flag where sp.school_id = :school_id", nativeQuery = true)
    int updateNaFlag(@Param("na_flag") String naFlag, @Param("school_id") Long school_id);

    @Transactional
    @Modifying
    @Query(value = "update school_profile sp set sp.na_action_date =  CURDATE() where sp.school_id = :school_id", nativeQuery = true)
    int updateActionDate(@Param("school_id") Long school_id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE school_profile_address " +
            "SET school_id = :school_Id " +
            "WHERE address_id = :address_id", nativeQuery = true)
    void setSchoolAddressMapping(@Param("school_Id") Long school_Id, @Param("address_id") Long address_id);
}

