package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.BuildingMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BuildingMasterRepo extends JpaRepository<BuildingMaster, Long> {
    @Query(value = "SELECT * " +
            "FROM building_master " +
            "WHERE category_id = :category_id", nativeQuery = true)
    List<BuildingMaster> findAllById(Long category_id);
}
