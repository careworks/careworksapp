package com.jpmorgan.ffg.careworks.rest.model;

import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "school_club_details")
public class SchoolClubDetails extends DateAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "school_club_details_id", updatable = false, nullable = false)
    private Long schoolClubId;

    //    @JsonIgnore
    @Column(name = "school_id", updatable = false, nullable = false)
    private Long schoolId;

    @Column(name = "name_of_the_club", nullable = false)
    private String nameOfTheClub;

    @Min(value = 0, message = "Members count cannot be less than zero")
    private Long totalMembers;
    @Min(value = 1000, message = "Formation Year - Please enter valid data in format YYYY")
    @Max(value = 9999, message = "Formation Year - Please enter valid data in format YYYY")
    private Long formationYear;
    @Pattern(regexp="^[a-zA-Z ]*",message="Spoc Name - Please enter valid data in alphabets only")
    private String spocName;

    @Min(value = 1000000000, message = "Contact Number - Please enter valid data with 10 digits")
    private Long contactNumber;

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

}
