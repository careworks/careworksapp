package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.data.NeedAssessmentResponseRepo;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponse;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponseData;
import com.jpmorgan.ffg.careworks.rest.service.NeedAssessmentService;
import com.jpmorgan.ffg.careworks.rest.service.SchoolProfileSvc;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class NeedAssessmentController {

    private final NeedAssessmentService service;

    private final SchoolProfileSvc basicSchoolProfilesvc;

    private final NeedAssessmentResponseRepo needAssessmentResponseRepo;

    private static final Logger logger = LoggerFactory.getLogger(NeedAssessmentController.class);

    @PostMapping("/saveNeedAssessment/{school_id}/{category}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public List<NeedAssessmentResponse> saveNeedAssessment(@Valid @RequestBody NeedAssessmentResponseData needAssessmentResponseData,
                                                      @PathVariable(value = "school_id") long schoolId, @PathVariable(value = "category") String category) {
        return service.saveNeedAssessment(needAssessmentResponseData, schoolId, category);
    }

    @GetMapping("/getNeedAssessmentResponse/{school_id}/{category}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public List<NeedAssessmentResponse> getNeedAssessmentResponse(@PathVariable(value = "school_id") long schoolId,
                                                                  @PathVariable(value = "category") String category) {
        return needAssessmentResponseRepo.findBySchoolId(schoolId, category);
    }

    @PostMapping("/submitNA/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity submitNA(@PathVariable(value = "school_id") Long schoolId){
        List<String> naCategoriesSaved = needAssessmentResponseRepo.fetchDistinctCategoriesResponded(schoolId);

        if (naCategoriesSaved.size() < 3) {
            logger.error("Health & Classroom categories must be completed first...");
            return ResponseEntity.ok(false);
        }

        List<NeedAssessmentResponse> responseList = needAssessmentResponseRepo.NaValidation(schoolId);
        if(CollectionUtils.isNotEmpty(responseList))
        {
            for(NeedAssessmentResponse response : responseList)
            {
                logger.error("Missing value for Field Value : " + response.getFieldValue() + "\n in Category : " +response.getCategory());

                // By-pass validation for School Environment
                if (StringUtils.equalsIgnoreCase("SCHOOL", response.getCategory())) {
                    continue;
                }
            }

            logger.error("VALIDATION FAILED");
            return ResponseEntity.ok(false);
        }
        else
        {
            logger.info("VALIDATION SUCCESS");
            basicSchoolProfilesvc.updateNaFlag(schoolId,"S");
            basicSchoolProfilesvc.updateActionDate(schoolId);
            return ResponseEntity.ok(true);
        }
    }

    @PostMapping("/naRejected/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity naRejected(@PathVariable(value = "school_id") Long schoolId){
        basicSchoolProfilesvc.updateNaFlag(schoolId,"I");
        basicSchoolProfilesvc.updateActionDate(schoolId);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/naApproved/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity naApproved(@PathVariable(value = "school_id") Long schoolId){
        List<NeedAssessmentResponse> responseList = needAssessmentResponseRepo.NaValidation(schoolId);
        if(CollectionUtils.isNotEmpty(responseList))
        {
            for(NeedAssessmentResponse response : responseList)
            {
                logger.error("Missing value for Field Value : " + response.getFieldValue() + "\n in Category : " +response.getCategory());

                // By-pass validation for School Environment
                if (StringUtils.equalsIgnoreCase("SCHOOL", response.getCategory())) {
                    continue;
                }
            }
            logger.error("VALIDATION FAILED");

            return ResponseEntity.ok(false);
        }
        else
        {
            logger.info("VALIDATION SUCCESS");
            basicSchoolProfilesvc.updateNaFlag(schoolId,"Y");
            basicSchoolProfilesvc.updateActionDate(schoolId);
            return ResponseEntity.ok(true);
        }
    }
}