package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.model.FormFields;
import com.jpmorgan.ffg.careworks.rest.service.FormFieldsService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Arijit on 03-Dec-19.
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/api/formfields")
public class FormFieldsController {

    private final FormFieldsService formFieldsService;

    @GetMapping("/getAllFieldsByCategory")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public List<FormFields> getAllFieldsByCategory(@RequestParam String category) {
        List<FormFields> allFields = formFieldsService.getAllFieldsByCategory(StringUtils.upperCase(category));
        return allFields;
    }
}
