package com.jpmorgan.ffg.careworks.rest.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.*;
import com.jpmorgan.ffg.careworks.rest.service.ActionPlanService;
import com.jpmorgan.ffg.careworks.rest.service.FormFieldsService;
import com.jpmorgan.ffg.careworks.rest.service.ReferenceDataService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api")
public class AdminController {

    private final ReferenceDataService referenceDataService;
    private final FormFieldsService formFieldsService;
    private final ActionPlanService actionPlanService;

    @PostMapping("/admin/medium/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolMediums(@PathVariable Boolean action, @RequestBody List<Medium> mediums) {
        mediums.forEach(medium -> referenceDataService.saveMedium(medium, action));
        return ResponseEntity.ok(mediums);
    }

    @GetMapping("/admin/medium")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getSchoolMedium() {
        return ResponseEntity.ok(referenceDataService.getAllMediums());
    }


    @PostMapping("/admin/location/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolLocations(@PathVariable Boolean action, @RequestBody List<Location> locations) {
        locations.forEach(location -> referenceDataService.saveLocation(location, action));
        return ResponseEntity.ok(locations);
    }

    @GetMapping("/admin/location")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getSchoolLocation() {
        return ResponseEntity.ok(referenceDataService.getAllLocation());
    }

    @PostMapping("/admin/type/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolTypes(@PathVariable Boolean action, @RequestBody List<SchoolType> schoolTypes) {
        schoolTypes.forEach(schoolType -> referenceDataService.saveType(schoolType, action));
        return ResponseEntity.ok(schoolTypes);
    }

    @GetMapping("/admin/type")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getSchoolType() {
        return ResponseEntity.ok(referenceDataService.getAllTypes());
    }

    @PostMapping("/admin/category/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolCategory(@PathVariable Boolean action, @RequestBody List<SchoolCategory> schoolCategories) {
        schoolCategories.forEach(schoolCategory -> referenceDataService.saveCategory(schoolCategory, action));
        return ResponseEntity.ok(schoolCategories);
    }

    @GetMapping("/admin/category")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getSchoolCategory() {
        return ResponseEntity.ok(referenceDataService.getAllCategories());
    }

    @PostMapping("/admin/questions/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveSchoolQuestion(@RequestBody List<Map<String, String>> formFields, @PathVariable Boolean action) {
        //formFields.forEach(formFields1 -> formFieldsService.saveQuestion(formFields1, action));
        log.info("Got form fields {}", formFields);
        for (Map<String, String> formField : formFields) {
            FormFields fields = new FormFields();
            formField.forEach((key, value) ->
                    {
                        if (StringUtils.isNotBlank(value))
                            switch (key) {
                                case "seq":
                                    fields.setSeq(Long.parseLong(value));
                                    break;
                                case "category":
                                    fields.setCategory(value);
                                    break;
                                case "subCategory":
                                    fields.setSubCategory(value);
                                    break;
                                case "fieldSection":
                                    fields.setFieldSection(value);
                                    break;
                                case "parentFieldId":
                                    fields.setParentField(value);
                                    break;
                                case "fieldId":
                                    fields.setFieldId(value);
                                    break;
                                case "fieldHeader":
                                    fields.setFieldSectionHeader(value);
                                    break;
                                case "fieldName":
                                    fields.setFieldLabel(value);
                                    break;
                                case "fieldType":
                                    fields.setFieldType(value);
                                    break;
                                case "fieldValidation":
                                    fields.setFieldValidation(value);
                                    break;
                                case "childFieldNumber":
                                    fields.setChildSeq(Long.parseLong(value));
                                    break;
                                case "fieldOptionsPositiveAndNegative":
                                    String[] options = value.split(",");
                                    fields.setFieldOptions("[{\"key\":\"" + options[0] + "\",\"value\":\"Y\"},{\"key\":\"" + options[1] + "\",\"value\":\"N\"}]");
                            }
                    }
            );
            formFieldsService.saveQuestion(fields, action);
        }

        return ResponseEntity.ok(formFields);
    }

    @GetMapping("/admin/questions")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getSchoolQuestions() {
        List<Map<String, String>> formattedFields = new LinkedList<>();
        List<FormFields> formFields = formFieldsService.getAllFields();

        formFields.forEach(formField -> {
            Map<String, String> field = new LinkedHashMap<>();
            field.put("seq", formField.getSeq() + "");
            field.put("category", formField.getCategory() == null ? "" : formField.getCategory());
            field.put("subCategory", formField.getSubCategory() == null ? "" : formField.getSubCategory());
            field.put("fieldSection", formField.getFieldSection() == null ? "" : formField.getFieldSection());
            field.put("parentFieldId", formField.getParentField() != null ? formField.getParentField() : "");
            field.put("fieldId", formField.getFieldId() == null ? "" : formField.getFieldId());
            field.put("fieldHeader", formField.getFieldSectionHeader() == null ? "" : formField.getFieldSectionHeader());
            field.put("fieldName", formField.getFieldLabel() == null ? "" : formField.getFieldLabel());
            field.put("fieldType", formField.getFieldType() == null ? "" : formField.getFieldType());
            field.put("fieldValidation", formField.getFieldValidation() == null ? "" : formField.getFieldValidation());
            field.put("childFieldNumber", formField.getChildSeq() == null ? "" : "" + formField.getChildSeq());
            if (StringUtils.isNotBlank(formField.getFieldOptions())) {
                List<HashMap<String, String>> fieldOptionsMap = new Gson().fromJson(formField.getFieldOptions(), new TypeToken<List<HashMap<String, String>>>() {
                }.getType());
                StringBuilder fieldOptions = new StringBuilder();
                fieldOptionsMap.forEach(valuePair -> valuePair.forEach((s, s2) -> {
                    if (s.equals("key"))
                        fieldOptions.append(s2).append(",");
                }));
                field.put("fieldOptionsPositiveAndNegative", fieldOptions.toString().substring(0, fieldOptions.toString().length() - 1));
            }
            formattedFields.add(field);
        });

        return ResponseEntity.ok(formattedFields);
    }

    @GetMapping("/admin/getAllCodes")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getAllCodes() throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(referenceDataService.getAllCodes());
    }

    @GetMapping("/admin/getCodesByType/{type}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> getCodesByType(@PathVariable String type) throws ResourceNotFoundExceptionSimple {
        if (StringUtils.isBlank(type)) {
            return ResponseEntity.ok(new ArrayList<>());
        }
        return ResponseEntity.ok(referenceDataService.getCodesByType(type));
    }

    @PostMapping("/admin/codes/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveCodes(@PathVariable Boolean action, @RequestBody List<Codes> codes) {
        codes.forEach(code -> referenceDataService.saveCodes(code, action));
        return ResponseEntity.ok(codes);
    }

    @PostMapping("/admin/activities/{action}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> saveActivities(@PathVariable Boolean action, @RequestBody List<ActivityMaster> activities) throws Exception {
        Map<String, Long> categoryMap = actionPlanService.getCategories().stream().collect(Collectors.toMap(CategoryMaster::getCategoryDetails, CategoryMaster::getCategoryId));
        if(activities.stream().filter(activityMaster -> !categoryMap.containsKey(activityMaster.getCategoryName())).findFirst().isPresent()) {
            throw new Exception("Category Name is not valid");
        }
        activities.forEach(activity -> {
            activity.setCategoryId(categoryMap.get(activity.getCategoryName()));
            actionPlanService.saveActivity(activity, action);
        });
        return ResponseEntity.ok(activities);
    }
}
