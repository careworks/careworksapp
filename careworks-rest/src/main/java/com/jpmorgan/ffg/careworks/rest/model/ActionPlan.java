package com.jpmorgan.ffg.careworks.rest.model;

import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ActionPlan extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long actionplanId;

    private String financialYear;

    private long schoolId;

    private String status;

    @ElementCollection
    @OneToMany(cascade = {CascadeType.ALL, CascadeType.MERGE}, orphanRemoval = true)
    @JoinColumn(name = "actionplan_id")
    private List<ActionMaster> actionMasters;

}
