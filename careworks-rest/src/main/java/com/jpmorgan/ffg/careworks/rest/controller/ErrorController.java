package com.jpmorgan.ffg.careworks.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class ErrorController {

    @RequestMapping(method = {RequestMethod.OPTIONS, RequestMethod.GET}, path = {"/error/**", "/error", "/not-found"})
    public String forwardReactPaths() {
        return "forward:/";
    }
}
