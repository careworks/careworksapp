package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.jpmorgan.ffg.careworks.rest.model.audit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Embeddable
@Entity
@NoArgsConstructor
@Table(name = "section_info")
@JsonPropertyOrder({"sectionId", "schoolId", "standard", "medium", "sectionName", "studentDetails"})
public class Section extends DateAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "section_Id", updatable = false, nullable = false)
    private Long sectionId;

    @Column(name = "school_Id")
    private Long schoolId;

    private String standard;

    private String medium;

    @Column(name = "section_name")
    private String sectionName;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "section_Id")
    private Set<StudentDetails> studentDetails;

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

}
