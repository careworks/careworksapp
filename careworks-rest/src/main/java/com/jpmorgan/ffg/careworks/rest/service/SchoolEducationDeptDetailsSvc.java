package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.EducationDepartmentRepo;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolEducationDepartment;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@AllArgsConstructor
@Service
public class SchoolEducationDeptDetailsSvc {

    private final EducationDepartmentRepo educationDepartmentRepo;

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    public Collection<SchoolEducationDepartment> GetSchoolEducationDepartments(Long schoolId) throws ResourceNotFoundExceptionSimple {
        validateSchoolId(schoolId);
        return educationDepartmentRepo.FindSchoolEducationDepartmentDetails(schoolId);
    }

    public List<SchoolEducationDepartment> SaveSchoolEducationDepartment(List<SchoolEducationDepartment> educationDepartmentDetails, long schoolId) throws ResourceNotFoundExceptionSimple {
        return educationDepartmentDetails.stream().map(item -> {
            SchoolEducationDepartment savedData = new SchoolEducationDepartment();
            try {
                savedData = saveEducationDepartment(schoolId, item);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
            return savedData;
        }).collect(Collectors.toList());
    }

    private void validateSchoolId(long schoolId) throws ResourceNotFoundExceptionSimple {
        Optional<SchoolProfile> school = basicSchoolProfileRepo.findById(schoolId);
        if (!school.isPresent()) {
            throw new ResourceNotFoundExceptionSimple("No School found for given school ID");
        }
    }

    public SchoolEducationDepartment getEducationDepartmentByName(String name, Long schoolId) throws ResourceNotFoundExceptionSimple {
        validateSchoolId(schoolId);
        Collection<SchoolEducationDepartment> educationDepartments = educationDepartmentRepo.FindSchoolEducationDepartmentDetails(schoolId);
        return educationDepartments.stream().filter(x -> x.getName().equals(name)).findFirst().get();
    }

    public SchoolEducationDepartment saveEducationDepartment(Long schoolId, SchoolEducationDepartment educationDepartment) throws ResourceNotFoundExceptionSimple {
        validateSchoolId(schoolId);
        educationDepartment.setSchoolId(schoolId);
        return educationDepartmentRepo.saveAndFlush(educationDepartment);
    }


    public void deleteEducationDepartmentDetails(Long id) {
        educationDepartmentRepo.findById(id).ifPresent(schoolEducationDepartment -> {
            try {
                if (GetSchoolEducationDepartments(schoolEducationDepartment.getSchoolId()).size() > 1)
                    educationDepartmentRepo.delete(schoolEducationDepartment);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
        });
    }
}
