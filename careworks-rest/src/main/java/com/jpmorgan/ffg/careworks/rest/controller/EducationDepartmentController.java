package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolEducationDepartment;
import com.jpmorgan.ffg.careworks.rest.service.SchoolEducationDeptDetailsSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class EducationDepartmentController {

    private final SchoolEducationDeptDetailsSvc schoolEducationDeptDetailsSvc;


    @GetMapping("/school/educationDepartments/All/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Collection<SchoolEducationDepartment> GetSchoolEduationDepartment(@PathVariable("school_id") Long schoolId) throws ResourceNotFoundExceptionSimple {
        return schoolEducationDeptDetailsSvc.GetSchoolEducationDepartments(schoolId);
    }

    @GetMapping("/school/educationDepartments/{school_id}/name/{name}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolEducationDepartment GetSchoolEduationDepartmentByName(@PathVariable("school_id") Long schoolId, @PathVariable("name") String name) throws ResourceNotFoundExceptionSimple {
        return schoolEducationDeptDetailsSvc.getEducationDepartmentByName(name, schoolId);
    }

    @PostMapping("/school/educationDepartment/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolEducationDepartment SaveSchoolEduationDepartmentByName(@PathVariable("school_id") Long schoolId, @Valid @RequestBody SchoolEducationDepartment educationDepartments) throws ResourceNotFoundExceptionSimple {
        return schoolEducationDeptDetailsSvc.saveEducationDepartment(schoolId, educationDepartments);
    }

    @PostMapping("/school/educationDepartments/All/{school_id}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> SaveEducationDepartments(@Valid @RequestBody List<SchoolEducationDepartment> educationDepartments, @PathVariable("school_id") long schoolId) throws ResourceNotFoundExceptionSimple {
        return ResponseEntity.ok(schoolEducationDeptDetailsSvc.SaveSchoolEducationDepartment(educationDepartments.stream().filter(Objects::nonNull).collect(Collectors.toList()), schoolId));
    }

    @DeleteMapping("/school/educationDepartments/")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public ResponseEntity<?> DeleteEducationDepartment(@RequestBody List<Long> Ids) {
        Ids.forEach(id -> schoolEducationDeptDetailsSvc.deleteEducationDepartmentDetails(id));
        return ResponseEntity.ok(true);
    }

    @GetMapping("/school/getdefaulteducationdeptdata")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public SchoolEducationDepartment getDefaultEducationDeptData() throws ResourceNotFoundExceptionSimple {
        return new SchoolEducationDepartment();
    }
}
