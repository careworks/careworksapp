package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.data.RoleRepository;
import com.jpmorgan.ffg.careworks.rest.data.UserRepository;
import com.jpmorgan.ffg.careworks.rest.exception.AppException;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundException;
import com.jpmorgan.ffg.careworks.rest.model.Role;
import com.jpmorgan.ffg.careworks.rest.model.RoleName;
import com.jpmorgan.ffg.careworks.rest.model.User;
import com.jpmorgan.ffg.careworks.rest.payload.ApiResponse;
import com.jpmorgan.ffg.careworks.rest.payload.JwtAuthenticationResponse;
import com.jpmorgan.ffg.careworks.rest.payload.LoginRequest;
import com.jpmorgan.ffg.careworks.rest.payload.SignUpRequest;
import com.jpmorgan.ffg.careworks.rest.payload.UserIdentityAvailability;
import com.jpmorgan.ffg.careworks.rest.security.JwtTokenProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtTokenProvider tokenProvider;


    private static final Map<String, String> forgotPasswordUserBase = new ConcurrentHashMap<>();


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws Exception {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getLogin(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<? extends GrantedAuthority> authority = authentication.getAuthorities().stream().findFirst();
        if (authority.isPresent() && authority.get().getAuthority().contains("UN")) {
            throw new Exception("Unauthorized");
        } else if (!authority.isPresent()) {
            throw new Exception("Unauthorized");
        }
        String jwt = tokenProvider.generateToken(authentication);
        forgotPasswordUserBase.remove(loginRequest.getLogin());
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, String.join(",", authority.get().getAuthority())));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPhone(), signUpRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        HashSet<Role> userRole = new HashSet<>();
        if (userRepository.count() == 0) {
            log.info("Registering user {} as ADMIN!!", user.getUsername());

            userRole.add(roleRepository.findByName(RoleName.ROLE_ADMIN)
                    .orElseThrow(() -> new AppException("User Role not set.")));
        } else
            userRole.add(roleRepository.findByName(RoleName.ROLE_UNAUTHORIZED)
                    .orElseThrow(() -> new AppException("User Role not set.")));

        user.setRoles(userRole);

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "SignUp Request Submitted Successfully"));
    }

    @PostMapping("/validatetoken")
    public ResponseEntity<?> validateToken(@RequestBody String token) {
        return ResponseEntity.ok(tokenProvider.validateToken(token));
    }


    @PostMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestParam(value = "otp") String otp, @RequestBody LoginRequest loginRequest) throws Exception {
        User user = userRepository.findByUsername(loginRequest.getLogin())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", loginRequest.getLogin()));
        if (forgotPasswordUserBase.get(user.getEmail()) != null && forgotPasswordUserBase.get(user.getEmail()).equals(otp)) {
            user.setPassword(passwordEncoder.encode(loginRequest.getPassword()));
            userRepository.save(user);
            forgotPasswordUserBase.remove(user.getEmail());
            return ResponseEntity.ok(new ApiResponse(true, "Password Updated"));
        } else
            throw new Exception("User has not requested change password");
    }


    @PostMapping("/forgotPassword")
    public UserIdentityAvailability forgotPassword(@RequestBody LoginRequest loginRequest) {
        Boolean isAvailable = userRepository.existsByUsername(loginRequest.getLogin());
        if (isAvailable) {
            forgotPasswordUserBase.put(loginRequest.getLogin(), "true");
        }
        return new UserIdentityAvailability(isAvailable);
    }


    @PostMapping("/approvePassword/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> approvePassword(@PathVariable Long userId) throws Exception {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

        if (forgotPasswordUserBase.get(user.getEmail()) != null) {
            forgotPasswordUserBase.replace(user.getEmail(), RandomStringUtils.randomNumeric(6));
            return ResponseEntity.ok(new ApiResponse(true, forgotPasswordUserBase.get(user.getEmail())));
        } else
            throw new Exception("User has not requested change password");
    }
}
