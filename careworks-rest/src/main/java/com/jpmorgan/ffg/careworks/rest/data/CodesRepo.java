package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.Codes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Arijit on 08-May-20.
 */
@Repository
public interface CodesRepo extends JpaRepository<Codes, Long> {
}
