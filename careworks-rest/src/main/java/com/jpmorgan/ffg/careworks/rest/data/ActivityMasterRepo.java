package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.ActivityMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityMasterRepo extends JpaRepository<ActivityMaster, Long> {
    @Query(value = "SELECT * " +
            "FROM activity_master " +
            "WHERE category_id = :category_id ", nativeQuery = true)
    List<ActivityMaster> findAllById(Long category_id);

    //@Query(value = "SELECT a.*,c.category_details as category_name FROM activity_master a, category_master c WHERE c.category_id = a.category_id", nativeQuery = true)
    List<ActivityMaster> findAll();
}
