package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.*;
import com.jpmorgan.ffg.careworks.rest.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ReferenceDataService {

    @Autowired
    private SchoolCategoryDataRepo schoolCategoryDataRepo;

    @Autowired
    private SchoolMediumDataRepo schoolMediumDataRepo;

    @Autowired
    private SchoolTypeDataRepo schoolTypeDataRepo;

    @Autowired
    private LocationDataRepo locationDataRepo;

    @Autowired
    private CodesRepo codesRepo;

    public void saveMedium(Medium medium, boolean action) {
        if (action) {
            schoolMediumDataRepo.saveAndFlush(medium);
        } else {
            if (schoolMediumDataRepo.count() > 1)
                schoolMediumDataRepo.deleteById(medium.getSeq());
        }
    }

    public Set<Medium> findMediumByName(Set<String> mediums) {
        Collection<Medium> schoolMediums = schoolMediumDataRepo.findAll();
        return schoolMediums.stream().filter(x -> {
            return mediums.stream().anyMatch(x.getMedium()::equalsIgnoreCase);
        }).collect(Collectors.toSet());
    }

    public Optional<Medium> findMedium(String medium) {
        Collection<Medium> schoolMediums = schoolMediumDataRepo.findAll();
        return schoolMediums.stream().filter(x -> x.getMedium().equalsIgnoreCase(medium)).findFirst();
    }

    public Optional<SchoolCategory> findCategoryByName(String category) {
        Collection<SchoolCategory> categories = schoolCategoryDataRepo.findAll();
        return categories.stream().filter(x -> x.getCategory().equalsIgnoreCase(category)).findFirst();
    }

    public Optional<SchoolType> findTypeByName(String type) {
        Collection<SchoolType> types = schoolTypeDataRepo.findAll();
        return types.stream().filter(x -> x.getType().equalsIgnoreCase(type)).findFirst();
    }

    public Optional<Location> findLocationByName(String location) {
        Collection<Location> locations = locationDataRepo.findAll();
        return locations.stream().filter(x -> x.getLocation().equalsIgnoreCase(location)).findFirst();
    }

    public Collection<SchoolCategory> getAllCategories() {
        return schoolCategoryDataRepo.findAll();
    }

    public Collection<Medium> getAllMediums() {
        return schoolMediumDataRepo.findAll();
    }

    public Collection<SchoolType> getAllTypes() {
        return schoolTypeDataRepo.findAll();
    }

    public Collection<Location> getAllLocation() {
        return locationDataRepo.findAll();
    }

    public void saveLocation(Location location, boolean action) {
        if (action) {
            locationDataRepo.saveAndFlush(location);

        } else {
            if (locationDataRepo.count() > 1)
                locationDataRepo.deleteById(location.getSeq());
        }
    }

    public void saveType(SchoolType schoolType, boolean action) {
        if (action) {
            schoolTypeDataRepo.saveAndFlush(schoolType);

        } else {
            if (schoolTypeDataRepo.count() > 1)
                schoolTypeDataRepo.deleteById(schoolType.getSeq());
        }
    }

    public void saveCategory(SchoolCategory result, boolean action) {
        if (action) {
            schoolCategoryDataRepo.saveAndFlush(result);
        } else {
            if (schoolCategoryDataRepo.count() > 1)
                schoolCategoryDataRepo.deleteById(result.getSeq());
        }
    }

    @Cacheable(value = "codesCache")
    public Collection<Codes> getAllCodes() {
        return codesRepo.findAll();
    }

    @CacheEvict(cacheNames  = {"codesCache"}, allEntries = true)
    public void saveCodes(Codes codes, boolean action) {
        if (action) {
            codesRepo.saveAndFlush(codes);
        } else {
            if (codesRepo.count() > 1) {
                codesRepo.deleteById(codes.getSeq());
            }
        }
    }

    public List<Codes> getCodesByType(String type) {
        return codesRepo.findAll().stream().filter(codes ->
                StringUtils.equals(type, codes.getType())).collect(Collectors.toList());
    }
}
