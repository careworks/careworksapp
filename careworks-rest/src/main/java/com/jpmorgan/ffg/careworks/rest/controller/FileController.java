package com.jpmorgan.ffg.careworks.rest.controller;

import com.jpmorgan.ffg.careworks.rest.data.FormFieldsRepo;
import com.jpmorgan.ffg.careworks.rest.data.NeedAssessmentResponseRepo;
import com.jpmorgan.ffg.careworks.rest.model.FormFields;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponse;
import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponseExport;
import com.jpmorgan.ffg.careworks.rest.util.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/file")
@Slf4j
public class FileController {

    private static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    NeedAssessmentResponseRepo needAssessmentResponseRepo;

    @Autowired
    FormFieldsRepo formFieldsRepo;

    @GetMapping("/export/na/{schoolId}")
    @PreAuthorize("hasRole('USER') || hasRole('ADMIN')")
    public Workbook exportNaBySchoolId(@PathVariable long schoolId, HttpServletResponse response) throws Exception {

            List<NeedAssessmentResponse> allResponses = needAssessmentResponseRepo.findAllResponseBySchoolId(schoolId);

            List<FormFields> formFields = formFieldsRepo.findByCategory(Arrays.asList("HEALTH", "CLASSROOM", "SCHOOL"));

            List<NeedAssessmentResponseExport> exportList = new ArrayList<>();

            for (NeedAssessmentResponse needAssessmentResponse : allResponses) {

                Optional<FormFields> matchedFields = formFields.stream().filter(field -> StringUtils.equalsIgnoreCase(needAssessmentResponse.getFieldId(),
                        field.getFieldId())).findFirst();

                if (!matchedFields.isPresent() || needAssessmentResponse == null) {
                    continue;
                }

                FormFields matchedField = matchedFields.get();

                NeedAssessmentResponseExport needAssessmentResponseExport = new NeedAssessmentResponseExport();

                needAssessmentResponseExport.setSchoolId(schoolId);

                needAssessmentResponseExport.setCategory(StringUtils.defaultIfBlank(matchedField.getCategory(), StringUtils.EMPTY));
                needAssessmentResponseExport.setSubCategory(StringUtils.defaultIfBlank(matchedField.getSubCategory(), StringUtils.EMPTY));
                needAssessmentResponseExport.setFieldSection(StringUtils.defaultIfBlank(matchedField.getFieldSection(), StringUtils.EMPTY));
                needAssessmentResponseExport.setFieldSectionHeader(StringUtils.defaultIfBlank(matchedField.getFieldSectionHeader(), StringUtils.EMPTY));
                needAssessmentResponseExport.setFieldLabel(StringUtils.defaultIfBlank(matchedField.getFieldLabel(), StringUtils.EMPTY));

                needAssessmentResponseExport.setFieldValue(StringUtils.defaultIfBlank(needAssessmentResponse.getFieldValue(), StringUtils.EMPTY));

                exportList.add(needAssessmentResponseExport);
            }

            response.setHeader("Content-Disposition", "attachment; filename=NeedAssessmentResponse.xlsx");
            response.setContentType("application/vnd.ms-excel");

            //return ResponseEntity.ok(ExcelUtil.prepareNeedAssessmentExport(exportList, response.getOutputStream()));
        logger.info("Exported Rows : " + allResponses.size());

        return ExcelUtil.prepareNeedAssessmentExport(exportList, response.getOutputStream());
    }
}