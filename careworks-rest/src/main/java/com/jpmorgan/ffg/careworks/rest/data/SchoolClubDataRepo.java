package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.SchoolClubDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("SchoolClubDataRepository")
public interface SchoolClubDataRepo extends JpaRepository<SchoolClubDetails, Long> {
    @Query(value = "SELECT * " +
            "FROM school_club_details " +
            "WHERE school_id = :school_id", nativeQuery = true)
    List<SchoolClubDetails> findAllClubsBySchool(@Param("school_id") Long school_id);
}
