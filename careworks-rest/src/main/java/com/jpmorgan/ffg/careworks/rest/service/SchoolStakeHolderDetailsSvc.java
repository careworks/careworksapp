package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.StakeHolderDataRepo;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.StakeholderInfo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
@AllArgsConstructor
@Service
public class SchoolStakeHolderDetailsSvc {

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    private final StakeHolderDataRepo stakeHolderDataRepo;


    public Collection<StakeholderInfo> GetStakeHolderDetailsForSchool(Long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        return stakeHolderDataRepo.FindAllStakeHolderForSchool(schoolId);
    }

    public List<StakeholderInfo> SaveSchoolStakeHolders(List<StakeholderInfo> stakeholderInfos, long schoolId) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        stakeholderInfos.forEach(item -> {item.setSchoolId(schoolId);stakeHolderDataRepo.saveAndFlush(item);});
        return stakeholderInfos;
    }

    public StakeholderInfo GetStakeHolderDetailsById(Long stakeHolderDetailId) throws ResourceNotFoundExceptionSimple {
        return stakeHolderDataRepo.findById(stakeHolderDetailId).get();
    }

    public StakeholderInfo SaveStakeHolderDetails(Long schoolId, StakeholderInfo stakeHolder) throws ResourceNotFoundExceptionSimple {
        ValidateSchool(schoolId);
        stakeHolder.setSchoolId(schoolId);
        return stakeHolderDataRepo.saveAndFlush(stakeHolder);
    }


    private void ValidateSchool(long schoolId) throws ResourceNotFoundExceptionSimple {
        Optional<SchoolProfile> school = basicSchoolProfileRepo.findById(schoolId);
        if (!school.isPresent()) {
            throw new ResourceNotFoundExceptionSimple("No School found for given school ID");
        }
    }

    public void DeleteSchoolStakeHolderDetails(Long id) {
        stakeHolderDataRepo.findById(id).ifPresent(stakeholderInfo -> {
            try {
                if (GetStakeHolderDetailsForSchool(stakeholderInfo.getSchoolId()).size() > 1)
                    stakeHolderDataRepo.delete(stakeholderInfo);
            } catch (ResourceNotFoundExceptionSimple resourceNotFoundExceptionSimple) {
                resourceNotFoundExceptionSimple.printStackTrace();
            }
        });
    }
}
