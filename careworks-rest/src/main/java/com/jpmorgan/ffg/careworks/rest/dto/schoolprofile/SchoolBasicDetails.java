package com.jpmorgan.ffg.careworks.rest.dto.schoolprofile;

import com.jpmorgan.ffg.careworks.rest.dto.common.DropdownVo;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class SchoolBasicDetails {
    private long schoolId;
    private String schoolName;
    private LocalDate establishedDate;
    private String addressLine1;
    private String addressLine2;
    private String streetAddress;
    private String city;
    private String postalCode;
    private String state;
    private String country;
    private String cluster;
    private String block;
    private String diseCode;
    private String emailId;
    private int studentCount;
    private String otherInfo;
    private String status;
    private List<String> mediums;
    private List<DropdownVo> mediumsList;
    private String location;
    private List<DropdownVo> locationList;
    private String category;
    private List<DropdownVo> categoryList;
    private String type;
    private List<DropdownVo> typeList;
    private String managedBy;
    private List<DropdownVo> managedByList;
    private String updatedAt;
    private Double latitude;
    private Double longitude;
    private String mapsApiKey;
}
