package com.jpmorgan.ffg.careworks.rest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Arijit on 08-May-20.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "codes")
public class Codes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seq;
    private String type;
    private String code;
    private String description;
}
