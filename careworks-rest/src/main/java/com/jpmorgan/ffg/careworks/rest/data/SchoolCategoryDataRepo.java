package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.SchoolCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("SchoolCategoryDataRepo")
public interface SchoolCategoryDataRepo extends JpaRepository<SchoolCategory, Long> {

}
