package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.ActionPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ActionPlanRepo extends JpaRepository<ActionPlan, Long> {

    List<ActionPlan> findAllBySchoolId(Long schoolId);

    @Transactional
    @Modifying
    @Query(value = "update action_plan set status = :status where actionplan_id = :actionplanId", nativeQuery = true)
    int updateActionPlanStatus(@Param("status") String status, @Param("actionplanId") Long actionplanId);

    List<ActionPlan> findFirstBySchoolIdOrderByUpdatedAtDesc(Long schoolId);
}
