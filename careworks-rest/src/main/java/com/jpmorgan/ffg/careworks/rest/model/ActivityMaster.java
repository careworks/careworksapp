package com.jpmorgan.ffg.careworks.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Activity_Master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"categoryId", "activityId"})
})
@Data
public class ActivityMaster implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long activityId;

    private long categoryId;

    @Transient
    private String categoryName;

    private String activityDetail;

}
