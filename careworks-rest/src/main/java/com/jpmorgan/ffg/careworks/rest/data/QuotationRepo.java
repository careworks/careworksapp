package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.Quotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Arijit on 27-Feb-20.
 */
@Repository
public interface QuotationRepo extends JpaRepository<Quotation, Long> {

    //    @Query(value = "SELECT * FROM quotation WHERE school_id = :schoolId ", nativeQuery = true)
    List<Quotation> findQuotationsBySchoolId(@Param("schoolId") Long schoolId);

    //    @Query(value = "SELECT * FROM quotation WHERE action_id = :actionId ", nativeQuery = true)
    List<Quotation> findQuotationsByActionId(@Param("actionId") Long actionId);

    List<Quotation> findFirstBySchoolIdOrderByUpdatedAtDesc(Long schoolId);

}
