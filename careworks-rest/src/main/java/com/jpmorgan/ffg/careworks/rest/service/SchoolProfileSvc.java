package com.jpmorgan.ffg.careworks.rest.service;

import com.jpmorgan.ffg.careworks.rest.dao.school.ISchoolDao;
import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.dto.common.DashboardSchoolStatus;
import com.jpmorgan.ffg.careworks.rest.dto.common.DropdownVo;
import com.jpmorgan.ffg.careworks.rest.dto.schoolprofile.SchoolBasicDetails;
import com.jpmorgan.ffg.careworks.rest.exception.ResourceNotFoundExceptionSimple;
import com.jpmorgan.ffg.careworks.rest.model.Location;
import com.jpmorgan.ffg.careworks.rest.model.Medium;
import com.jpmorgan.ffg.careworks.rest.model.School;
import com.jpmorgan.ffg.careworks.rest.model.SchoolCategory;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfileAddress;
import com.jpmorgan.ffg.careworks.rest.model.SchoolType;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class SchoolProfileSvc {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm a")
            .withZone(ZoneId.systemDefault());
    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    private static final double defaultLongitude = 77.5946;
    private static final double defaultLatitude = 12.9716;

    private final ISchoolDao schoolDao;

    private final ReferenceDataService refrenceDataSvc;

    private final SchoolEducationDeptDetailsSvc schoolEducationDeptDetailsSvc;

    private final SchoolStakeHolderDetailsSvc schoolStakeHolderDetailsSvc;

    private final SchoolClassDetailsSvc schoolClassDetailsSvc;

    private final SchoolTeacherDetailsService schoolTeacherDetailsService;

    @Value("${maps.key:AIzaSyCjO9dnLwqClC-mUHU88wvO_1J5YOj34aU}")
    private String mapsApiKey;

    public SchoolProfile GetSchoolProfileBasic(Long schoolId) {
        if (schoolId == null) {
            throw new IllegalArgumentException("School id is null");
        }
        SchoolProfile profile = basicSchoolProfileRepo.findBySchoolId(schoolId);
        enrichProfileFromRead(profile);

        return profile;
    }

    public SchoolBasicDetails getSchoolProfileBasicV1(Long schoolId) {
        if (schoolId == null) {
            throw new IllegalArgumentException("School id is null");
        }
        SchoolProfile profile = schoolDao.getSchoolProfileDataBySchoolId(schoolId);
        SchoolBasicDetails schoolBasicDetails = new SchoolBasicDetails();
        schoolBasicDetails.setMapsApiKey(mapsApiKey);
        buildDropDowns(schoolBasicDetails);
        if (profile != null) {
            schoolBasicDetails.setSchoolName(profile.getSchoolName());
            schoolBasicDetails.setBlock(profile.getBlock());
            schoolBasicDetails.setCluster(profile.getSchoolCluster());
            schoolBasicDetails.setDiseCode(profile.getDiseCode());
            schoolBasicDetails.setSchoolId(profile.getSchoolId());
            schoolBasicDetails.setEmailId(profile.getEmailId());
            schoolBasicDetails.setEstablishedDate(profile.getEstablishedDate());
            schoolBasicDetails.setLocation(profile.getLocation());
            schoolBasicDetails.setCategory(profile.getCategory());
            schoolBasicDetails.setType(profile.getType());
            schoolBasicDetails.setManagedBy(profile.getSchoolManagedBy());
            schoolBasicDetails.setStatus(profile.getSchool_status());
            schoolBasicDetails.setStudentCount(profile.getStudentCount());
            schoolBasicDetails.setMediums(profile.getMediums() != null ? Arrays.stream(profile.getMediums().split(",")).collect(Collectors.toList()) : new ArrayList<>());
            schoolBasicDetails.setOtherInfo(profile.getAnyOtherInformation());
            schoolBasicDetails.setLatitude(profile.getLatitude() != null ? profile.getLatitude() : defaultLatitude);
            schoolBasicDetails.setLongitude(profile.getLongitude() != null ? profile.getLongitude() : defaultLongitude);

            Optional<SchoolProfileAddress> optSchoolProfileAddress = profile.getSchoolProfileAddresses().stream().findFirst();
            if (optSchoolProfileAddress.isPresent()) {
                SchoolProfileAddress schoolProfileAddress = optSchoolProfileAddress.get();
                schoolBasicDetails.setAddressLine1(schoolProfileAddress.getAddressLine1());
                schoolBasicDetails.setAddressLine2(schoolProfileAddress.getAddressLine2());
                schoolBasicDetails.setCity(schoolProfileAddress.getCity());
                schoolBasicDetails.setStreetAddress(schoolProfileAddress.getStreetAddress());
                schoolBasicDetails.setPostalCode(schoolProfileAddress.getPostalCode());
                schoolBasicDetails.setCountry(schoolProfileAddress.getCountry());
                schoolBasicDetails.setState(schoolProfileAddress.getState());
            }
            schoolBasicDetails.setUpdatedAt(DATE_TIME_FORMATTER.format(profile.getUpdatedAt()));
        }
        return schoolBasicDetails;
    }

    private void buildDropDowns(SchoolBasicDetails schoolBasicDetails) {
        // Medium
        List<Medium> mediums = schoolDao.getAllAvailableMediumsList();
        schoolBasicDetails.setMediumsList(mediums.stream().map(item -> {
            DropdownVo dropdownVo = new DropdownVo();
            dropdownVo.setLabel(item.getMedium());
            return dropdownVo;
        }).collect(Collectors.toList()));

        //location
        List<Location> locations = schoolDao.getAllAvailableLocation();
        schoolBasicDetails.setLocationList(locations.stream().map(item -> {
            DropdownVo dropdownVo = new DropdownVo();
            dropdownVo.setLabel(item.getLocation() + " - " + item.getDescription());
            return dropdownVo;
        }).collect(Collectors.toList()));

        // category
        List<SchoolCategory> categories = schoolDao.getAllAvailableSchoolCategoryList();
        schoolBasicDetails.setCategoryList(categories.stream().map(item -> {
            DropdownVo dropdownVo = new DropdownVo();
            dropdownVo.setLabel(item.getCategory() + " - " + item.getDescription());
            return dropdownVo;
        }).collect(Collectors.toList()));

        //type
        List<SchoolType> types = schoolDao.getAllAvailableSchoolTypeList();
        schoolBasicDetails.setTypeList(types.stream().map(item -> {
            DropdownVo dropdownVo = new DropdownVo();
            dropdownVo.setLabel(item.getType() + " - " + item.getDescription());
            return dropdownVo;
        }).collect(Collectors.toList()));

        //managed by
        schoolBasicDetails.setManagedByList(Stream.of("Government", "Government Aided", "Private", "Municipal Corporation").map(item -> {
            DropdownVo dropdownVo = new DropdownVo();
            dropdownVo.setLabel(item);
            return dropdownVo;
        }).collect(Collectors.toList()));
    }

    public SchoolProfile generateSchoolProfileFormData() {

        return SchoolProfile.builder()
                .schoolId((long) 1)
                .schoolName("")
                .establishedDate(LocalDate.now())
                .location(refrenceDataSvc.getAllLocation().stream().map(location -> location.getLocation() + " - " + location.getDescription()).collect(Collectors.joining(",")))
                .schoolManagedBy("Government,Government Aided,Municipal Corporation")
                .schoolCluster("")
                .schoolProfileAddress(SchoolProfileAddress.builder()
                        .addressLine1("")
                        .addressLine2("")
                        .city("")
                        .country("")
                        .postalCode("")
                        .state("")
                        .streetAddress("")
                        .build())
                .block("")
                .category(refrenceDataSvc.getAllCategories().stream().map(schoolCategory -> schoolCategory.getCategory() + " - " + schoolCategory.getDescription()).collect(Collectors.joining(",")))
                .type(refrenceDataSvc.getAllTypes().stream().map(schoolType -> schoolType.getType() + " - " + schoolType.getDescription()).collect(Collectors.joining(",")))
                .medium(refrenceDataSvc.getAllMediums().stream().map(Medium::getMedium).collect(Collectors.toSet()))
                .diseCode("")
                .emailId("@")
                .studentCount(5)
                .anyOtherInformation("")
                .build();
    }

    private void enrichProfileFromRead(SchoolProfile profile) {
//        if (profile.getSchoolType() != null) {
//            profile.setType(profile.getSchoolType().getType());
//        }
//        if (profile.getSchoolCategory() != null) {
//            profile.setCategory(profile.getSchoolCategory().getCategory());
//        }
//
//        if (profile.getSchoolLocation() != null) {
//            profile.setLocation(profile.getSchoolLocation().getLocation());
//        }
//
//        if (profile.getSchoolMediums() != null) {
//            profile.setMedium(profile.getSchoolMediums().stream().map(Medium::getMedium).collect(Collectors.toSet()));
//        }
        if (profile.getMediums() != null) {
            profile.setMedium(new HashSet<>(Arrays.asList(profile.getMediums().split(","))));
        }
        if (profile.getSchoolProfileAddresses() != null && !profile.getSchoolProfileAddresses().isEmpty()) {
            profile.setSchoolProfileAddress(profile.getSchoolProfileAddresses().stream().findAny().get());
        } else {
            profile.setSchoolProfileAddress(SchoolProfileAddress.builder()
                    .addressLine1("")
                    .addressLine2("")
                    .city("")
                    .country("")
                    .postalCode("")
                    .state("")
                    .streetAddress("")
                    .build());
        }
    }

    public LinkedList<SchoolProfile> GetAllSchoolProfileBasic() {
        LinkedList<SchoolProfile> profiles = basicSchoolProfileRepo.findAllByOrderByUpdatedAtDesc();
        profiles.forEach(this::enrichProfileFromRead);
        return profiles;
    }

    private void EnrichSavedSchoolProfile(@RequestBody @Valid SchoolProfile schoolProfile) {
//        if (schoolProfile.getCategory() != null && !schoolProfile.getCategory().isEmpty()) {
//            Optional<SchoolCategory> category = refrenceDataSvc.findCategoryByName(schoolProfile.getCategory());
//            category.ifPresent(schoolProfile::setSchoolCategory);
//        }
//
//        if (schoolProfile.getLocation() != null && !schoolProfile.getLocation().isEmpty()) {
//            refrenceDataSvc.findLocationByName(schoolProfile.getLocation()).ifPresent(schoolProfile::setSchoolLocation);
//        }
//
//        if (schoolProfile.getType() != null && !schoolProfile.getType().isEmpty()) {
//            Optional<SchoolType> type = refrenceDataSvc.findTypeByName(schoolProfile.getType());
//            type.ifPresent(schoolProfile::setSchoolType);
//        }
//
//        if (schoolProfile.getMedium() != null && schoolProfile.getMedium().stream().findAny().isPresent()) {
//            Set<Medium> mediums = refrenceDataSvc.findMediumByName(schoolProfile.getMedium());
//            schoolProfile.setSchoolMediums(mediums);
//        }
        // At the time of creating school profile
        if (schoolProfile.getMedium() != null && !schoolProfile.getMedium().isEmpty()) {
            schoolProfile.setMediums(String.join(",", schoolProfile.getMedium()));
        }
        schoolProfile.setNaFlag("N");

        if (schoolProfile.getSchoolProfileAddress() != null) {
            SchoolProfileAddress schoolProfileAddress = schoolProfile.getSchoolProfileAddress();
            schoolProfileAddress.setSchoolId(schoolProfile.getSchoolId());
            schoolProfile.setSchoolProfileAddresses(new HashSet<>(Collections.singletonList(schoolProfileAddress)));
        }
    }

    public SchoolProfile AddSchoolProfileBasic(SchoolProfile schoolProfile) {
        EnrichSavedSchoolProfile(schoolProfile);
        SchoolProfile profile = basicSchoolProfileRepo.saveAndFlush(schoolProfile);
        schoolProfile.getSchoolProfileAddresses().stream().filter(Objects::nonNull).forEach(schoolProfileAddress -> basicSchoolProfileRepo.setSchoolAddressMapping(profile.getSchoolId(), schoolProfileAddress.getAddressId()));
        enrichProfileFromRead(profile);
        return profile;
    }

    public Collection<School> GetSchoolProfileByName(String name) {
        return basicSchoolProfileRepo.findByMatchingNames(name);
    }

    public int updateNaFlag(Long schoolId, String flag) {
        return basicSchoolProfileRepo.updateNaFlag(flag, schoolId);
    }

    public int updateActionDate(Long schoolId) {
        int result = basicSchoolProfileRepo.updateActionDate(schoolId);
        return result;
    }

    public SchoolProfile updateSchoolStatus(Long schoolId, String status) throws ResourceNotFoundExceptionSimple, Exception {
        SchoolProfile schoolProfile = basicSchoolProfileRepo.findBySchoolId(schoolId);
        if (schoolProfile != null) {
            String validation = validateSpSubmit(schoolId);
            if (StringUtils.isNotBlank(validation)) {
                throw new Exception(validation + " is/are incomplete or incorrect");
            }
            schoolProfile.setSchool_status(status);
            return basicSchoolProfileRepo.saveAndFlush(schoolProfile);
        }
        return null;
    }

    private String validateSpSubmit(Long schoolId) throws ResourceNotFoundExceptionSimple {
        StringBuilder stringBuilder = new StringBuilder();
        boolean eduStatus = schoolEducationDeptDetailsSvc.GetSchoolEducationDepartments(schoolId).stream()
                .filter(Objects::nonNull)
                .anyMatch(schoolEducationDepartment ->
                                StringUtils.isNotBlank(schoolEducationDepartment.getName()) &&
                                StringUtils.isNotBlank(schoolEducationDepartment.getDesignation()) &&
                                (StringUtils.isNotBlank(schoolEducationDepartment.getEmailId()) ||
                                        schoolEducationDepartment.getContactDetails() != null));
        if (!eduStatus) {
            stringBuilder.append("Education Department , ");
        }

        boolean stakeHldStatus = schoolStakeHolderDetailsSvc.GetStakeHolderDetailsForSchool(schoolId).stream()
                .filter(Objects::nonNull)
                .anyMatch(stakeholderInfo ->
                        stakeholderInfo.getContactNumber() != null &&
                                stakeholderInfo.getFormationYear() != null &&
                                stakeholderInfo.getMemberCount() != null &&
                                StringUtils.isNotBlank(stakeholderInfo.getSpocName()) &&
                                StringUtils.isNotBlank(stakeholderInfo.getStakeHolderName()));
        if (!stakeHldStatus) {
            stringBuilder.append("Stake Holder Details , ");
        }

        boolean classStatus = schoolClassDetailsSvc.GetSchoolClassDetails(schoolId).stream().filter(Objects::nonNull)
                .anyMatch(section -> StringUtils.isNotBlank(section.getMedium()) && StringUtils.isNotBlank(section.getSectionName()) && StringUtils.isNotBlank(section.getStandard()));

        if (!classStatus)
            stringBuilder.append("School Class details , ");

        boolean teacherStatus = schoolTeacherDetailsService.GetSchoolTeacherDetails(schoolId).stream().filter(Objects::nonNull)
                .anyMatch(teacherGroupDetails -> StringUtils.isNotBlank(teacherGroupDetails.getTeacher())
                        && teacherGroupDetails.getMale() != null &&
                        teacherGroupDetails.getFemale() != null &&
                        teacherGroupDetails.getTeacherCount() != null);

        if (!teacherStatus)
            stringBuilder.append("Teachers details");

        return stringBuilder.toString();
    }

    public SchoolBasicDetails saveSchoolData(SchoolBasicDetails schoolBasicDetails) {
        SchoolProfile schoolProfile = basicSchoolProfileRepo.findBySchoolId(schoolBasicDetails.getSchoolId());
        if (schoolProfile != null) {
            saveSchoolProfileData(schoolBasicDetails, schoolProfile);
        } else {
            saveSchoolProfileData(schoolBasicDetails, new SchoolProfile());
        }
        return schoolBasicDetails;
    }

    private void saveSchoolProfileData(SchoolBasicDetails schoolBasicDetails, SchoolProfile newSchoolProfiile) {
        newSchoolProfiile.setSchoolName(schoolBasicDetails.getSchoolName());
        newSchoolProfiile.setEmailId(schoolBasicDetails.getEmailId());
        newSchoolProfiile.setMediums(String.join(",", schoolBasicDetails.getMediums()));
        newSchoolProfiile.setDiseCode(schoolBasicDetails.getDiseCode());
        newSchoolProfiile.setAnyOtherInformation(schoolBasicDetails.getOtherInfo());
        newSchoolProfiile.setBlock(schoolBasicDetails.getBlock());
        newSchoolProfiile.setType(schoolBasicDetails.getType());
        newSchoolProfiile.setCategory(schoolBasicDetails.getCategory());
        newSchoolProfiile.setEstablishedDate(schoolBasicDetails.getEstablishedDate());
        newSchoolProfiile.setLocation(schoolBasicDetails.getLocation());
        newSchoolProfiile.setSchoolCluster(schoolBasicDetails.getCluster());
        newSchoolProfiile.setSchoolManagedBy(schoolBasicDetails.getManagedBy());
        newSchoolProfiile.setStudentCount(schoolBasicDetails.getStudentCount());
        newSchoolProfiile.setSchool_status(schoolBasicDetails.getStatus());
        newSchoolProfiile.setNaFlag(newSchoolProfiile.getNaFlag() == null ? "N" : newSchoolProfiile.getNaFlag());
        if (schoolBasicDetails.getLatitude() == null) {
            schoolBasicDetails.setLatitude(defaultLatitude);
        }
        if (schoolBasicDetails.getLongitude() == null) {
            schoolBasicDetails.setLongitude(defaultLongitude);
        }
        newSchoolProfiile.setLatitude(schoolBasicDetails.getLatitude());
        newSchoolProfiile.setLongitude(schoolBasicDetails.getLongitude());
        SchoolProfileAddress schoolProfileAddress = buildSchoolProfileAddressData(schoolBasicDetails, newSchoolProfiile, schoolBasicDetails.getSchoolId());
        newSchoolProfiile.setSchoolProfileAddress(schoolProfileAddress);
        newSchoolProfiile.setSchoolProfileAddresses(new HashSet<>(Collections.singletonList(schoolProfileAddress)));
        SchoolProfile savedData = this.schoolDao.saveSchoolData(newSchoolProfiile);
        if (schoolBasicDetails.getSchoolId() == 0) {
            savedData.getSchoolProfileAddresses().stream().filter(Objects::nonNull)
                    .forEach(spAdd -> basicSchoolProfileRepo.setSchoolAddressMapping(savedData.getSchoolId(), spAdd.getAddressId()));
            schoolBasicDetails.setSchoolId(savedData.getSchoolId());
        }
    }

    private SchoolProfileAddress buildSchoolProfileAddressData(SchoolBasicDetails schoolBasicDetails, SchoolProfile schoolProfile, long schoolId) {
        SchoolProfileAddress schoolProfileAddress = null;
        if (schoolBasicDetails != null) {
            if (schoolId == 0) {
                schoolProfileAddress = SchoolProfileAddress.builder()
                        .addressLine1(schoolBasicDetails.getAddressLine1())
                        .addressLine2(schoolBasicDetails.getAddressLine2())
                        .streetAddress(schoolBasicDetails.getStreetAddress())
                        .city(schoolBasicDetails.getCity())
                        .country(schoolBasicDetails.getCountry())
                        .state(schoolBasicDetails.getState())
                        .postalCode(schoolBasicDetails.getPostalCode())
                        .build();
            } else {
                Optional<SchoolProfileAddress> optSchoolProfileAddress = schoolProfile.getSchoolProfileAddresses().stream().findFirst();
                if (optSchoolProfileAddress.isPresent()) {
                    schoolProfileAddress = optSchoolProfileAddress.get();
                    schoolProfileAddress.setSchoolId(schoolId);
                    schoolProfileAddress.setAddressLine1(schoolBasicDetails.getAddressLine1());
                    schoolProfileAddress.setAddressLine2(schoolBasicDetails.getAddressLine2());
                    schoolProfileAddress.setCity(schoolBasicDetails.getCity());
                    schoolProfileAddress.setCountry(schoolBasicDetails.getCountry());
                    schoolProfileAddress.setPostalCode(schoolBasicDetails.getPostalCode());
                    schoolProfileAddress.setState(schoolBasicDetails.getState());
                    schoolProfileAddress.setStreetAddress(schoolBasicDetails.getStreetAddress());
                }
            }
        }
        return schoolProfileAddress;
    }

    private void saveSchoolProfileAddress(SchoolBasicDetails schoolBasicDetails, long schoolId, SchoolProfileAddress schoolProfileAddress) {

        this.schoolDao.saveSchoolAddress(schoolProfileAddress);
    }

    public Map<String, List<DashboardSchoolStatus>> getAllSchoolStatus() {
        List<DashboardSchoolStatus> statusList = schoolDao.getAllSchoolStatus();
        LinkedList<DashboardSchoolStatus> lstSchoolStatuses = new LinkedList<>();
        List<DashboardSchoolStatus> profile = statusList.stream().filter(dashboardSchoolStatus -> StringUtils.equalsIgnoreCase(dashboardSchoolStatus.getType(), "Schools")).collect(Collectors.toList());
        List<DashboardSchoolStatus> na = statusList.stream().filter(dashboardSchoolStatus -> StringUtils.equalsIgnoreCase(dashboardSchoolStatus.getType(), "Need Assessment")).collect(Collectors.toList());
        List<DashboardSchoolStatus> ap = statusList.stream().filter(dashboardSchoolStatus -> StringUtils.equalsIgnoreCase(dashboardSchoolStatus.getType(), "Action Plan")).collect(Collectors.toList());
        List<DashboardSchoolStatus> quot = statusList.stream().filter(dashboardSchoolStatus -> StringUtils.equalsIgnoreCase(dashboardSchoolStatus.getType(), "Quotation")).collect(Collectors.toList());
        lstSchoolStatuses.addAll(profile);
        lstSchoolStatuses.addAll(na);
        lstSchoolStatuses.addAll(ap);
        lstSchoolStatuses.addAll(quot);
        LinkedHashMap<String, List<DashboardSchoolStatus>> statusMap = new LinkedHashMap<>();
        lstSchoolStatuses.forEach(dashboardSchoolStatus -> statusMap.put(dashboardSchoolStatus.getType(), lstSchoolStatuses.stream().filter(dashboardSchoolStatus1 -> dashboardSchoolStatus.getType().equals(dashboardSchoolStatus1.getType())).collect(Collectors.toList())));
//        lstSchoolStatuses.stream()
//                .collect(Collectors.groupingBy(DashboardSchoolStatus::getType));

        return statusMap;
    }
}
