package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.ActionMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ActionMasterRepo extends JpaRepository<ActionMaster, Long> {
    @Query(value = "SELECT * " +
            "FROM action_master " +
            "WHERE actionplan_id = :actionplanId ", nativeQuery = true)
    List<ActionMaster> findAllById(@Param("actionplanId") Long actionplanId);
}
