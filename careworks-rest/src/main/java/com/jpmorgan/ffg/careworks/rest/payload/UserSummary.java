package com.jpmorgan.ffg.careworks.rest.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@Getter
@Setter
public class UserSummary {
    private Long id;
    private String username;
    private String name;
    private String email;
    private String phone;
    private Instant updatedAt;
    private String role;
}
