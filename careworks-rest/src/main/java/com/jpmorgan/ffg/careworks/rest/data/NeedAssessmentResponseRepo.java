package com.jpmorgan.ffg.careworks.rest.data;

import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Arijit on 28-Dec-19.
 */
@Repository
public interface NeedAssessmentResponseRepo extends JpaRepository<NeedAssessmentResponse, Integer> {

    @Query(value = "SELECT nar.* FROM need_assessment_response nar WHERE nar.school_id = :schoolId and category = :category ", nativeQuery = true)
    List<NeedAssessmentResponse> findBySchoolId(@Param("schoolId") Long schoolId, @Param("category") String category);

    @Query(value = "SELECT * FROM need_assessment_response WHERE CATEGORY in ('HEALTH','CLASSROOM') and SCHOOL_ID = :schoolId and (FIELD_VALUE is NULL or FIELD_VALUE = '') ",nativeQuery = true)
    List<NeedAssessmentResponse> NaValidation(@Param("schoolId") Long schoolId);

    @Query(value = "SELECT DISTINCT CATEGORY FROM need_assessment_response WHERE SCHOOL_ID = :schoolId ",nativeQuery = true)
    List<String> fetchDistinctCategoriesResponded (@Param("schoolId") Long schoolId);

    @Query(value = "SELECT nar.* FROM need_assessment_response nar WHERE nar.school_id = :schoolId ", nativeQuery = true)
    List<NeedAssessmentResponse> findAllResponseBySchoolId(@Param("schoolId") Long schoolId);
}
