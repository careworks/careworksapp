package com.jpmorgan.ffg.careworks.rest.util;

import com.jpmorgan.ffg.careworks.rest.model.NeedAssessmentResponseExport;
import org.apache.poi.ss.usermodel.*;
import org.springframework.core.io.ClassPathResource;

import java.io.OutputStream;
import java.util.List;

public class ExcelUtil {

    public static Workbook prepareNeedAssessmentExport(List<NeedAssessmentResponseExport> exportList, OutputStream outputStream) throws Exception {
        Workbook workbook = null;

        try {
            workbook = WorkbookFactory.create(new ClassPathResource("templates/NeedAssessmentResponse.xlsx").getInputStream());

            Sheet sheet = workbook.getSheet("Export");

            int rowNum = 1;

            for (NeedAssessmentResponseExport data : exportList) {

                Row row = sheet.getRow(rowNum);

                if (row == null) {
                    row = sheet.createRow(rowNum);
                }

                int cell = 0;

                setCell(row, cell++, Long.toString(data.getSchoolId()));
                setCell(row, cell++, data.getCategory());
                setCell(row, cell++, data.getSubCategory());
                setCell(row, cell++, data.getFieldSection());
                setCell(row, cell++, data.getFieldSectionHeader());
                setCell(row, cell++, data.getFieldLabel());
                setCell(row, cell++, data.getFieldValue());

                rowNum ++;
            }
            workbook.write(outputStream);
            return workbook;
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
    }

    public static void setCell(Row row, int column, String value) {
        Cell cell = row.getCell(column);

        if (cell == null) {
            cell = row.createCell(column);
        }

        cell.setCellValue(value);
    }

}