package com.jpmorgan.ffg.careworks.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name = "medium")
public class Medium {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seq;

    private String medium;

    private String description;

}
