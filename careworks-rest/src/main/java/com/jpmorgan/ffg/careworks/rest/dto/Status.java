package com.jpmorgan.ffg.careworks.rest.dto;

public enum Status {
    NOT_CREATED, CREATED, IN_PROGRESS, PENDING_APPROVAL, REJECTED, APPROVED, COMPLETED, SUBMITTED, ON_BOARDED

}
