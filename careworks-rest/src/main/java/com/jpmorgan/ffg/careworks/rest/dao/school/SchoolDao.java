package com.jpmorgan.ffg.careworks.rest.dao.school;

import com.jpmorgan.ffg.careworks.rest.data.BasicSchoolProfileRepo;
import com.jpmorgan.ffg.careworks.rest.data.LocationDataRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolCategoryDataRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolMediumDataRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolProfileAddressRepo;
import com.jpmorgan.ffg.careworks.rest.data.SchoolTypeDataRepo;
import com.jpmorgan.ffg.careworks.rest.dto.common.DashboardSchoolStatus;
import com.jpmorgan.ffg.careworks.rest.model.Location;
import com.jpmorgan.ffg.careworks.rest.model.Medium;
import com.jpmorgan.ffg.careworks.rest.model.SchoolCategory;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfile;
import com.jpmorgan.ffg.careworks.rest.model.SchoolProfileAddress;
import com.jpmorgan.ffg.careworks.rest.model.SchoolType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.SqlResultSetMapping;
import java.util.List;

@Service
@AllArgsConstructor
public class SchoolDao implements ISchoolDao {

    private final BasicSchoolProfileRepo basicSchoolProfileRepo;

    private final SchoolProfileAddressRepo schoolProfileAddressRepo;

    private final LocationDataRepo locationDataRepo;

    private final SchoolCategoryDataRepo schoolCategoryDataRepo;

    private final SchoolMediumDataRepo schoolMediumDataRepo;

    private final SchoolTypeDataRepo schoolTypeDataRepo;

    private final EntityManagerFactory emf;

    @Override
    public SchoolProfile getSchoolProfileDataBySchoolId(long schoolId){
        return basicSchoolProfileRepo.findBySchoolId(schoolId);
    }

    @Override
    public List<Location> getAllAvailableLocation(){
        return locationDataRepo.findAll();
    }

    @Override
    public List<SchoolCategory> getAllAvailableSchoolCategoryList(){
        return schoolCategoryDataRepo.findAll();
    }

    @Override
    public List<Medium> getAllAvailableMediumsList(){
        return schoolMediumDataRepo.findAll();
    }

    @Override
    public List<SchoolType> getAllAvailableSchoolTypeList(){
        return schoolTypeDataRepo.findAll();
    }

    @Override
    public SchoolProfile saveSchoolData(SchoolProfile schoolProfile) {
        return basicSchoolProfileRepo.saveAndFlush(schoolProfile);
    }

    @Override
    public SchoolProfileAddress saveSchoolAddress(SchoolProfileAddress schoolProfileAddress) {
        return schoolProfileAddressRepo.save(schoolProfileAddress);
    }

    @Override
    public List<DashboardSchoolStatus> getAllSchoolStatus() {
        EntityManager em = emf.createEntityManager();
        //em.getTransaction().begin( );

        Query query = em.createNativeQuery("select type as type, upper(REPLACE(status,'_',' ')) as status, count as count  from (\n" +
                "SELECT 'Schools' type, school_status status, count(distinct school_id) count FROM care_works.school_profile   group by status\n" +
                "union\n" +
                "select 'Need Assessment' type, (case  na_flag\n" +
                "when \"S\" then \"SUBMITTED\"\n" +
                "when \"I\" then \"IN_PROGRESS\"\n" +
                "when \"P\" then \"PENDING\"\n" +
                "when \"N\" then \"PENDING\"\n" +
                "when \"Y\" then \"APPROVED\"\n" +
                "when \"R\" then \"REJECTED\"\n" +
                " else na_flag end) status, count(distinct school_id) count from care_works.school_profile group by na_flag\n" +
                "union\n" +
                "select 'Action Plan' type, status, count(distinct actionplan_id) count from care_works.action_plan group by status\n" +
                "union\n" +
                "select 'Quotation' type, status, count(distinct action_id) count from care_works.quotation group by status\n" +
                "order by 1, 2) temp", "DashboardSchoolStatus");

        List<DashboardSchoolStatus> list = query.getResultList();
        em.close();

        return list;
    }

    @SqlResultSetMapping(
            name="DashboardSchoolStatus",
            classes={
                    @ConstructorResult(
                            targetClass=com.jpmorgan.ffg.careworks.rest.dto.common.DashboardSchoolStatus.class,
                            columns={
                                    @ColumnResult(name="type", type = String.class),
                                    @ColumnResult(name="status", type = String.class),
                                    @ColumnResult(name="count", type=Integer.class)
                            }
                    )
            }
    ) @Entity class SQLMappingStatus{@Id int it;}
}
