package com.jpmorgan.ffg.careworks.rest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Category_Master")
@Data
public class CategoryMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long categoryId;

    private String categoryDetails;

}
