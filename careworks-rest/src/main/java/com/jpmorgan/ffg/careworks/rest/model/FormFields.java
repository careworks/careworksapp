package com.jpmorgan.ffg.careworks.rest.model;

import javax.persistence.*;

/**
 * Created by Arijit on 03-Dec-19.
 */

@Entity
@Table(name = "form_fields")
public class FormFields {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long seq;
    private String category;
    private String subCategory;
    private String fieldSection;
    private String fieldSectionHeader;
    private String fieldId;
    private String fieldLabel;
    private String fieldType;
    private String fieldValidation;
    private String fieldRequired;
    private String parentField;
    private Long childSeq;
    private String fieldOptions;
    //private NeedAssessmentResponse needAssessmentResponse;

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldLabel() {
        return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
        this.fieldLabel = fieldLabel;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldRequired() {
        return fieldRequired;
    }

    public void setFieldRequired(String fieldRequired) {
        this.fieldRequired = fieldRequired;
    }

    public String getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(String fieldOptions) {
        this.fieldOptions = fieldOptions;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getFieldSection() {
        return fieldSection;
    }

    public void setFieldSection(String fieldSection) {
        this.fieldSection = fieldSection;
    }

    public String getFieldValidation() {
        return fieldValidation;
    }

    public void setFieldValidation(String fieldValidation) {
        this.fieldValidation = fieldValidation;
    }

    public String getParentField() {
        return parentField;
    }

    public void setParentField(String parentField) {
        this.parentField = parentField;
    }

    public String getFieldSectionHeader() {
        return fieldSectionHeader;
    }

    public void setFieldSectionHeader(String fieldSectionHeader) {
        this.fieldSectionHeader = fieldSectionHeader;
    }

    public Long getChildSeq() {
        return childSeq;
    }

    public void setChildSeq(Long childSeq) {
        this.childSeq = childSeq;
    }

    /*public NeedAssessmentResponse getNeedAssessmentResponse() {
        return needAssessmentResponse;
    }

    public void setNeedAssessmentResponse(NeedAssessmentResponse needAssessmentResponse) {
        this.needAssessmentResponse = needAssessmentResponse;
    }*/
}
