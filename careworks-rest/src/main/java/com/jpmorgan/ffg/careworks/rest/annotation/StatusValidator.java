package com.jpmorgan.ffg.careworks.rest.annotation;

import com.jpmorgan.ffg.careworks.rest.dto.Status;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StatusValidator implements ConstraintValidator<ValidateStatus, String> {

    @Override
    public boolean isValid(String status, ConstraintValidatorContext context) {
        for (Status s : Status.values()) {
            if (s.name().equals(status)) {
                return true;
            }
        }
        return false;
    }

}
