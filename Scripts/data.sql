-- -----------------------------------------------------
-- Data for table `care_works`.`form_fields`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (1, 'HEALTH',null,null,'FIRST_AID_BOX','Is there a first aid box in the school?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (2, 'HEALTH',null,null,'MEDS_AVAILABLE','Is Medicines Available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (3, 'HEALTH','Are Health camps organized','Dental','DENTAL','Are Health camps organized','Dental',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (4, 'HEALTH','Are Health camps organized','Eye','EYE','Are Health camps organized','Eye',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (5, 'HEALTH','Are Health camps organized','General','GENERAL','Are Health camps organized','General',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (7, 'HEALTH','Is WASH Program Implemented?','Hand wash Program','HWP','Is WASH Program Implemented?','Hand wash Program',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (8, 'HEALTH','Is WASH Program Implemented?','Toilet usage awareness','TUA','Is WASH Program Implemented?','Toilet usage awareness',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (9, 'HEALTH','Is WASH Program Implemented?','Awareness on Safe drinking water','ASD','Is WASH Program Implemented?','Awareness on Safe drinking water',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (10, 'HEALTH',null,null,'HAND_WASH','Is there a Hand wash area?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (11, 'HEALTH',null,null,'RENOVATION_REQD','If yes, is renovation required?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','HAND_WASH','radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (12, 'HEALTH',null,null,'UTENSIL_AREA','Is there a separate utensil washing area?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (13, 'HEALTH','Toilet Details','TOILET_BOYS','TOILET_BOYS_AVAILABLE','Toilet for boys','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (14, 'HEALTH','Toilet Details','TOILET_BOYS','TOILET_BOYS_REPAIR_REQD','Toilet for boys','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (15, 'HEALTH','Toilet Details','TOILET_BOYS','TOILET_BOYS_NEW_REQD','Toilet for boys','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (16, 'HEALTH','Toilet Details','URINALS_BOYS','URINALS_BOYS_AVAILABLE','Urinals for Boys','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (17, 'HEALTH','Toilet Details','URINALS_BOYS','URINALS_BOYS_REPAIR_REQD','Urinals for Boys','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (18, 'HEALTH','Toilet Details','URINALS_BOYS','URINALS_BOYS_NEW_REQD','Urinals for Boys','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (19, 'HEALTH','Toilet Details','TOILET_GIRLS','TOILET_GIRLS_AVAILABLE','Toilet for girls','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (20, 'HEALTH','Toilet Details','TOILET_GIRLS','TOILET_GIRLS_REPAIR_REQD','Toilet for girls','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (21, 'HEALTH','Toilet Details','TOILET_GIRLS','TOILET_GIRLS_BOYS_NEW_REQD','Toilet for girls','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (22, 'HEALTH','Toilet Details','URINALS_GIRLS','URINALS_GIRLS_AVAILABLE','Urinals for girls','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (23, 'HEALTH','Toilet Details','URINALS_GIRLS','URINALS_GIRLS_REPAIR_REQD','Urinals for girls','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (24, 'HEALTH','Toilet Details','URINALS_GIRLS','URINALS_GIRLS_BOYS_NEW_REQD','Urinals for girls','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (25, 'HEALTH','Toilet Details','TOILETS_CWSN','TOILETS_CWSN_AVAILABLE','Toilets for CWSN','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (26, 'HEALTH','Toilet Details','TOILETS_CWSN','TOILETS_CWSN_REPAIR_REQD','Toilets for CWSN','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (27, 'HEALTH','Toilet Details','TOILETS_CWSN','TOILETS_CWSN_BOYS_NEW_REQD','Toilets for CWSN','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (28, 'HEALTH','Toilet Details','TOILETS_STAFF','TOILETS_STAFF_AVAILABLE','Toilets for Staff','No of Toilets Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (29, 'HEALTH','Toilet Details','TOILETS_STAFF','TOILETS_STAFF_REPAIR_REQD','Toilets for Staff','No of Toilets Req. Repair','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (30, 'HEALTH','Toilet Details','TOILETS_STAFF','TOILETS_STAFF_BOYS_NEW_REQD','Toilets for Staff','No of New Toilets Required','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (31, 'HEALTH',null,null,'WATER_PURIFIER','Is water purifier available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (32, 'HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_TOTAL','RO Unit','Total Available','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (33, 'HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_REPAIR_REQD','RO Unit','Total req. Repair','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (34, 'HEALTH','Details','WATER_PURIFIER_RO','WATER_PURIFIER_RO_NEW_REQ','RO Unit','New Requirement','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (35, 'HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_TOTAL','UV Unit','Total Available','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (36, 'HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_REPAIR_REQD','UV Unit','Total req. Repair','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (37, 'HEALTH','Details','WATER_PURIFIER_UV','WATER_PURIFIER_UV_NEW_REQ','UV Unit','New Requirement','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (38, 'HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_TOTAL','Storage facility','Total Available','number',null,'N','WATER_PURIFIER','textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (39, 'HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_REPAIR_REQD','Storage facility','Total req. Repair','number',null,'N','WATER_PURIFIER','textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (40, 'HEALTH','Details','WATER_PURIFIER_STRG','WATER_PURIFIER_STRG_NEW_REQ','Storage facility','New Requirement','number',null,'N','WATER_PURIFIER','textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (41, 'HEALTH',null,null,'SRC_OF_WATER','Source of water :',null,'[{"key":"Bore well","value":"BW"},{"key":"Corporation/GP/Water Connection","value":"CP"},{"key":"Water Tanker","value":"WT"},{"key":"Open Well","value":"OW"}]','N',null,'dropdown',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (42, 'HEALTH',null,null,'WATER_STORAGE','Water storage System :',null,'[{"key":"Sump","value":"SMP"},{"key":"Overhead tank","value":"OHT"}]','N',null,'dropdown',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (43, 'HEALTH',null,null,'WATER_TESTED','Is water tested regularly?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (44, 'HEALTH',null,null,'CLEANER','Is there a staff to clean toilet?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (45, 'HEALTH',null,null,'DUSTBINS','Is there dustbins available for each class?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (46, 'HEALTH',null,null,'WASTE_DISPOSABLE','Water Disposable system?',null,'[{"key":"Through corporation/GP","value":"THC"},{"key":"Having own waste management system","value":"OWM"},{"key":"None of the above","value":"WDN"}]','N',null,'checkbox',null);

-- Classroom Environment
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (47, 'CLASSROOM',null,null,'SEP_COMP_LAB','Is there a separate room for computer Lab in the school?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (48, 'CLASSROOM',null,null,'CURR_AVL','Is curriculum available?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (49, 'CLASSROOM',null,null,'CURR_DTLS','If yes collect the details',null,null,'N','CURR_AVL','textbox',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (50, 'CLASSROOM',null,null,'E_LEARN','Is e-learning system of teaching is available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (51, 'CLASSROOM',null,null,'E_LEARN_PRVDR','If yes mention the organization who provide the system',null,null,'N','E_LEARN','textbox',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (52, 'CLASSROOM','Details of the computer lab','COMPS','COMPS_AVAILABLE','Computers','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (53, 'CLASSROOM','Details of the computer lab','COMPS','COMPS_REPAIR_REQD','Computers','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (54, 'CLASSROOM','Details of the computer lab','COMPS','COMPS_NEW_REQD','Computers','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (55, 'CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_AVAILABLE','Laptops','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (56, 'CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_REPAIR_REQD','Laptops','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (57, 'CLASSROOM','Details of the computer lab','LAPTOPS','LAPTOPS_NEW_REQD','Laptops','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (58, 'CLASSROOM','Details of the computer lab','UPS','UPS_AVAILABLE','UPS','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (59, 'CLASSROOM','Details of the computer lab','UPS','UPS_REPAIR_REQD','UPS','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (60, 'CLASSROOM','Details of the computer lab','UPS','UPS_NEW_REQD','UPS','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (61, 'CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_AVAILABLE','Computer tables','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (62, 'CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_REPAIR_REQD','Computer tables','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (63, 'CLASSROOM','Details of the computer lab','COMP_TABLES','COMP_TABLES_NEW_REQD','Computer tables','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (64, 'CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_AVAILABLE','Computer chairs','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (65, 'CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_REPAIR_REQD','Computer chairs','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (66, 'CLASSROOM','Details of the computer lab','COMP_CHAIRS','COMP_CHAIRS_NEW_REQD','Computer chairs','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (67, 'CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_AVAILABLE','Projector','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (68, 'CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_REPAIR_REQD','Projector','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (69, 'CLASSROOM','Details of the computer lab','PROJECTOR','PROJECTOR_NEW_REQD','Projector','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (70, 'CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_AVAILABLE','Projector Screen','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (71, 'CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_REPAIR_REQD','Projector Screen','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (72, 'CLASSROOM','Details of the computer lab','PROJECTOR_SCRN','PROJECTOR_SCRN_NEW_REQD','Projector Screen','New Req.','number',null,'N',null,'textbox',3);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (73, 'CLASSROOM','Details of the computer lab','WALL_MOUNT','WALL_MOUNT_AVAILABLE','Wall mounted table','Total Available','number',null,'N',null,'textbox',1);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (74, 'CLASSROOM','Details of the computer lab','WALL_MOUNT','WALL_MOUNT_REPAIR_REQD','Wall mounted table','Repair Req.','number',null,'N',null,'textbox',2);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (75, 'CLASSROOM','Details of the computer lab','WALL_MOUNT','WALL_MOUNT_NEW_REQD','Wall mounted table','New Req.','number',null,'N',null,'textbox',3);


INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (76, 'CLASSROOM',null,null,'SEP_LIBRARY','Is there a separate room for the Library?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (77, 'CLASSROOM',null,null,'LIBRARY_NGO','Is there any NGO/Org managing/supporting the library?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (78, 'CLASSROOM',null,null,'LIBRARY_NGO_ORG','If yes Mention name of the organization',null,null,'N','LIBRARY_NGO','textbox',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (79, 'CLASSROOM','Library Room Details','Is Library arranged level wise','LIBRARY_LEVEL','Library Room Details','Is Library arranged level wise',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (80, 'CLASSROOM','Library Room Details','No of books available','LIBRARY_BOOKS_COUNT','Library Room Details','No of books available',null,null,'N',null,'textbox',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (81, 'CLASSROOM','Library Room Details','Is sufficient cupboards available','LIBRARY_CUPBOARDS','Library Room Details','Is sufficient cupboards available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (82, 'CLASSROOM','Library Room Details','Is Table available','LIBRARY_TABLE','Library Room Details','Is Table available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (83, 'CLASSROOM','Library Room Details','Is sufficient Chairs/Bench available','LIBRARY_CHRS_BNCH','Library Room Details','Is sufficient Chairs/Bench available',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (84, 'CLASSROOM',null,null,'SCIENCE_LAB','Is there a separate room for Science Lab?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (85, 'CLASSROOM','','Wall mounted Table','SC_LAB_WALL_MOUNT',null,'Wall mounted Table',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (86, 'CLASSROOM','','Centre Table','SC_LAB_CNTR_TBL',null,'Centre Table',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (87, 'CLASSROOM','','Chemicals','SC_LAB_CHMCLS',null,'Chemicals',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (88, 'CLASSROOM','','Washbasin','SC_LAB_WSH_BASIN',null,'Washbasin',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (89, 'CLASSROOM','','Models','SC_LAB_MODELS',null,'Models',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_section_header`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (90, 'CLASSROOM','','Any Other','SC_LAB_ANY_OTH',null,'Any Other',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N','SCIENCE_LAB','radio',null);

INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (91, 'CLASSROOM',null,null,'SCIENCE_NGO','Is there any Org/NGO supporting Science lab activities?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (92, 'CLASSROOM',null,null,'SCIENCE_NGO_ORG','If yes Mention the name of the organization',null,null,'N','SCIENCE_NGO','textbox',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (93, 'CLASSROOM',null,null,'SPORTS_ROOM','Is there a store room dedicated for sports?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (94, 'CLASSROOM',null,null,'PLAY_GRND','Do you have play ground?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (95, 'CLASSROOM',null,null,'SPRTS_MTRLS','Do you have sufficient sports materials?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (96, 'CLASSROOM',null,null,'ARTS_CRAFTS','Are you engaging students in teaching art & Craft?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (97, 'CLASSROOM',null,null,'ARTS_CRAFTS_ORG','Is there any organization supporting Art & Craft?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);
INSERT INTO `care_works`.`form_fields` (`seq`,`category`,`sub_category`,`field_section`,`field_id`,`field_label`,`field_validation`,`field_options`,`field_required`,`parent_field`,`field_type`,`child_seq`)
VALUES (98, 'CLASSROOM',null,null,'LRNG_MTRLS','Do you have sufficient Teacher Learning Materials?',null,'[{"key":"Yes","value":"Y"},{"key":"No","value":"N"}]','N',null,'radio',null);


COMMIT;