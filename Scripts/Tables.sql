USE `care_works`;

create table action_master
(
    id             bigint not null auto_increment,
    activity_id    bigint not null,
    building_id    bigint not null,
    category_id    bigint not null,
    deadline       datetime,
    human_resource bit    not null,
    kind           bit    not null,
    money          bit    not null,
    responsibility varchar(255),
    school_id      bigint not null,
    stakeholders   varchar(255),
    primary key (id)
) engine = InnoDB;
create table activity_master
(
    id              bigint not null auto_increment,
    activity_detail varchar(255),
    activity_id     bigint not null,
    category_id     bigint not null,
    primary key (id)
) engine = InnoDB;
create table assessment_detail
(
    detail_type varchar(255) not null,
    name        varchar(255) not null,
    school_id   integer      not null,
    available   integer      not null,
    pristine    integer      not null,
    repair      integer      not null,
    value       varchar(255),
    primary key (detail_type, name, school_id)
) engine = InnoDB;
create table assessment_option
(
    option_id   integer not null auto_increment,
    option_name varchar(255),
    primary key (option_id)
) engine = InnoDB;
create table assessment_question
(
    question_id   integer not null auto_increment,
    question      varchar(255),
    question_type varchar(255),
    section       varchar(255),
    primary key (question_id)
) engine = InnoDB;
create table building_master
(
    id          bigint not null auto_increment,
    building_id bigint,
    category_id bigint,
    primary key (id)
) engine = InnoDB;
create table category_master
(
    category_id      bigint not null auto_increment,
    category_details varchar(255),
    primary key (category_id)
) engine = InnoDB;
create table medium
(
    medium      varchar(255) not null,
    description varchar(255),
    primary key (medium)
) engine = InnoDB;
create table need_assessment
(
    assessment_type varchar(255) not null,
    category        varchar(255) not null,
    response        varchar(255) not null,
    school_id       integer      not null,
    sub_category    varchar(255) not null,
    remarks         varchar(255),
    primary key (assessment_type, category, response, school_id, sub_category)
) engine = InnoDB;
create table question_option
(
    question_id integer not null,
    option_id   integer not null,
    primary key (question_id, option_id)
) engine = InnoDB;
create table school_category
(
    category    varchar(255) not null,
    description varchar(255),
    primary key (category)
) engine = InnoDB;
create table school_education_department
(
    education_department_id bigint       not null auto_increment,
    contact_details         varchar(255) not null,
    designation             varchar(255) not null,
    email_id                varchar(255) not null,
    name                    varchar(255) not null,
    school_id               bigint       not null,
    primary key (education_department_id)
) engine = InnoDB;

alter table care_works.school_env
    add column id bigint not null;
alter table care_works.school_env
    add column nalika_chapara varchar(255);
alter table care_works.school_env
    add column wallmounted_table varchar(255);
    
ALTER TABLE care_works.school_env
DROP COLUMN id;
ALTER TABLE care_works.school_env
DROP COLUMN nalika_chapara;
ALTER TABLE care_works.school_env
DROP COLUMN wallmounted_table;

create table school_profile
(
    school_id        bigint  not null auto_increment,
    block            varchar(255),
    dise_code        varchar(255),
    district         varchar(255),
    email_id         varchar(255),
    established_date date,
    location         varchar(255),
    region           varchar(255),
    school_address   varchar(255),
    school_cluster   varchar(255),
    school_name      varchar(255),
    state            varchar(255),
    student_count    integer not null,
    category         varchar(255),
    type             varchar(255),
    na_flag			 varchar(1) default 'N',
    primary key (school_id)
) engine = InnoDB;
create table school_status
(
    school_id       bigint not null,
    action_plan     varchar(255),
    monitoring      varchar(255),
    need_assessment varchar(255),
    school_profile  varchar(255),
    primary key (school_id)
) engine = InnoDB;
create table school_type
(
    type        varchar(255) not null,
    description varchar(255),
    primary key (type)
) engine = InnoDB;
create table school_profile_medium
(
    school_id bigint       not null,
    medium    varchar(255) not null,
    primary key (school_id, medium)
) engine = InnoDB;
create table section_info
(
    section_id   bigint       not null auto_increment,
    standard     varchar(255) not null,
    school_id    bigint       not null,
    section_name varchar(255) not null,
    medium       varchar(255),
    primary key (section_id)
) engine = InnoDB;
create table section_medium_map
(
    section_id bigint       not null,
    medium     varchar(255) not null,
    primary key (section_id, medium)
) engine = InnoDB;
create table stakeholder_info
(
    id                bigint       not null auto_increment,
    contact_number    bigint,
    formation_year    integer      not null,
    member_count      integer      not null,
    school_id         bigint       not null,
    spoc_name         varchar(255),
    stake_holder_name varchar(255) not null,
    primary key (id)
) engine = InnoDB;
create table student_info
(
    student_details_id bigint  not null auto_increment,
    gender             varchar(60),
    social_divison     varchar(60),
    student_count      integer not null,
    section_id         bigint,
    primary key (student_details_id)
) engine = InnoDB;
create table teacher_details
(
    teacher_group_id bigint       not null auto_increment,
    employment_type  varchar(255) not null,
    gender           varchar(255) not null,
    school_id        bigint       not null,
    subject          varchar(255) not null,
    teacher_count    integer      not null,
    primary key (teacher_group_id)
) engine = InnoDB;
alter table action_master
    drop index UKiy183uxlirtvvk24fcp0gxyxf;
alter table action_master
    add constraint UKiy183uxlirtvvk24fcp0gxyxf unique (school_id, category_id, activity_id);
alter table activity_master
    drop index UKf3vqcg2ek3gfbtjn8srdmnvtn;
alter table activity_master
    add constraint UKf3vqcg2ek3gfbtjn8srdmnvtn unique (category_id, activity_id, activity_detail);
alter table building_master
    drop index UKiqghbpnf2w8pue1fmgg2wmi08;
alter table building_master
    add constraint UKiqghbpnf2w8pue1fmgg2wmi08 unique (building_id, category_id);
alter table question_option
    add constraint FKbbc67ouw29w6lwjx71yjfkt39 foreign key (option_id) references assessment_option (option_id);
alter table question_option
    add constraint FKd09syq6rd4lh20h6apycy0gro foreign key (question_id) references assessment_question (question_id);
alter table school_profile
    add constraint FKtflr2c5lb0qq5ge0qd7w4rnpi foreign key (category) references school_category (category);
alter table school_profile
    add constraint FKdnltkclt4cpolfn5dli4f7und foreign key (type) references school_type (type);
alter table school_profile_medium
    add constraint FK74gtpi53t43ua11m5og4nbkqc foreign key (medium) references medium (medium);
alter table school_profile_medium
    add constraint FKt2477fxm2fhtjrl5onaybv37e foreign key (school_id) references school_profile (school_id);
alter table section_info
    add constraint FK8tw6r5pr6cq9gn3x39suqxgvq foreign key (medium) references medium (medium);
alter table section_medium_map
    add constraint FKna473a8og1ivjpou6xuq9drqk foreign key (section_id) references section_info (section_id);
alter table section_medium_map
    add constraint FKogx3ti5rgygteg67mqdcyrdb4 foreign key (medium) references medium (medium);
alter table student_info
    add constraint FKnbs671mig1x2rphxdodro3nyt foreign key (section_id) references section_info (section_id);
