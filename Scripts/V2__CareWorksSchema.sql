CREATE SCHEMA IF NOT EXISTS `care_works` DEFAULT CHARACTER SET utf8;
USE `care_works`;

CREATE TABLE `roles`
(
    `id`          bigint(20)  NOT NULL AUTO_INCREMENT,
    `name`        varchar(60) NOT NULL,
    `description` varchar(200),
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_roles_name` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;

CREATE TABLE `users`
(
    `id`         bigint(20)   NOT NULL AUTO_INCREMENT,
    `name`       varchar(40)  NOT NULL,
    `username`   varchar(15)  NOT NULL,
    `email`      varchar(40)  NOT NULL,
    `password`   varchar(100) NOT NULL,
    `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_users_username` (`username`),
    UNIQUE KEY `uk_users_email` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `user_roles`
(
    `user_id` bigint(20) NOT NULL,
    `role_id` bigint(20) NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`),
    KEY `fk_user_roles_role_id` (`role_id`),
    CONSTRAINT `fk_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
    CONSTRAINT `fk_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT IGNORE INTO roles(name)
VALUES ('ROLE_USER');
INSERT IGNORE INTO roles(name)
VALUES ('ROLE_ADMIN');
COMMIT;

create table location
(
    location    varchar(25) not null PRIMARY KEY,
    description text        null
);


create table medium
(
    medium      varchar(25) not null
        primary key,
    description varchar(25) null
);

create table school_category
(
    category    varchar(25) not null
        primary key,
    description varchar(25) null
);

create table school_type
(
    type        varchar(25) not null
        primary key,
    description varchar(25) null
);

create table social_division
(
    name        varchar(45) not null
        primary key,
    description varchar(45) null
);

create table school_profile
(
    school_id        bigint auto_increment PRIMARY KEY,
    school_name      text                   null,
    established_date date                   not null,
    location         text                   null,
    school_address   text                   null,
    block            text                   null,
    region           text                   null,
    district         text                   null,
    state            text                   null,
    category         varchar(255)           null,
    type             varchar(255)           null,
    medium           varchar(255)           null,
    dise_code        varchar(25)            null,
    email_id         text                   null,
    student_count    bigint                 not null,
    na_flag          varchar(1) default 'N' not null,
    na_action_date   date                   null
);

alter table school_profile
    modify category varchar(25) not null;

alter table school_profile
    modify type varchar(25) not null;

alter table school_profile
    modify medium varchar(25) null;

alter table school_profile
    add comments text null;

alter table school_profile
    add constraint school_profile___fk2
        foreign key (category) references school_category (category)
            on update cascade on delete cascade;

alter table school_profile
    add constraint school_profile___fk3
        foreign key (type) references school_type (type)
            on update cascade on delete cascade;

alter table school_profile
    add constraint school_profile_medium_medium_fk
        foreign key (medium) references medium (medium)
            on update cascade on delete cascade;

alter table school_profile
    modify location varchar(25) not null;

alter table school_profile
    add constraint school_profile___fk4
        foreign key (location) references location (location)
            on update cascade on delete cascade;

create table section_info
(
    section_id   bigint auto_increment PRIMARY KEY,
    standard     varchar(25) null,
    school_id    bigint      not null,
    section_name varchar(25) null,
    medium       varchar(25) null,
    constraint section_info_section_id_uindex
        unique (section_id),
    constraint section_info___fk2
        foreign key (school_id) references school_profile (school_id),
    constraint section_info_medium_medium_fk
        foreign key (medium) references medium (medium)
);

create table student_info
(
    student_details_id bigint auto_increment PRIMARY KEY,
    gender             varchar(25) null,
    social_division    varchar(25) null,
    student_count      bigint      not null,
    section_id         bigint      null,
    constraint student_info_social_division_name_fk
        foreign key (social_division) references social_division (name)
);

create unique index student_info_student_details_id_uindex
    on student_info (student_details_id);

alter table student_info
    add constraint student_info___fk2
        foreign key (section_id) references section_info (section_id)
            on update cascade on delete cascade;

create table school_education_department
(
    education_department_id bigint auto_increment PRIMARY KEY,
    contact_details         text   null,
    designation             text   null,
    email_id                text   null,
    name                    text   not null,
    school_id               bigint not null
);

alter table school_education_department
    add constraint school_education_department_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade;

create table stakeholder_info
(
    id                bigint auto_increment PRIMARY KEY,
    contact_number    text        null,
    formation_year    varchar(10) null,
    member_count      bigint      null,
    school_id         bigint      not null,
    spoc_name         text        null,
    stake_holder_name text        null,
    constraint stakeholder_info_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);

create table teacher_details
(
    teacher_group_id bigint auto_increment PRIMARY KEY,
    employment_type  text        null,
    gender           varchar(25) null,
    subject          text        null,
    teacher_count    bigint      null,
    school_id        bigint      not null,
    constraint teacher_details_school_profile_school_id_fk
        foreign key (school_id) references school_profile (school_id)
            on update cascade on delete cascade
);


INSERT INTO care_works.school_type (type, description)
VALUES ('Boys', 'Boys School');
INSERT INTO care_works.school_type (type, description)
VALUES ('Co-education', '');
INSERT INTO care_works.school_type (type, description)
VALUES ('Girls', 'Girls School');

INSERT INTO care_works.medium (medium, description)
VALUES ('English', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Kannada', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Others', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Tamil', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Telugu', null);
INSERT INTO care_works.medium (medium, description)
VALUES ('Urdu', null);

INSERT INTO care_works.school_category (category, description)
VALUES ('GHPS', 'Govt. Higher Primary School (1-7/8th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GHS', 'Govt. High School (8-10th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GLPS', 'Govt. Lower Primary School (1-5th)');
INSERT INTO care_works.school_category (category, description)
VALUES ('GMPS', 'Govt. Model Primary School (1-7/8th)');

INSERT INTO care_works.location (location, description)
VALUES ('Rural', 'Rural Village');
INSERT INTO care_works.location (location, description)
VALUES ('Urban', 'Urban City');

INSERT INTO care_works.social_division (name, description)
VALUES ('CWSN', 'CWSN');
INSERT INTO care_works.social_division (name, description)
VALUES ('General', 'General');
INSERT INTO care_works.social_division (name, description)
VALUES ('ScheduleCaste', 'Schedule Caste');
INSERT INTO care_works.social_division (name, description)
VALUES ('ST', 'ScheduleTribe');

INSERT INTO care_works.school_profile (school_id, school_name, established_date, location, school_managed_by, block,
                                       region, district, state, category, type, medium, dise_code, email_id,
                                       student_count, na_flag, na_action_date, any_other_information)
VALUES (1, 'dummy', '2020-01-31', 'Urban', 'dum', 'mmy', 'aa', 'mm', 'k', 'GHPS', 'Boys', 'English', 'ya11',
        'aa@aa.com', 100, 'N', null, 'aa');

INSERT INTO `care_works`.`section_info` ('section_id', `standard`, `school_id`, `section_name`, `medium`)
VALUES (1, '1', 1, 'A', 'English');

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Boys', 'General', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Girls', 'General', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Boys', 'ScheduleCaste', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Girls', 'ScheduleCaste', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Boys', 'ST', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Girls', 'ST', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Boys', 'CWSN', 100, 1);

INSERT INTO `care_works`.`student_info` (`gender`, `social_division`, `student_count`, `section_id`)
VALUES ('Girls', 'CWSN', 100, 1);

INSERT INTO care_works.school_education_department (education_department_id, contact_details, designation, email_id,
                                                    name, school_id)
VALUES (374, '123', 'kkk', 'aaa@a.com', 'name1', 1);

INSERT INTO care_works.stakeholder_info (id, contact_number, formation_year, member_count, school_id, spoc_name,
                                         stake_holder_name)
VALUES (375, 123, 2019, 10, 1, 'abc', 'bca');

INSERT INTO care_works.teacher_details (teacher_group_id, employment_type, gender, school_id, subject, teacher_count)
VALUES (377, 'Permanent', 'Male', 1, 'Math', 10);