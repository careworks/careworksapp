import React from "react";
import {login, signup} from "../utils/APIUtils";

var UserStateContext = React.createContext();
var UserDispatchContext = React.createContext();

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return {...state, isAuthenticated: true};
    case "LOGIN_FAILURE":
      return {...state, isAuthenticated: false};
    case "SIGN_OUT_SUCCESS":
      return {...state, isAuthenticated: false};
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function UserProvider({children}) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: !!sessionStorage.getItem("id_token"),
  });

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

function useUserState() {
  var context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

function useUserDispatch() {
  var context = React.useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error("useUserDispatch must be used within a UserProvider");
  }
  return context;
}

export {UserProvider, useUserState, useUserDispatch, loginUser,signupUser, signOut};

// ###########################################################

function loginUser(dispatch, userName, password, history, setIsLoading, setError) {
  setError(false);
  setIsLoading(true);

  if (!!userName && !!password) {
    sessionStorage.clear();
    login({
      login: userName,
      password: password
    }).then(json => {
      sessionStorage.setItem('id_token', json.id_token);
      sessionStorage.setItem('username', userName);
      sessionStorage.setItem('role', json.role)
      setError(null);
      setIsLoading(false);
      dispatch({type: 'LOGIN_SUCCESS'});

      history.push('/app/schools')
    }).catch(err => {
        console.error('Error: ', err);
        setError(err.message.replace('An unexpected internal server error occured', ''));
        setIsLoading(false);
      }
    );
  } else {
    dispatch({type: "LOGIN_FAILURE"});
    setError(true);
    setIsLoading(false);
  }
}

function signupUser(dispatch, name,userName, password, phone, setIsLoading, setError) {
  setError(false);
  setIsLoading(true);

  if (!!userName && !!password && !!phone) {
    const signupRequest = {
      name: name,
      email: userName,
      phone: phone,
      username: userName,
      password: password
    };
    signup(signupRequest)
      .then(response => {
        setError(response.message);
        setIsLoading(false);
      }).catch(error => {
      console.error('Error: ', error);
      setError(error.message.replace('An unexpected internal server error occured', ''));
      setIsLoading(false);
    });
  } else {
    dispatch({type: "LOGIN_FAILURE"});
    setError(true);
    setIsLoading(false);
  }
}

function signOut(dispatch, history) {
  sessionStorage.clear();
  dispatch({type: "SIGN_OUT_SUCCESS"});
  history.push("/login");
}
