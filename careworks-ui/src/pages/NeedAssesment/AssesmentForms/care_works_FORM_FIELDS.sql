-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: care_works
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FORM_FIELDS`
--

DROP TABLE IF EXISTS `FORM_FIELDS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FORM_FIELDS` (
  `SEQ` bigint(20) NOT NULL,
  `CATEGORY` varchar(20) NOT NULL,
  `SUB_CATEGORY` varchar(100) DEFAULT NULL,
  `FIELD_SECTION` varchar(100) DEFAULT NULL,
  `FIELD_SECTION_HEADER` varchar(100) DEFAULT NULL,
  `FIELD_ID` varchar(100) NOT NULL,
  `FIELD_LABEL` varchar(500) NOT NULL,
  `FIELD_TYPE` varchar(50) NOT NULL,
  `FIELD_VALIDATION` varchar(100) DEFAULT NULL,
  `FIELD_REQUIRED` varchar(10) DEFAULT NULL,
  `PARENT_FIELD` varchar(100) DEFAULT NULL,
  `FIELD_OPTIONS` blob,
  `FIELD_DEFAULT_VALUE` varchar(100) DEFAULT NULL,
  `IS_ACTIVE` VARCHAR(10) default 'Y',
  PRIMARY KEY (`SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FORM_FIELDS`
--

LOCK TABLES `FORM_FIELDS` WRITE;
/*!40000 ALTER TABLE `FORM_FIELDS` DISABLE KEYS */;
INSERT INTO `FORM_FIELDS` VALUES (1,'HEALTH',NULL,NULL,NULL,'FIRST_AID_BOX','Is there a first aid box in the school?','radio','Y','N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(2,'HEALTH',NULL,NULL,NULL,'MEDS_AVAILABLE','Is Medicines Available?','radio','N','N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(3,'HEALTH','Are Health camps organized',NULL,NULL,'HEALTH_CAMPS','Are Health camps organized','checkbox','DENTAL','N',NULL,_binary '[\n	{\n		\"key\" : \"Dental\",\n		\"value\" : \"Dental\"\n	},\n	{\n		\"key\" : \"Eye\",\n		\"value\" : \"Eye\"\n	},\n {\n		\"key\" : \"General\",\n		\"value\" : \"Genral\"\n	}\n]',NULL),(6,'HEALTH',NULL,NULL,NULL,'WASH_PROGRAMS','Is WASH Program Implemented?','checkbox',NULL,'N',NULL,_binary '[{\n	\"key\":\"Hand wash Program\",\"value\":\"HW\"\n},\n{\n\"key\":\"Toilet usage awareness\",\"value\":\"TUA\"\n},\n{\n\"key\":\"Awareness on Safe drinking water\",\"value\":\"ASD\"\n}\n]',NULL),(7,'HEALTH','Is WASH Program Implemented?',NULL,NULL,'HWP','Hand wash Program','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(10,'HEALTH',NULL,NULL,NULL,'HAND_WASH','Is there a Hand wash area?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(11,'HEALTH',NULL,NULL,NULL,'RENOVATION_REQD','If yes, is renovation required?','radio',NULL,'N','HAND_WASH',_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(12,'HEALTH',NULL,NULL,NULL,'UTENSIL_AREA','Is there a separate utensil washing area?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(13,'HEALTH','Toilet Details','TOILET_BOYS','Toilet for boys','TOILET_BOYS_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(14,'HEALTH','Toilet Details','TOILET_BOYS','Toilet for boys','TOILET_BOYS_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(15,'HEALTH','Toilet Details','TOILET_BOYS','Toilet for boys','TOILET_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(16,'HEALTH','Toilet Details','URINALS_BOYS','Urinals for Boys','URINALS_BOYS_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(17,'HEALTH','Toilet Details','URINALS_BOYS','Urinals for Boys','URINALS_BOYS_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(18,'HEALTH','Toilet Details','URINALS_BOYS','Urinals for Boys','URINALS_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(19,'HEALTH','Toilet Details','TOILET_GIRLS','Toilet for girls','TOILET_GIRLS_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(20,'HEALTH','Toilet Details','TOILET_GIRLS','Toilet for girls','TOILET_GIRLS_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(21,'HEALTH','Toilet Details','TOILET_GIRLS','Toilet for girls','TOILET_GIRLS_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(22,'HEALTH','Toilet Details','URINALS_GIRLS','Urinals for girls','URINALS_GIRLS_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(23,'HEALTH','Toilet Details','URINALS_GIRLS','Urinals for girls','URINALS_GIRLS_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(24,'HEALTH','Toilet Details','URINALS_GIRLS','Urinals for girls','URINALS_GIRLS_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(25,'HEALTH','Toilet Details','TOILETS_CWSN','Toilets for CWSN','TOILETS_CWSN_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(26,'HEALTH','Toilet Details','TOILETS_CWSN','Toilets for CWSN','TOILETS_CWSN_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(27,'HEALTH','Toilet Details','TOILETS_CWSN','Toilets for CWSN','TOILETS_CWSN_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(28,'HEALTH','Toilet Details','TOILETS_STAFF','Toilets for Staff','TOILETS_STAFF_AVAILABLE','No of Toilets Available','textbox','number','N',NULL,NULL,NULL),(29,'HEALTH','Toilet Details','TOILETS_STAFF','Toilets for Staff','TOILETS_STAFF_REPAIR_REQD','No of Toilets Req. Repair','textbox','number','N',NULL,NULL,NULL),(30,'HEALTH','Toilet Details','TOILETS_STAFF','Toilets for Staff','TOILETS_STAFF_BOYS_NEW_REQD','No of New Toilets Required','textbox','number','N',NULL,NULL,NULL),(31,'HEALTH',NULL,NULL,NULL,'WATER_PURIFIER','Is water purifier available?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(32,'HEALTH','Details','WATER_PURIFIER_RO','RO Unit','WATER_PURIFIER_RO_TOTAL','Total Available','textbox','number','N','WATER_PURIFIER',NULL,NULL),(33,'HEALTH','Details','WATER_PURIFIER_RO','RO Unit','WATER_PURIFIER_RO_REPAIR_REQD','Total req. Repair','textbox','number','N','WATER_PURIFIER',NULL,NULL),(34,'HEALTH','Details','WATER_PURIFIER_RO','RO Unit','WATER_PURIFIER_RO_NEW_REQ','New Requirement','textbox','number','N','WATER_PURIFIER',NULL,NULL),(35,'HEALTH','Details','WATER_PURIFIER_UV','UV Unit','WATER_PURIFIER_UV_TOTAL','Total Available','textbox','number','N','WATER_PURIFIER',NULL,NULL),(36,'HEALTH','Details','WATER_PURIFIER_UV','UV Unit','WATER_PURIFIER_UV_REPAIR_REQD','Total req. Repair','textbox','number','N','WATER_PURIFIER',NULL,NULL),(37,'HEALTH','Details','WATER_PURIFIER_UV','UV Unit','WATER_PURIFIER_UV_NEW_REQ','New Requirement','textbox','number','N','WATER_PURIFIER',NULL,NULL),(38,'HEALTH','Details','WATER_PURIFIER_STRG','Storage facility','WATER_PURIFIER_STRG_TOTAL','Total Available','textbox','number','N','WATER_PURIFIER',NULL,NULL),(39,'HEALTH','Details','WATER_PURIFIER_STRG','Storage facility','WATER_PURIFIER_STRG_REPAIR_REQD','Total req. Repair','textbox','number','N','WATER_PURIFIER',NULL,NULL),(40,'HEALTH','Details','WATER_PURIFIER_STRG','Storage facility','WATER_PURIFIER_STRG_NEW_REQ','New Requirement','textbox','number','N','WATER_PURIFIER',NULL,NULL),(41,'HEALTH',NULL,NULL,NULL,'SRC_OF_WATER','Source of water :','dropdown',NULL,'N',NULL,_binary '[{\"key\":\"Bore well\",\"value\":\"BW\"},{\"key\":\"Corporation/GP/Water Connection\",\"value\":\"CP\"},{\"key\":\"Water Tanker\",\"value\":\"WT\"},{\"key\":\"Open Well\",\"value\":\"OW\"}]',NULL),(42,'HEALTH',NULL,NULL,NULL,'WATER_STORAGE','Water storage System :','dropdown',NULL,'N',NULL,_binary '[{\"key\":\"Sump\",\"value\":\"SMP\"},{\"key\":\"Overhead tank\",\"value\":\"OHT\"}]',NULL),(43,'HEALTH',NULL,NULL,NULL,'WATER_TESTED','Is water tested regularly?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(44,'HEALTH',NULL,NULL,NULL,'CLEANER','Is there a staff to clean toilet?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(45,'HEALTH',NULL,NULL,NULL,'DUSTBINS','Is there dustbins available for each class?','radio',NULL,'N',NULL,_binary '[{\"key\":\"Yes\",\"value\":\"Y\"},{\"key\":\"No\",\"value\":\"N\"}]',NULL),(46,'HEALTH',NULL,NULL,NULL,'WASTE_DISPOSABLE','Water Disposable system?','checkbox',NULL,'N',NULL,_binary '[{\n\"key\":\"Through corporation/GP\",\"value\":\"THC\"\n},{\n\"key\":\"Having own waste management system\",\"value\":\"OWM\"\n},{\n\"key\":\"None of the above\",\"value\":\"WDN\"\n}]',NULL);
/*!40000 ALTER TABLE `FORM_FIELDS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
