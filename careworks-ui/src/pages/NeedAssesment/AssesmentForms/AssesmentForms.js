import React, {Component} from 'react';
import {getFormFields,saveNeedAssessment,getNeedAssessmentSavedData} from '../../../utils/APIUtils';
import Form from './form';
import { Snackbar } from '@material-ui/core';
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class AssesmentForms extends Component {

  constructor (){
    super();
    this.state = {
      fields : [],
      schoolId : '',
      severe : 'success'
    }
  }

  componentDidMount(){
    var category = this.props.category;
    const schoolId = this.props.schoolId ;
    const naFlag = this.props.naFlag;
    //console.log("School Id : " + schoolId + " NA Flag : " + naFlag);
    this.setState({schoolId:schoolId ,naFlag :naFlag});
    //console.log("Input from AssessmentTab "+category+" "+schoolId + this.state.schoolId);
    this.renderFields(category, schoolId);
  }

  renderFields(category,schoolId) {
    this.getFields(category);
    this.getSavedData(schoolId, category);
  }


  getFields = (category) => {
    getFormFields(category).then(response => {
      if (response.length > 0) {
        this.setState(
          {
            fields: response
          }
        )
        //console.log(this.state.fields);
      }

    })
  }

  getSavedData = (schoolId, category) =>{
    getNeedAssessmentSavedData(schoolId, category).then(response => {
    if (response.length > 0) {
      this.setState(
        {
          savedData: response
        }
      )
      //console.log('Saved Data :  School Id ' + schoolId + ' Data' + this.state.savedData);
    } else {
      console.log('No Saved Data :  School Id ' + schoolId + ' Data' + this.state.savedData);
    }
    })
  }

  onSubmit = (e,data) => {
    e.preventDefault();
    //var data = this.state;
    //console.log(data);
    //console.log(JSON.stringify(data));
    //console.log('Saving response for ' + this.state.schoolId)
    //console.log('Saving response for category ' + this.props.category)
    saveNeedAssessment(data, this.state.schoolId, this.props.category).then(response => {
      if (response.length > 0) {
        //console.log(response);
        console.log('Data saved');

        this.getSavedData(this.state.schoolId, this.props.category);
        this.setState({
          fields:this.state.fields,
          savedData: this.state.savedData ,
          message: this.props.category + ' data saved Successfully',
          severe : 'success'
        },()=>{
          this.handleClickOpen("Data Saved Successfully");
         });
      }else{
           this.setState({
                message:'Error Occurred while Saving',
                severe : 'error'
                },()=>{
           this.handleClickOpen("Data Save is not successful");
           });
      }
    })
  }


  handleClickOpen = (msg) => {
      this.setState(
        {
          saveStatus: msg,
          dialog: true
        }
      )
    };

    handleDialogClose = () => {
      this.setState(
        {
          dialog: false
        }
      )
    };

  render(){
    return (
      <div>
        <Form
              //  title = "Please Enter the details "
                fields ={this.state.fields}
                savedData ={this.state.savedData}
                naFlag = {this.props.naFlag}
                onSubmit = {(e,model) => {this.onSubmit(e,model)}}
              />

                <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={this.state.dialog} autoHideDuration={10000} onClose={this.handleDialogClose}>
                        <Alert onClose={this.handleDialogClose} severity={this.state.severe}>
                          {this.state.message}
                        </Alert>
              </Snackbar>
      </div>

    );
  }
}
export default AssesmentForms;
