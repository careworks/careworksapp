import React from 'react';
import {
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Input,
  Radio,
  Select,
  Typography
} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import GridBase from "@material-ui/core/Grid";
import styled from "@material-ui/core/styles/styled";

const Grid = styled(GridBase)`
  .MuiGrid-root {
    flex-grow: 1;
  }
`;

export class DynamicForm extends React.Component {

  useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(3)

    },
    formGroup: {
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'column',
      justifyContent: 'space-between'
    },
    content: {
      marginTop: theme.spacing(2)
    },
    formInput: {
      marginTop: '5px',
      lineHeight: '1em'
    },
    radioButton: {
      display: 'flex',
      position: 'relative',
      marginLeft: '15px',
    }
  }));

  state = {};

  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      category: this.props.category
    }
  }

  componentWillReceiveProps(props) {
    // this.setState({savedData :props.savedData,fields :props.fields});
    props.fields.map((m) => {
      this.setState({[m.fieldId]: undefined});
      // if(m.fieldType === 'radio')   this.validateOne(m.fieldId);
      /* if(m.parentField !== null){
             this.setState({[m.fieldId] : m.value});
        }*/
      return " ";
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    var data = this.state;
    if (Object.keys(this.state.errors).length === 0)
      this.props.onSubmit(e, data);
  }

  validation = (e, r) => {
    if (r === "number") {
      var reg = /^\d+$/;
      var regex = new RegExp(reg);
      var errors = this.state.errors;
      if (!regex.test(this.state[e])) {
        errors[e] = 'Invalid'
        this.setState({errors: errors});
      } else {
        delete errors[e];
        this.setState({errors: errors});
      }
    }

  }

  validateOne = (name) => {
    var errors = this.state.errors;
    if (!this.state.hasOwnProperty(name) || this.state[name] === null || this.state[name] === undefined) {
      errors[name] = 'Field Required';
      this.setState({errors: errors});
    } else {
      delete errors[name];
      this.setState({errors: errors});
    }

  }

  onChange = (e, key, input, type = "single") => {
    //console.log("input" + e.target.value);
    //console.log(type);
    if (type === "single") {
      this.setState({
        [key]: e.target.value
      }, () => {
        if (input === 'radio') {
          this.validateOne(key);
        }
      });
    } else {
      let found = this.state[key] ?
        this.state[key].find((d) => d === e.target.value) : false;

      //console.log("found" + found);

      if (found) {
        let data = this.state[key].filter((d) => {
          return d !== found;
        });
        //console.log("found in if");
        this.setState({
          [key]: data
        }, () => {
          if (input === 'radio')
            this.validateOne(key);
        });
      } else {
        //console.log("found in else");
        //console.log("existing value " + this.state[key]);
        //TODO fix error for multiple select checkbox
        this.setState({
          [key]: [e.target.value, ...this.state[key]]
        }, () => {
          if (input === 'radio')
            this.validateOne(key);
        });
      }
    }
  }


  renderForm = () => {

    let fields = this.props.fields;
    let formUI = fields.map((m) => {
      let key = m.fieldId;
      let type = m.fieldType || "text";
      let props = m.props || {};
      let name = m.fieldId;
      let value = this.state[m.fieldId];
      let json = this.props.savedData;
      if (this.props.savedData) {
        //console.log("Saved Data " + this.props.savedData);
        for (var i = 0; i < json.length; i++) {
          if (json[i].hasOwnProperty("fieldId") && json[i]["fieldId"] === m.fieldId && value === undefined) {
            value = json[i]["fieldValue"];
            //console.log("savedVal " + json[i]["fieldValue"]);
            break;
          }
        }
      }
      //  let value =   this.props.savedData && this.props.savedData.filter(x => x.fieldId === m.fieldId)? savedVal : '';
      let visibility = 'visible';
      let numberCheck = m.fieldValidation || null;
      let required = true;

      if (m.parentField !== null) {
        //console.log("parent Field " + m.fieldLabel + "  " + m.parentField + " " + this.state[m.parentField]);

        if (this.state[m.parentField] === "Y") {
          //console.log("parent Field " + m.fieldLabel + "  " + m.parentField + " " + this.state[m.parentField]);
          visibility = 'visible';
        } else if (this.props.savedData && value !== undefined) {
          if (this.props.naFlag === "Y" || this.props.naFlag === "S") {
            visibility = 'visible';
          } else if (this.props.naFlag === "I" || this.props.naFlag === "R") {
            visibility = 'visible';
          } else {
            visibility = 'hidden';
          }
        } else {
          visibility = 'hidden';
        }
      }
      const classes = this.useStyles;

      let input = <Input  {...props}
                          className={classes.formInput}
                          type={type}
                          key={key}
                          name={name}
                          numberCheck={numberCheck}
                          onKeyUp={(e) => {
                            this.validation(key, numberCheck)
                          }}
                          value={value}
                          onChange={(e) => {
                            this.onChange(e, name, "single")
                          }}
                          required
                          disabled={(this.props.naFlag === "Y" || this.props.naFlag === "S") ? "disabled" : ""}
      />;

      if (type === "radio") {
        let radioOptions = JSON.parse(m.fieldOptions);
        // let radioOptions = [{"key":"Yes","value":"Y"},{"key":"No","value":"N"}];
        input = radioOptions.map((o, index) => {
          let checked = o.value === value;
        

          return (
            <React.Fragment key={'fr' + m.fieldId + o.key}>
              <FormControlLabel control={
                <Radio {...props}
                       className={classes.formInput}
                       key={o.key}
                       name={m.fieldId}
                       required
                       value={o.value}
                       checked={checked}
                       disabled={(this.props.naFlag === "Y" || this.props.naFlag === "S") ? "disabled" : ""}
                       onChange={(e) => {
                         this.onChange(e, m.fieldId, "radio", "single");
                       }}

                />
              } labelPlacement="end" key={"ll" + o.key}
                                label={o.key}/>

            </React.Fragment>
          );
        });

        input = <div>{input}</div>;
      }

      if (type === "dropdown") {
        let dropdownOptions = JSON.parse(m.fieldOptions);
        //let dropdownOptions = [{"key":"Bore well","value":"BW"},{"key":"Corporation/GP/Water Connection","value":"CP"},{"key":"Water Tanker","value":"WT"}];
        input = dropdownOptions.map((o, index) => {
          let checked = o.value;
          if (this.props.naFlag !== "N") {
            checked = o.value === value;
            //console.log("Disabled mode select value" + value + o.value);
          }
          // let val = value !==" " ? value :o.value ;
          return (
            <option {...props}
                    className={classes.formInput}
                    key={o.key}
                    value={o.value}
                    selected={checked}
                    required={index === 0 ? required : undefined}
                    disabled={(this.props.naFlag === "Y" || this.props.naFlag === "S") ? "disabled" : ""}
            >{o.key}</option>
          );
        });
        input = <Select native value={this.state[name]} onChange={(e) => {
          this.onChange(e, m.fieldId, "single")
        }}>{input}</Select>;
      }

      if (type === "checkbox") {
        let checkBoxOptions = JSON.parse(m.fieldOptions);
        //let checkBoxOptions = [{"key":"Dental","value":"D"},{"key":"Eye","value":"E"},{"key":"General","value":"G"}];
        input = checkBoxOptions.map((o, index) => {

          let checked = false;
          if (value) {
            checked = value.indexOf(o.value) > -1 ? true : false;
            //console.log(value + " " + checked);
          }
          return (
            <React.Fragment key={"cfr" + o.key}>
              <FormGroup>

                <FormControlLabel control={
                  <Checkbox {...props}
                            key={o.key}
                            name={o.key}
                            checked={checked}
                            value={o.value}
                    // required = {index === 0 ? required : undefined}
                            disabled={(this.props.naFlag === "Y" || this.props.naFlag === "S") ? "disabled" : ""}
                            onChange={(e) => {
                              this.onChange(e, m.fieldId, "multiple")
                            }}
                  />}
                                  labelPlacement="end"
                                  label={o.key} key={"ll" + o.key}/>
              </FormGroup>
            </React.Fragment>
          );
        });

        input = <div> {input}</div>;

      }
      if (visibility === 'visible')
        return (
          <>

            {m.fieldSectionHeader !== null && m.childSeq === 1 ? <Grid item xs={12}><Typography
              variant="h4">{m.fieldSectionHeader}</Typography></Grid> : <></>}
            <Grid item>
              <div key={'g' + key} >
                <FormLabel
                  style={{'font-weight': 'bold'}}
                  required
                  key={"l" + key}
                  htmlFor={"h" + key}>
                  {m.fieldLabel}
                </FormLabel>
                {input}

                <FormLabel error
                           key={"la" + key}
                           htmlFor={"ht" + key}>
                  {this.state.errors !== undefined ? this.state.errors[key] : null}
                </FormLabel>
              </div>
            </Grid>
          </>
        );
      return "";
    });
    return formUI;
  }

  render() {
    const classes = this.useStyles;
    //  let title = this.props.title || "Dynamic Form";


    return (
      <div className="container-fluid">
        <form className={classes.root} onSubmit={(e) => {
          this.onSubmit(e)
        }}>
          <Grid container spacing={5}
            // direction="row"
            // justify="left"
            // alignItems="left"
          >
            {this.renderForm()}
          </Grid>
          <div className="form-actions">
            <Button style={(this.props.naFlag === 'Y' || this.props.naFlag === "S") ? {display: 'none'} : {}}
                    variant="contained" color="primary" type="submit">Save</Button>
          </div>
        </form>
      </div>
    )
  }
}

export default DynamicForm;
