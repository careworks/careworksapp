import React, {useCallback,useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import AssesmentForms from '../AssesmentForms';
import SchoolEnvironmentForm from './SchoolEnvironmentForm';
import {approveNeedAssessment, rejectNeedAssessment, submitNeedAssessment,exportNaBySchoolId} from '../../../utils/APIUtils';
import {Button,Snackbar} from '@material-ui/core';
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  summary: {
    overflow: 'scroll',
  },
  heading: {
    fontSize: theme.typography.pxToRem(20),
    fontWeight: theme.typography.fontWeightRegular,
  },
  submitButton: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  cancelButton: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

const loadDashboard = (props) => {
  props.history.push('/app/schools');
}

export default function SimpleExpansionPanel(props) {
  //console.log('props in assessment tabs', props);
  let schoolId = props.match.params.school_id;
  let naFlag = props.match.params.na_Flag;
  let schoolName = props.match.params.school_Name;
  let schoolStatus = props.match.params.schoolStatus;
  const classes = useStyles();
  const onClick = useCallback((id) => {
    submitNeedAssessment(schoolId).then(response => {
      if (response) {
            setDialog(true);
            setMessage('Need Assessment Submitted Successfully for Review !');
      } else {
            setSevere('error');
            setDialog(true);
            setMessage('Unable to Submit ! Please ensure all sections are Saved.');
      }
      setTimeout(() => loadDashboard(props), 2000)
    }, error => {
      console.log(error)
    })
  }, [props.match.params.school_id]);
  //console.log(props.match.params);
  //console.log("School Id : " + schoolId + " NA Flag : " + naFlag);
  //console.log("School Status : " + schoolStatus);

  const[message, setMessage] = useState();
  const[dialog, setDialog] = useState(false);
  const[severe, setSevere] = useState('success');

  const handleDialogClose = () => {
      setDialog(false);
  };

  const triggerApprove = (schoolId) => {
    approveNeedAssessment(schoolId).then(response => {
      //console.log(response)
      if (response) {
            setDialog(true);
            setMessage('Need Assessment Approved Successfully !');
      } else {
            setSevere('error');
            setDialog(true);
            setMessage('Could not Approve ! Please ensure all sections are Saved.');
      }
      setTimeout(() => loadDashboard(props), 2000)
    }, error => {
      console.log(error)
    })
  }

  const triggerReject = (schoolId) => {
    rejectNeedAssessment(schoolId).then(response => {
      if (response) {
      console.log(response)
            setDialog(true);
            setMessage('Need Assessment Rejected and Available for Correction !');
      }
      setTimeout(() => loadDashboard(props), 2000)
    }, error => {
      console.log(error)
    })
  }

  const exportNaResponse = (schoolId) => {
    exportNaBySchoolId(schoolId).then(response => {
        if (response) {
        console.log(response)
              setDialog(true);
              setMessage('Data Exported Successfully !');
        }
        setTimeout(() => loadDashboard(props), 2000)
      }, error => {
        console.log(error)
      })
  }

  const renderNaForm = props.match.params.schoolStatus === "On Boarded";

  return ( renderNaForm ?
    (
    <div className={classes.root}>

         <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={dialog} onClose={handleDialogClose} autoHideDuration={10000}>
            <Alert onClose={handleDialogClose} severity={severe}>
              {message}
            </Alert>
         </Snackbar>


      <Typography align="center" variant={'h1'}>{schoolName + " Need Assessment"}</Typography>
      <Grid container direction="row" justify="flex-end" alignItems="flex-end">
        <Grid item xs={12} align="right">
          {sessionStorage.getItem('role').includes('ADMIN') && naFlag === 'S' ?
            <Button variant="contained" className={classes.submitButton} color="primary" onClick={(e) => {
              if (window.confirm('Do you want to proceed with Approve?')) triggerApprove(props.match.params.school_id)
            }}>Approve</Button> : null}
          {sessionStorage.getItem('role').includes('ADMIN') && naFlag === 'S' ?
            <Button variant="contained" className={classes.cancelButton} color="secondary" onClick={(e) => {
              if (window.confirm('Do you want to proceed with Reject?')) triggerReject(props.match.params.school_id)
            }}>Reject</Button> : null}
          {(naFlag === 'I' || naFlag === 'N') ?
            <Button variant="contained" className={classes.submitButton} color="primary" onClick={(e) => {
              if (window.confirm('Have you Saved data for all sections? Do you want to Submit?')) onClick()
            }}>Submit</Button> : null}
          <Button variant="contained" className={classes.submitButton} color="secondary"
                  onClick={() => exportNaResponse(props.match.params.school_id)}>Export</Button>
          <Button variant="contained" className={classes.cancelButton} color="secondary"
                  onClick={() => loadDashboard(props)}>Cancel</Button>
        </Grid>
        <Grid item xs={12}>
          <br/>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon/>}
              // aria-controls="panel1a-content"
              // id="panel1a-header"
            >
              <Typography className={classes.heading}><h3>Health & Sanitation</h3></Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.summary}>
              <AssesmentForms category="HEALTH" schoolId={schoolId} naFlag={naFlag}/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item xs={12}>
          <br/>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon/>}
              // aria-controls="panel2a-content"
              // id="panel2a-header"
            >
              <Typography className={classes.heading}><h3>Classroom Environment</h3></Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.summary}>
              <AssesmentForms category="CLASSROOM" schoolId={schoolId} naFlag={naFlag}/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item xs={12}>
          <br/>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon/>}
              // aria-controls="panel2a-content"
              // id="panel2a-header"
            >
              <Typography className={classes.heading}><h3>School Environment</h3></Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.summary}>
              <SchoolEnvironmentForm category="SCHOOL" schoolId={schoolId} naFlag={naFlag} isStarted="Y"/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
      </Grid>

    </div> ) :
    ( <div style = {{ width: '100%' }}>
        <Alert variant="outlined" severity="warning">
          <span style = {{ fontSize : 'large' }}> School Profile for School - <b> {props.match.params.school_Name} </b> is not submitted yet! </span>
        </Alert>
        <br/>
        <Button variant="contained" color="secondary" onClick={(e) => { loadDashboard(props) }}>Back</Button>
      </div>
    )
  );
}
