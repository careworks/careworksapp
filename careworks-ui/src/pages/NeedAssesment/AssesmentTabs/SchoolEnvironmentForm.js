import React from 'react';
import {Input,TextField, Table, TableBody, TableCell,TableHead, TableRow, Button,FormControlLabel, Checkbox, Snackbar } from '@material-ui/core';
import {getFormFields,saveNeedAssessment,getNeedAssessmentSavedData} from '../../../utils/APIUtils';
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const headCells = [
  { id: 'a', disablePadding: true, label: 'Particulars',type:'checkbox' },
  { id: 'yesNo', disablePadding: true, label: 'Is Available',type:'checkbox'  },
  { id: 'totalNo', disablePadding: false, label: 'Total No',type:'text'  },
  { id: 'renovation',  disablePadding: false, label: 'Renovation Req',type:'checkbox'  },
  { id: 'flooring', disablePadding: false, label: 'Flooring' ,type:'checkbox' },
  { id: 'plastering', disablePadding: false, label: 'Plastering' ,type:'checkbox' },
  { id: 'waterProofing', disablePadding: false, label: 'WaterProofing' ,type:'checkbox' },
  { id: 'electric', disablePadding: false, label: 'Electrification',type:'checkbox'  },
  { id: 'plumbing', disablePadding: false, label: 'Plumbing' ,type:'checkbox'  },
  { id: 'blackboardPaint', disablePadding: false, label: 'Blackboard Painting',type:'checkbox'  },
  { id: 'wallMountedTable', disablePadding: false, label: 'Wall Mounted Table',type:'checkbox'  },
  { id: 'nc', disablePadding: false, label: 'Tiles Roof',type:'checkbox'  },
  { id: 'safetyGrill', disablePadding: false, label: 'Safety grill' ,type:'checkbox' },
  { id: 'externalPainting',disablePadding: false, label: 'External painting' ,type:'checkbox' },
  { id: 'internalPainting', disablePadding: false, label: 'Internal Painting' ,type:'checkbox' },
  { id: 'remarks', disablePadding: false, label: 'Remarks',type:'text'  },
];



/*const criteriaList = [
  {name: 'HM Room/Office',id:'hmRoomOffice'},
  {name: 'Staff Room', id:'staffRoom'},
  {name: 'Library', id:'library'},
  {name: 'Computer Lab',id:'computerLab'},
  {name: 'Science Lab', id:'scienceLab'},
  {name: 'Store Room', id:'storeRoom'},
  {name: 'Kitchen', id:'kitchen'},
  {name: 'Corridor',id:'corridor'},
  ]*/

class SchoolEnvironmentForm extends React.Component{

 createRows= () =>{
  var rows =[];
  /*const criteriaList = [
  {name: 'HM Room/Office',id:'hmRoomOffice'},
  {name: 'Staff Room', id:'staffRoom'},
  {name: 'Library', id:'library'},
  {name: 'Computer Lab',id:'computerLab'},
  {name: 'Science Lab', id:'scienceLab'},
  {name: 'Store Room', id:'storeRoom'},
  {name: 'Kitchen', id:'kitchen'},
  {name: 'Corridor',id:'corridor'},
  ]*/

  const criteriaList = this.state.fields;
  for (var criteria of criteriaList){
     rows.push(this.createData(criteria.name, criteria.id, this.createCheckboxButton(), this.createNumberField(), this.createCheckboxButton(), this.createCheckboxButton(), this.createCheckboxButton(),this.createCheckboxButton(), this.createNumberField(), this.createCheckboxButton(), this.createCheckboxButton(), this.createCheckboxButton(), this.createCheckboxButton(), this.createCheckboxButton()));
  }
  //console.log(rows);
  return rows;
}

 createData = (name, id, yesNo, total, renovation, flooring,plastering,waterproofing,e,plumbering,bbp,wt,nc,sg,ip,remarks) => {
  return {     name, id, yesNo, total, renovation, flooring,plastering,waterproofing,e,plumbering,bbp,wt,nc,sg,ip,remarks };
}

    state ={};

    constructor(props) {
      super(props);
      this.state = {
        errors : {},
        category : this.props.category,
        fields : [],
        savedData : [],
        severe : 'success'
      }
     }

    getFields = (category) => {
      getFormFields(category).then(response => {
        if (response.length > 0) {
          var fields = [];
          var field = {};
          response.forEach(data => {
            field = {};
            field["name"] = data.fieldLabel;
            field["id"] = data.fieldId;

            fields.push(field);
          });
          this.setState({fields: fields});

          //console.log(`Field values for ${this.state.category}`);
          //console.log(this.state.fields);
        }

      })
    }

    checkBoxChange =(e, name) =>{
      //console.log(e.target);
      if(this.state[name] === "Y"){
        e.target.value = "N";
      }else{
        e.target.value = "Y";
      }
      this.setState({[name]: e.target.value},
      ()=>{
                //console.log("new  state value"+this.state[name] +" "+name)
                  this.updateCellWithNewInputs(e,name);
      });
     }

     onChange =(e,name) =>{
        //console.log(e.target.value);
        const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
        //console.log('School Environment Fields Disable ' + disabled + ' ' + this.props.naFlag);
        this.setState({[name] :e.target.value},() => {
           return(
             <input type="number" value = {this.state[name]} id ={name} style={{maxWidth : 60}}  disabled={disabled}  onChange={(e)=>{this.onChange(e, name)}}/>
          );
        });
     }

     onChangeMultiLineTextBox =(e,name) =>{
        //console.log(e.target.value);
        const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
        this.setState({[name] :e.target.value},() => {
           return(
              <TextField
                             id={name}
                             variant="outlined"
                             multiline
                             value = {this.state[name]}
                             disabled={disabled}
                             onChange={(e)=>{this.onChangeMultiLineTextBox(e, name)}}
                           />
          );
        });
     }


   getSavedData = (schoolId,category) =>{
      getNeedAssessmentSavedData(schoolId,category).then(response => {
      if (response.length > 0) {
        this.setState(
          {
            savedData: response
          },()=>{
         // console.log(this.state.savedData);
             Object.keys(this.state.savedData).forEach(elem1 =>{
                const savedFieldId = this.state.savedData[elem1].fieldId;
                const savedFieldValue = this.state.savedData[elem1].fieldValue;
                if (this.state[savedFieldId] !== savedFieldValue){
                    this.setState({[savedFieldId] :savedFieldValue });
                }
              });
          }
        );
      }
      })
    }

  createCellId = () => {
   Object.keys(this.state.fields).forEach(elem1 => {
        Object.keys(headCells).forEach(elem2 => {
             let fieldId = this.state.fields[elem1].id + headCells[elem2].id ;
                 if(headCells[elem2].type === 'checkbox'){
                    this.setState({[fieldId] : 'N'} );
                 }else {
                    this.setState({[fieldId] : ''} );
                 }
        })
      });
    }

    updateCellIdWithSavedData = () =>{
        this.getSavedData(this.props.schoolId, this.props.category);

    }
    updateCellWithNewInputs = (fieldId ,e) => {
       const checked =  this.state[fieldId] === "Y" ;
        // TODO change the flag as per backend api
       const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
       //console.log('School Environment Fields Disable ' + disabled + ' ' + this.props.naFlag);
       return  ( <FormControlLabel control={<Input type="checkbox" /> } value = {this.state[fieldId]} disabled={disabled} label='Yes' name = {fieldId}   checked= {checked}/>);
    }

   createCheckboxButton = (name) => {
       const checked =  this.state[name] === "Y" ;
        // TODO change the flag as per backend api
       const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
       //console.log('School Environment Fields Disable ' + disabled + ' ' + this.props.naFlag);
       return (<FormControlLabel control={<Checkbox /> }  checked={checked} disabled = {disabled} label='Yes' name = {name}     onChange={(e)=>{this.checkBoxChange(e, name)}}/>
      );
    }

    createNumberField = (name) => {
      const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
      //console.log('School Environment  TOTAL Field Disable ' + disabled + ' ' + this.props.naFlag);
      return(
       <input type="number" min="0" value = {this.state[name]} id ={name} style={{maxWidth : 60}}  disabled={disabled}  onChange={(e)=>{this.onChange(e, name)}}/>
      );
    }

    createMultiLineField=(name)=>{
      const disabled = (this.props.naFlag === 'Y' || this.props.naFlag ==="S");
      //console.log('School Environment Remarks Fields Disable ' + disabled + ' ' + this.props.naFlag);
      //console.log("Remark box "+ name + this.state.name);
      return(
       //<input type="number" id ={name} style={{width : '1'}}  />
         <TextField
                 id={name}
                 variant="outlined"
                 multiline
                 value = {this.state[name]}
                 disabled={disabled}
                 style = {{width: 250}}
                 inputStyle = {{width: '100%'}}
                 onChange={(e)=>{this.onChangeMultiLineTextBox(e, name)}}
               />
      );
     }

  componentDidMount(){
      this.getFields(this.state.category);

    //  this.setState({isEditable : this.props.naFlag},() =>{
         //console.log("isStarted flag"+this.props.isStarted);
         // TODO change the flag as per backend api
          if(this.props.isStarted ==="Y"){
          this.createCellId();
          this.updateCellIdWithSavedData();
          }else{
          this.createCellId();
          }
     // });


    }

    onSubmit = (e) => {
      e.preventDefault();
      var data = this.state;
      //console.log(data);
    //  if(Object.keys(this.state.errors).length ===0)
      //this.props.onSubmit(data);
      //console.log(JSON.stringify(data));
      //console.log('Saving response for ' + this.props.schoolId);
      //console.log('Saving response for category ' + this.props.category)
      saveNeedAssessment(data, this.props.schoolId, this.props.category).then(response => {
      if (response.length > 0) {
        //console.log('Data saved');
        this.setState(
          {
              message:'SCHOOL Environment Data saved Successfully',
              severe : 'success'
          });
         this.handleClickOpen("SCHOOL Environment data saved Successfully");
      }else{
         this.setState(
            {
              message:'Error Occurred while Saving',
              severe : 'error'
            }
         );
          this.handleClickOpen("Error Occurred while Saving");
      }
    })
  }

  handleClickOpen = (msg) => {
        this.setState(
          {
            saveStatus: msg,
            dialog: true
          }
        )
      };

      handleDialogClose = () => {
        this.setState(
          {
            dialog: false
          }
        )
      };

render () {
    return (
    <div>
      <form  onSubmit={(e)=>{this.onSubmit(e)}}>

          <Table  aria-label="simple table" responsive>
              <TableHead>
                <TableRow>
                  {headCells.map(headCell => (
                          <TableCell
                            key={headCell.id}
                            align={headCell.numeric ? 'right' : 'left'}
                            padding={headCell.disablePadding ? 'none' : 'default'}
                 > {headCell.label} </TableCell>
                   ))}
                 </TableRow>
              </TableHead>
              <TableBody>
                {this.createRows().map(row => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>

                    <TableCell align="right">{this.createCheckboxButton(row.id+'yesNo')}</TableCell>
                    <TableCell align="right">{this.createNumberField(row.id+'totalNo')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'renovation')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'flooring')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'plastering')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'waterproofing')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'electrical')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'plumbering')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'blackboardPaint')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'wallMountedTable')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'nc')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'safetyGrill')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'externalPaint')}</TableCell>
                    <TableCell align="right">{this.createCheckboxButton(row.id+'internalPaint')}</TableCell>
                    <TableCell align="right">{this.createMultiLineField(row.id+'remarks')}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>

          <Button variant="contained" color="primary"
           type="submit"
           style={ (this.props.naFlag === 'Y' || this.props.naFlag ==="S") ? {display : 'none'}: {} }
          >Save</Button>


      </form>
      <Snackbar
          anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.dialog}
          onClose={this.handleDialogClose}
          autoHideDuration={15000}
      >
             <Alert onClose={this.handleDialogClose} severity={this.state.severe}>
             {this.state.message}
            </Alert>
        </Snackbar>
      </div>
    )
  }
}

export default SchoolEnvironmentForm;
