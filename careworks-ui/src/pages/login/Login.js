import React, {useState} from "react";
import {Button, CircularProgress, Fade, Grid, Tab, Tabs, TextField, Typography,} from "@material-ui/core";
import {withRouter} from "react-router-dom";
// styles
import useStyles from "./styles";
// logo
import logo from "./logo.svg";
// context
import {loginUser, signupUser, useUserDispatch} from "../../context/UserContext";
import {checkEmailAvailability, forgotPassword, updateNewPassword} from "../../utils/APIUtils";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";

const NAME_MIN_LENGTH = 4;
const NAME_MAX_LENGTH = 4000;

const PHONE_MAX_LENGTH = 10;
const PASSWORD_MIN_LENGTH = 6;
const PASSWORD_MAX_LENGTH = 2000;

function Login(props) {
  let classes = useStyles();
  let userDispatch = useUserDispatch();

  let [isLoading, setIsLoading] = useState(false);
  let [error, setError] = useState(null);
  let [activeTabId, setActiveTabId] = useState(0);
  let [nameValue, setNameValue] = useState("");
  let [phoneValue, setPhoneValue] = useState("");
  let [loginValue, setLoginValue] = useState("");
  let [passwordValue, setPasswordValue] = useState("");
  const [open, setOpen] = useState(false);
  let [newPassword, setNewPassword] = useState("")
  let [otp, setOtp] = useState("")
  let [isEmailAvailable, setIsEmailAvailable] = useState(true)

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const updatePassword = () => {
    setIsLoading(true);
    updateNewPassword(otp, loginValue, newPassword).then(response => {
      setError(response.message)
      setIsLoading(false)
    }).catch(error => {
      setError(error.message.replace('An unexpected internal server error occured', ''))
      setIsLoading(false)
    })
    setOpen(false);
  }
  // Validation Functions

  let validateName = () => {
    if (nameValue.length < NAME_MIN_LENGTH) {
      setError(`Name is too short (Minimum ${NAME_MIN_LENGTH} characters needed.)`);
    } else if (nameValue.length > NAME_MAX_LENGTH) {
      setError(`Name is too long (Maximum ${NAME_MAX_LENGTH} characters allowed.)`);
    } else {
      setError(null);
    }
  };

  let validateEmail = () => {
    if (!loginValue) {
      setError('Email may not be empty');
      return;
    }

    const EMAIL_REGEX = RegExp('[^@ ]+@[^@ ]+\\.[^@ ]+');
    if (!EMAIL_REGEX.test(loginValue)) {
      setError('Email not valid');
      return
    }
    setError(null);
    validateEmailAvailability()
  };

  let validatePhone = () => {
    if (!phoneValue) {
      setError('Phone may not be empty');
      return
    }

    // const PHONE_REGEX = RegExp('[0-9]{10}');
    // if (!PHONE_REGEX.test(phoneValue)) {
    //   setError('Phone not valid');
    //   return
    // }

    if (phoneValue.length !== PHONE_MAX_LENGTH) {
      setError(`Phone should be ${PHONE_MAX_LENGTH} numbers`);
      return
    }

    setError(null)
  };

  function validateEmailAvailability() {
    checkEmailAvailability(loginValue)
      .then(response => {
        if (response.available) {
          setError(null)
          setIsEmailAvailable(true)
        } else {
          setError('This Email is not available');
          setIsEmailAvailable(false)
        }
      }).catch(error => {
      console.log("Email server error ", error);
      setError(null);
    });
  }

  let validatePassword = () => {
    if (passwordValue.length < PASSWORD_MIN_LENGTH) {
      setError(`Password is too short (Minimum ${PASSWORD_MIN_LENGTH} characters needed.)`);
    } else if (passwordValue.length > PASSWORD_MAX_LENGTH) {
      setError(`Password is too long (Maximum ${PASSWORD_MAX_LENGTH} characters allowed.)`);
    } else {
      setError(null);
    }
  };
  return (
    <Grid container className={classes.container}>
      <div className={classes.logotypeContainer}/>
      <div className={classes.formContainer}>
        <img src={logo} alt="logo" className={classes.logotypeImage}/>
        <Typography variant="h1" className={classes.greeting}>Careworks Foundation</Typography>
        <form>
          <div className={classes.form}>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
              <DialogTitle id="form-dialog-title">Reset Password</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  To Reset Your Password, Enter the OTP given by administrator
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="otp"
                  value={otp}
                  onChange={event => setOtp(event.target.value)}
                  label="OTP"
                  type="text"
                  fullWidth
                />
                <TextField
                  margin="dense"
                  id="newPassword"
                  value={newPassword}
                  onChange={event => setNewPassword(event.target.value)}
                  label="New Password"
                  type="password"
                  fullWidth
                />
              </DialogContent>
              <DialogActions style={{justifyContent: 'center'}}>
                <Button variant="contained"
                        color="primary"
                        size="large"
                        onClick={handleClose}>
                  Cancel
                </Button>
                <Button onClick={updatePassword} variant="contained"
                        color="primary"
                        size="large">
                  Update Password
                </Button>
              </DialogActions>
            </Dialog>
            <Tabs
              value={activeTabId}
              onChange={(e, id) => {
                setError(null);
                setActiveTabId(id)
              }}
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              <Tab label="Login" classes={{root: classes.tab}}/>
              <Tab label="New User" classes={{root: classes.tab}}/>
            </Tabs>
            {activeTabId === 0 && (
              <React.Fragment>
                <Typography variant="h1" className={classes.greeting}>
                  Welcome!
                </Typography>
                <Fade in={error}>
                  <Typography color="secondary" className={classes.errorMessage}>
                    Something is wrong with your login or password :(
                    <br/>
                    {error}
                  </Typography>
                </Fade>
                <TextField
                  id="email"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={loginValue}
                  onChange={e => {
                    setLoginValue(e.target.value)
                  }}
                  margin="normal"
                  placeholder="Email Address"
                  type="email"
                  fullWidth
                />
                <TextField
                  id="password"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={passwordValue}
                  onChange={e => setPasswordValue(e.target.value)}
                  margin="normal"
                  placeholder="Password"
                  type="password"
                  fullWidth
                />
                <div className={classes.formButtons}>
                  {isLoading ? (
                    <CircularProgress size={26} className={classes.loginLoader}/>
                  ) : (
                    <Button
                      type={"submit"}
                      disabled={
                        loginValue.length === 0 || passwordValue.length === 0
                      }
                      onClick={() =>
                        loginUser(
                          userDispatch,
                          loginValue,
                          passwordValue,
                          props.history,
                          setIsLoading,
                          setError,
                        )
                      }
                      variant="contained"
                      color="primary"
                      size="large"
                    >
                      Login
                    </Button>
                  )}
                  <Button
                    color="primary"
                    size="large"
                    disabled={
                      loginValue.length === 0 || passwordValue.length === 0
                    }
                    className={classes.forgetButton}
                    onClick={() => {
                      setIsLoading(true)
                      forgotPassword(loginValue).then(value => {
                        setIsLoading(false)
                        if (value) {
                          setError("Password Change Request Submitted Successfully")
                        } else {
                          setError("Username/Email not found")
                        }

                      }).catch(error => {
                        setIsLoading(false)
                        setError(error.message.replace('An unexpected internal server error occured', ''))
                      })
                    }}
                  >
                    Forgot Password
                  </Button>
                </div>
              </React.Fragment>
            )}
            {activeTabId === 1 && (
              <React.Fragment>
                <Typography variant="h1" className={classes.greeting}>
                  Welcome!
                </Typography>
                <Typography variant="h2" className={classes.subGreeting}>
                  Create your account
                </Typography>
                <Fade in={error}>
                  <Typography color="secondary" className={classes.errorMessage}>
                    {error}
                  </Typography>
                </Fade>
                <TextField
                  id="name"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={nameValue}
                  onChange={e => setNameValue(e.target.value)}
                  margin="normal"
                  placeholder="Full Name"
                  type="email"
                  fullWidth
                  onBlur={validateName}
                />
                <TextField
                  id="phone"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={phoneValue}
                  onChange={e => setPhoneValue(e.target.value)}
                  margin="normal"
                  placeholder="Phone Number"
                  type="number"
                  min="0"
                  max="10"
                  fullWidth
                  onBlur={validatePhone}
                />
                <TextField
                  id="email"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={loginValue}
                  onChange={e => setLoginValue(e.target.value)}
                  margin="normal"
                  placeholder="Email Address"
                  type="email"
                  fullWidth
                  onBlur={validateEmail}
                />
                <TextField
                  id="password"
                  InputProps={{
                    classes: {
                      underline: classes.textFieldUnderline,
                      input: classes.textField,
                    },
                  }}
                  value={passwordValue}
                  onChange={e => setPasswordValue(e.target.value)}
                  margin="normal"
                  placeholder="Password"
                  type="password"
                  fullWidth
                  onBlur={validatePassword}
                />
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26}/>
                  ) : (
                    <>
                      <Button
                        onClick={() => {
                          if (!error) {
                            signupUser(userDispatch,
                              nameValue,
                              loginValue,
                              passwordValue,
                              phoneValue,
                              setIsLoading,
                              setError
                            )
                          }
                        }
                        }
                        disabled={
                          loginValue.length === 0 ||
                          passwordValue.length === 0 ||
                          nameValue.length === 0 || !isEmailAvailable
                        }
                        size="large"
                        variant="contained"
                        color="primary"
                        fullWidth
                        className={classes.createAccountButton}
                      >
                        Create your account
                      </Button>
                      <Button color="primary"
                              size="large"
                              className={classes.forgetButton}
                              onClick={handleClickOpen}
                              disabled={isEmailAvailable}
                      >
                        Reset Password
                      </Button>
                    </>
                  )}
                </div>
              </React.Fragment>
            )}
            <Typography color="primary" className={classes.copyright}>
              © 2020 Careworks Foundation. All rights reserved.
            </Typography>
          </div>
        </form>
      </div>
    </Grid>
  );
}

export default withRouter(Login);
