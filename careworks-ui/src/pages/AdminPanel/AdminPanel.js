import React, {useState} from 'react';
import {ExpansionPanel, Grid, Snackbar, Typography} from '@material-ui/core';
import {
  getAllQuestions,
  getAllSchoolCategory,
  getAllSchoolLocation,
  getAllSchoolMedium,
  getAllSchoolType,
  getAllCodes,
  saveInfo,
  getActions,
  getCategories
} from "../../utils/APIUtils";
import DataTable from "./components/DataTable";
import MuiAlert from "@material-ui/lab/Alert";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import UserList from "./components/UserList";
import lodash from "lodash";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function AdminPanel(props) {
  let [dialog, setDialog] = useState(false)
  let [saveStatus, setSaveStatus] = useState('')
  let [severe, setSevere] = useState('success')

  const handleDialogClose = () => {
    setDialog(false)
  };

  const setDataSavedInGrid = (message, severe) => {
    if (!severe) {
      severe = "success";
    }
    setDialog(true)
    setSaveStatus(message)
    setSevere(severe)
  };

  const ignoreHeaderList = ['seq', 'categoryId', 'activityId'];


  return (
    <>
      <Typography align="center" variant={'h1'}>Administration</Typography>
      <Snackbar open={dialog} autoHideDuration={10000} onClose={handleDialogClose}>
        <Alert onClose={handleDialogClose} severity={severe}>
          {saveStatus}
        </Alert>
      </Snackbar>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          xs={12}
        >
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1a-content"
                                   id="panel1a-header">
              <Typography variant='h3'>Users</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <UserList/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1a-content"
                                   id="panel1a-header">
              <Typography variant='h3'>School Profile</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DataTable title={'Mediums'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllSchoolMedium();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'medium');
                             }}
                             deleteFunc={(ids) => {
                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'medium');
                             }}
                             resetFunc={() => {
                               return getAllSchoolMedium();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DataTable title={'Categories'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllSchoolCategory();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'category');
                             }}
                             deleteFunc={(ids) => {

                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'category');
                             }}
                             resetFunc={() => {
                               return getAllSchoolCategory();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DataTable title={'Locations'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllSchoolLocation();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'location');
                             }}
                             deleteFunc={(ids) => {

                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'location');
                             }}
                             resetFunc={() => {
                               return getAllSchoolLocation();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DataTable title={'Types'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllSchoolType();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'type');
                             }}
                             deleteFunc={(ids) => {

                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'type');
                             }}
                             resetFunc={() => {
                               return getAllSchoolType();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1a-content"
                                   id="panel1a-header">
              <Typography variant='h3'>Action Plan</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  <DataTable title={'Activities'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                              return getActions();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'activities');
                             }}
                             deleteFunc={(ids) => {
                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'activities');
                             }}
                             resetFunc={() => {
                               return getAllSchoolMedium();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1a-content"
                                   id="panel1a-header">
              <Typography variant='h3'>Need Assessment</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  <DataTable title={'Questions'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllQuestions();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'questions');
                             }}
                             deleteFunc={(ids) => {

                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'questions');
                             }}
                             resetFunc={() => {
                               return getAllQuestions();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>

        <Grid item xs={12}>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1a-content"
                                   id="panel1a-header">
              <Typography variant='h3'>Codes</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  <DataTable title={'Codes'}
                             saveButton={true}
                             isGridLoaded={() => {
                               console.log("loaded form")
                             }}
                             isDataSaved={setDataSavedInGrid}
                             loaderFunc={() => {
                               return getAllCodes();
                             }}
                             saveFunc={(data, id, name) => {
                               return saveInfo(data, true, 'codes');
                             }}
                             deleteFunc={(ids) => {

                               let dataToDelete = []
                               ids.forEach(id => dataToDelete.push({'seq': id}))
                               return saveInfo(dataToDelete, false, 'codes');
                             }}
                             resetFunc={() => {
                               return getAllCodes();
                             }}
                             ignoreHeaders={ignoreHeaderList}
                             uniqueId={'seq'}
                             schoolId={1}
                             rowId={'seq'}
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
      </Grid>
    </>
  );

}
