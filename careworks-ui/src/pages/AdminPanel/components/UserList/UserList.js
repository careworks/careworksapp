import React, {useEffect, useState} from "react";
import {Button, CircularProgress, Grid, Snackbar} from "@material-ui/core";
import MUIDataTable from "mui-datatables";
// components
import moment from 'moment';
// data
import {approvePassword, getAllUsers, roleUpgrade} from "../../../../utils/APIUtils";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/DoneOutline";
import BlockIcon from "@material-ui/icons/Block";
import LockOpen from "@material-ui/icons/LockOpen";
import AdminIcon from "@material-ui/icons/Publish";
import Typography from "@material-ui/core/Typography";
import useStyles from "../../../login/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function UserList() {
  let [users, setUsers] = useState([]);
  let [error, setError] = useState(null);
  let classes = useStyles();
  let [dialog, setDialog] = useState(false)
  let [saveStatus, setSaveStatus] = useState('')
  let [severe, setSevere] = useState('success')
  let [action, setAction] = useState('')
  let [selUsers, setSelUsers] = useState([])
  let [userNames, setUserNames] = useState([])

  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const setDataSavedInGrid = (message, severe) => {
    if (!severe) {
      severe = "success";
    }
    setDialog(true)
    setSaveStatus(message.replace('An unexpected internal server error occured', ''))
    setSevere(severe)
  };

  const takeAction = () => {
    if (action === "APPROVE") {
      approvePassword(selUsers).then(response => {
        setDataSavedInGrid("Forgot Password Approved. OTP : " + response.message)
      }).catch(error => setDataSavedInGrid(error.message, "error"))
    } else {
      roleUpgrade(selUsers, action).then(response => {
        setDataSavedInGrid(response.message)
      }).catch(error => {
        setDataSavedInGrid(error.message, "error")
      })
    }
    setOpen(false);
  };

  useEffect(() => {
    const loadUsers = () => {
      if (!users.length > 0)
        getAllUsers().then(response => (
            setUsers(response.map(data => {
              return [data.name, data.email, data.phone, moment(data.createdAt).format('DD-MMM-YYYY hh:mm A'), data.id, data.role]
            }))
          )
        ).catch(error => (
          setError(error.message)
        ));

    };
    loadUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDialogClose = () => {
    setDialog(false)
  };

  function customToolbar(selectedRows, displayData, setSelectedRows) {
    function upgradeAdmin(selectedUsers) {
      setAction("ADMIN")
      setSaveStatus("The Following Users will get ADMIN ACCESS")
      setSelUsers(selectedUsers)
      setUserNames(Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][0]))
      handleClickOpen()
    }

    function upgradeUser(selectedUsers) {
      setAction("USER")
      setSaveStatus("The Following Users will get USER ACCESS")
      setSelUsers(selectedUsers)
      setUserNames(Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][0]))
      handleClickOpen()
    }

    function disableUser(selectedUsers) {
      setAction("BLOCK")
      setSaveStatus("The Following Users will be BLOCKED!")
      setSelUsers(selectedUsers)
      setUserNames(Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][0]))
      handleClickOpen()
    }

    function approveForgotPassword(selectedUsers) {
      setUserNames(Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][0]))
      if (selectedUsers.length > 1) {
        setDataSavedInGrid("Cannot approve more than One user at a time", "error")
      } else {
        setAction("APPROVE")
        setSaveStatus("Are you sure to approve forgot password request for User")
        setSelUsers(selectedUsers)
        handleClickOpen()
      }
    }

    return (
      <React.Fragment>
        <Tooltip title={"Enable Users"} onClick={event => upgradeUser(
          Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][4])
        )}>
          <IconButton>
            <AddIcon/>
          </IconButton>
        </Tooltip>
        <Tooltip title={"Block Users"} onClick={event => disableUser(
          Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][4])
        )}>
          <IconButton>
            <BlockIcon/>
          </IconButton>
        </Tooltip>
        <Tooltip title={"Upgrade to Admin"} onClick={event => upgradeAdmin(
          Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][4])
        )}>
          <IconButton>
            <AdminIcon/>
          </IconButton>
        </Tooltip>
        <Tooltip title={"Approve Forgot Password"} onClick={event => approveForgotPassword(
          Object.keys(selectedRows['lookup']).map(row => displayData[row]['data'][4])
        )}>
          <IconButton>
            <LockOpen/>
          </IconButton>
        </Tooltip>
      </React.Fragment>
    );
  }

  return (
    <>
      <Typography color="secondary" className={classes.errorMessage}>
        {error}
      </Typography>
      <Snackbar open={dialog} autoHideDuration={10000} onClose={handleDialogClose}>
        <Alert onClose={handleDialogClose} severity={severe}>
          {saveStatus}
        </Alert>
      </Snackbar>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Confirm Action</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {saveStatus} <br/>
            {userNames.map(userName => (<Typography variant={"body1"}>{userName}</Typography>))}
          </DialogContentText>

        </DialogContent>
        <DialogActions style={{justifyContent: 'center'}}>
          <Button variant="contained"
                  color="primary"
                  size="large"
                  onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={takeAction} variant="contained"
                  color="primary"
                  size="large">
            Confirm Action
          </Button>
        </DialogActions>
      </Dialog>
      {users.length > 0 ? <Grid container spacing={4}>
        <Grid item xs={12}>
          <MUIDataTable
            title={null}
            data={users}
            columns={["Name", "Email", "Phone", {
              name: "Updated At",
              options: {
                filter: true,
                display: 'false'
              }
            }, {
              name: "ID",
              options: {
                filter: false,
                display: 'excluded'
              }
            },
              {
                name: "Role",
                options: {
                  filter: true,
                  display: 'false'
                }
              }
            ]}
            options={{
              filterType: "checkbox",
              responsive: "scroll",
              customToolbarSelect: customToolbar
            }}
          />
        </Grid>
      </Grid> : <CircularProgress size="5rem" style={{marginLeft: '40%', marginBottom: '20%', marginTop: '10%'}}/>}

    </>
  );
}
