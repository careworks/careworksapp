import React from 'react';
import MUIDataTable from "mui-datatables";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import lodash from "lodash";
import {Button, CircularProgress, IconButton, MuiThemeProvider, Typography} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import Tooltip from "@material-ui/core/Tooltip";
import {Add} from "@material-ui/icons";
import CardActions from "@material-ui/core/CardActions";
import {trimErrorMessage} from "../../../../utils/Utils";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

class DataTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      titleData: this.props.title,
      response: [],
      isLoading: false,
      error: null,
      emptyRow: [],
      fullEmpty: {},
      rowData: [],
      columnDefs: [],
      updatedAt: null,
      tableDataStore: [],
      status: props.status,
      defaultColDef: {
        editable: true
      },
      rowIdIdx: null
    };
  }

  componentDidMount() {
    this.loadGridDetails();
  }

  onAddRow() {
    let rowDef = this.state.rowData
    if (JSON.stringify(rowDef[rowDef.length - 1]) !== JSON.stringify(this.state.emptyRow)) {
      rowDef.push(this.state.emptyRow)
      let tableStore = this.state.tableDataStore;
      tableStore.push(this.state.fullEmpty)
      this.setState({tableDataStore: tableStore}, () => this.setState({rowData: rowDef}))
      this.forceUpdate();
    }
  }

  onRemoveSelected(rowsDeleted, data, dataIndex) {
    if (this.state.defaultColDef.editable) {
      let rowDef = this.state.rowData;
      const tableData = this.state.tableDataStore;
      let ids = Object.keys(rowsDeleted['lookup']).map(row => tableData[row][this.props.rowId]).filter(value => (value != null && value !== " " && value !== ""));
      if (ids.length === 0) {
        if (JSON.stringify(rowDef[rowDef.length - 1]) === JSON.stringify(this.state.emptyRow)) {
          rowDef.pop()
          tableData.pop()
          this.setState({
            rowData: rowDef,
            tableDataStore: tableData
          })
        }
        return true;
      }
      this.setState({isLoading: true})
      this.getRowData(false, ids)
    } else {
      this.props.isDataSaved("Data is not editable", 'error');
    }
  }

  getTableTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableHeadCell: {
          root: {
            fontWeight: 'bold'
          }
        }
      }
    });

  render() {
    const coloumns = this.prepareColumn(this.state.response);
    return (
      <>
        <Typography>{this.state.error ? JSON.stringify(this.state.error) : null}</Typography>
        <Card>
          <CardHeader titleTypographyProps={{align: "center"}}
                      title={this.props.title ? this.props.title : this.state.titleData}
                      subheaderTypographyProps={{align: "justify"}}
                      subheader={this.state.updatedAt && this.state.status ? "    Status : " + lodash.startCase(this.state.status.toLowerCase()) + " and Last Updated : " + moment(this.state.updatedAt).format('DD-MMM-YYYY hh:mm A') : null}
          />
          {coloumns.length > 0 ?
            <MuiThemeProvider theme={this.getTableTheme()}><MUIDataTable
              data={this.state.rowData}
              columns={coloumns}
              options={{
                filter: true,
                sort: false,
                filterType: 'dropdown',
                responsive: 'scroll',
                downloadOptions: {
                  filename: (this.props.title ? this.props.title : this.state.titleData) + ".csv",
                  separator: ',',
                  filterOptions: {useDisplayedColumnsOnly: true, useDisplayedRowsOnly: true}
                },
                onRowsDelete: this.onRemoveSelected.bind(this)
              }}
            /></MuiThemeProvider>
            :
            <CircularProgress size="5rem" style={{marginLeft: '40%', marginBottom: '20%', marginTop: '10%'}}/>}
          <Tooltip title="Add new Item">
            <IconButton disabled={!this.state.defaultColDef.editable} onClick={() => this.onAddRow()}>
              <Add/>
            </IconButton>
          </Tooltip>
          {this.props.saveButton ? <>
            <CardActions style={{justifyContent: 'center'}}>
              {this.state.isLoading ?
                <CircularProgress size={26} style={{
                  marginRight: '3%',
                  marginLeft: '3%'
                }}/> :
                <Button color="primary" variant="contained"
                        disabled={this.state.status && this.state.status.includes('approve')}
                        size="large" onClick={() => {
                  this.setState({isLoading: true})
                  this.getRowData()
                }}>Save</Button>}
            </CardActions>
          </> : <></>}
        </Card>
      </>
    );

  }

  getRowData(isFinalSubmit, toDeleteIds) {
    const rowData = JSON.parse(JSON.stringify(this.state.tableDataStore));
    const title = this.props.title ? this.props.title : this.state.titleData

    let formattedData = rowData;
    formattedData.forEach(obj => {
      if (typeof obj == "object") {
        Object.keys(obj).forEach(k => {
            if (k.includes("-")) {
              let i = k.split('-');
              let toObj = {};
              if (i.length > 2) {
                toObj[this.props.nestedCol[0]] = i[0];
                toObj[this.props.nestedCol[1]] = i[1];
                toObj[this.props.uniqueId] = obj[k];
              } else {
                toObj[this.props.nestedCol[0]] = i[0];
                toObj[this.props.nestedCol[1]] = i[1];
                toObj[this.props.dataField] = obj[k];
              }
              if (!Array.isArray(obj[this.props.nestedData])) {
                obj[this.props.nestedData] = [
                  toObj
                ];
              } else {
                let updated = false;
                obj[this.props.nestedData].forEach(nesObj => {
                  if (nesObj[this.props.nestedCol[0]] === i[0] && nesObj[this.props.nestedCol[1]] === i[1]) {
                    updated = true;
                    if (typeof nesObj[this.props.uniqueId] == "undefined") {
                      nesObj[this.props.uniqueId] = obj[k];
                    } else {
                      nesObj[this.props.dataField] = obj[k];
                    }
                  }
                });
                if (!updated) {
                  obj[this.props.nestedData].push(toObj);
                }
              }

              delete obj[k];
            }
          }
        )
      }
    });
    return this.props.saveFunc(formattedData, this.props.schoolId, this.state.titleData).then(res => {
      console.log("Data saved");
      if (toDeleteIds) {
        this.props.deleteFunc(toDeleteIds).then(res => {
          console.log("Data saved");
          this.props.isDataSaved('Rows Deleted Successfully');
          this.loadGridDetails();
          this.setState({isLoading: false})
        }).catch(err => {
          this.setState({isLoading: false})
          this.props.isDataSaved("Data Could not be saved\nThis means there is invalid data in some cell\nError : " + err.code + "\nDescription : " + trimErrorMessage(err.message), 'error');
          console.log(err);
        });
      } else {
        if (this.props.setChange) {
          this.props.setChange(this.props.change.filter(e => e !== title))
        }
        this.loadGridDetails();
        this.setState({isLoading: false})
        this.props.isDataSaved(title + ' Data saved successfully.')
        if (isFinalSubmit) this.props.finalSubmit();
      }
      return true;
    }).catch(err => {
      this.setState({isLoading: false})
      this.props.isDataSaved(title + ' data could not be saved. Error : ' + trimErrorMessage(err.message), 'error');
      console.log(err);
      return Promise.reject(err.message)
    });
  }

  numberParse(data) {
    let num = parseInt(data)
    if (isNaN(num))
      return ''

    return num
  }

  prepareColumn(data) {
    let i = 0;
    let colDefs = [];

    if (data.length > 0) {
      data.forEach(rowDat1 => {
        if (rowDat1[this.props.resetKeepHeader]) {
          let tmpObj = {};
          tmpObj[this.props.resetKeepHeader] = rowDat1[this.props.resetKeepHeader];
        }
      });
      const colBaseData = data[0];
      Object.keys(colBaseData).forEach(key => {
        if (Array.isArray(colBaseData[key]) && this.props.nestedCol) {
          colBaseData[key].sort(this.compareValues(this.props.nestedCol[0]));
          colBaseData[key].forEach(ob => {
            colDefs[i++] = {
              label: lodash.startCase(ob[this.props.nestedCol[0]] + " - " + ob[this.props.nestedCol[1]]),
              name: ob[this.props.nestedCol[0]] + "-" + ob[this.props.nestedCol[1]],
              options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                  //console.log(value);
                  //console.log(tableMeta)
                  return (
                    <TextField
                      value={this.numberParse(this.state.tableDataStore[tableMeta.rowIndex][tableMeta.columnData.name])}
                      type="number"
                      // id={key}
                      // key={key}
                      multiline
                      rows={1}
                      rowsMax={100}
                      disabled={!this.state.defaultColDef.editable}
                      onChange={event => {
                        let tableData = this.state.tableDataStore
                        if (!tableData[tableMeta.rowIndex]) {
                          tableData[tableMeta.rowIndex] = {}
                        }
                        tableData[tableMeta.rowIndex][tableMeta.columnData.name] = this.numberParse(event.target.value)
                        this.setState({
                          tableDataStore: tableData
                        })
                      }}
                    />
                  );
                }
              }
            };
            colDefs[i++] = {
              label: lodash.startCase(ob[this.props.nestedCol[0]] + "-" + ob[this.props.nestedCol[1]] + "-" + this.props.uniqueId),
              name: ob[this.props.nestedCol[0]] + "-" + ob[this.props.nestedCol[1]] + "-" + this.props.uniqueId,
              options: {
                filter: !this.props.ignoreHeaders.includes(this.props.uniqueId),
                display: '' + !this.props.ignoreHeaders.includes(this.props.uniqueId),
                customBodyRender: (value, tableMeta, updateValue) => {
                  //console.log(value);
                  //console.log(tableMeta)
                  return (
                    <TextField
                      value={this.numberParse(this.state.tableDataStore[tableMeta.rowIndex][tableMeta.columnData.name])}
                      type="number"
                      // id={key}
                      // key={key}
                      multiline
                      rows={1}
                      rowsMax={100}
                      disabled={!this.state.defaultColDef.editable}
                      onChange={event => {
                        let tableData = this.state.tableDataStore
                        if (!tableData[tableMeta.rowIndex]) {
                          tableData[tableMeta.rowIndex] = {}
                        }
                        tableData[tableMeta.rowIndex][tableMeta.columnData.name] = this.numberParse(event.target.value)
                        this.setState({
                          tableDataStore: tableData
                        })
                      }}
                    />
                  );
                }
              }
            }
          });
        } else {
          colDefs[i++] = {
            label: lodash.startCase(key),
            name: key,
            options: {
              filter: !this.props.ignoreHeaders.includes(key),
              display: '' + !this.props.ignoreHeaders.includes(key),
              customBodyRender: (value, tableMeta, updateValue) => {
                //console.log(value);
                //console.log(tableMeta)
                return (
                  <TextField
                    style={{
                      display: 'block',
                      width: 'max-content',
                      overflow: 'hidden',
                      resize: 'both',
                    }}
                    fullWidth
                    value={this.props.numberFields && this.props.numberFields.includes(key) ? this.numberParse(this.state.tableDataStore[tableMeta.rowIndex][tableMeta.columnData.name]) : this.state.tableDataStore[tableMeta.rowIndex][tableMeta.columnData.name]}
                    // id={key}
                    // key={key}
                    type={(this.props.numberFields && this.props.numberFields.includes(key)) || !isNaN(parseInt(value)) ? "number" : "text"}
                    multiline
                    rows={1}
                    rowsMax={100}
                    disabled={!this.state.defaultColDef.editable}
                    onChange={event => {
                      let tableData = this.state.tableDataStore
                      if (!tableData[tableMeta.rowIndex]) {
                        tableData[tableMeta.rowIndex] = {}
                      }
                      let val = event.target.value ? event.target.value : '';
                      if ((this.props.numberFields && this.props.numberFields.includes(key)) || (!isNaN(parseInt(value)) && this.props.nestedCol && !this.props.nestedCol.includes(key))) {
                        val = this.numberParse(val)
                      }
                      tableData[tableMeta.rowIndex][tableMeta.columnData.name] = val
                      this.setState({
                        tableDataStore: tableData
                      })
                      if (this.props.setChange) {
                        const chang = this.props.change
                        if (!chang.includes(this.state.titleData)) {
                          chang.push(this.state.titleData)
                          this.props.setChange(chang)
                        }
                      }
                    }}
                  />
                );
              }
            }
          }
        }
      });
    }
    colDefs.forEach(columnDef => {
      if (this.props.nestedCol && columnDef.label.includes('Id')) {
        columnDef['options']['filter'] = false;
        columnDef['options']['display'] = 'false'
      }
    })
    return colDefs;
  }

  prepareRowData(data) {
    let rowDef = [];
    let updatedAt = null;

    data.forEach(dat => {
        if (typeof dat == "object") {
          let copyDat = dat;
          Object.keys(dat).forEach(key => {
            if (Array.isArray(dat[key]) && this.props.nestedCol) {
              dat[key].forEach(ob => {
                copyDat[ob[this.props.nestedCol[0]] + "-" + ob[this.props.nestedCol[1]]] = ob[this.props.dataField];
                copyDat[ob[this.props.nestedCol[0]] + "-" + ob[this.props.nestedCol[1]] + "-" + this.props.uniqueId] = ob[this.props.uniqueId];
              });
              delete copyDat[key];
            }
            if (key && key.includes('updatedAt') && dat[key]) {
              updatedAt = dat[key]
            }
          });
          rowDef.push(copyDat);
        }
      }
    );
    let defRowData = {};
    if (rowDef.length > 0) {
      const rowDat = rowDef[0];
      Object.keys(rowDat).forEach((key, i) => {
        defRowData[key] = ''
        if (key && key.includes('status') && rowDat[key]) {
          this.setState({status: rowDat[key].toLowerCase()})
          if (rowDat[key].toLowerCase().includes('approve')) {
            console.log('locking edit', rowDat[key])
            let defs = this.state.defaultColDef;
            defs['editable'] = false;
            this.setState({defaultColDef: defs})
          }
        }
        if (key === this.props.rowId) {
          this.setState({rowIdIdx: i})
        }
      });
    }
    let condensedRowData = Object.values(rowDef);
    condensedRowData = condensedRowData.map(row => Object.values(row))

    let condensedDefRowData = Object.values(defRowData)

    this.setState(
      {
        rowData: condensedRowData,
        tableDataStore: rowDef,
        fullEmpty: defRowData,
        emptyRow: condensedDefRowData,
        updatedAt: updatedAt
      }, () => {
        const row = rowDef[0];
        if (this.props.titleColumn && row) {
          console.log("setting title", row[this.props.titleColumn])
          this.setState({
            titleData: row[this.props.titleColumn]
          })
        }
      }
    )
  }

  resetNeed = false;


  loadGridDetails() {
    if (typeof this.props.loaderFunc == "function") {
      this.props.loaderFunc().then(response => {
        this.setState({response: response})
        if (response) {
          if (!(Array.isArray(response) && response.length > 0)) {
            this.resetNeed = true;
            this.props.resetFunc().then(response => {
              this.setState({response: response})
              if (response) {
                this.prepareRowData(response)
              }
            }).catch(error => this.setState({error: error}));
          } else {
            this.prepareRowData(response)
          }
          // if (response && response.length === 0) {
          //   window.location.reload(true)
          // }
          this.props.isGridLoaded()
        }
      }).catch(error => this.setState({error: error}));
    }
  }

  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }
}

export default DataTable;
