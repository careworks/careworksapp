import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import DeleteIcon from '@material-ui/icons/Delete';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Checkbox, FormControlLabel, Grid, TextField } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import DateFnsUtils from '@date-io/date-fns';
import Typography from '@material-ui/core/Typography';
import { KeyboardDatePicker, MuiPickersUtilsProvider, } from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    display: 'flex'
  },
  formControlDate: {
    margin: '0px 16px 0 16px',
    display: 'flex'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  singleCard: {
    marginBottom: theme.spacing(2),
    boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
  },
  title: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: '10px'
  },
  deleteAction: {
    cursor: 'pointer',
    marginRight: '15px'
  },
  activitytracker: {
    'marginRight': '10px',
    'fontSize': '15px',
    'color': 'green'
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 300,
    },
  },
};

const names = [
  'HM',
  'SDMC',
  'CWF',
  'DOPI',
  'SVYM',
  'RBSK',
  'Education Department',
  'School Parliament'
];

let stakeholderArray = []


export default function SimpleSelect(props) {
  const classes = useStyles();
  const [activity, setActivity] = React.useState(props.actionData.activityId);
  const [responsibility, setResponsiblity] = React.useState(props.actionData.responsibility);
  const [selectedDate, setSelectedDate] = React.useState(new Date(props.actionData.deadline));
  const [selectedReviewDate, setSelectedReviewDate] = React.useState(new Date(props.actionData.reviewDate));
  const [contactNumber, setContactNumber] = React.useState(props.actionData.contactNumber);
  const [remarks, setRemarks] = React.useState(props.actionData.remarks);
  const [supportingNgoDonor, setSupportingNgoDonor] = React.useState(props.actionData.supportingNgoDonor);
  const [state, setState] = React.useState({
    checked1: props.actionData.money && props.actionData.money,
    checked2: props.actionData.kind && props.actionData.kind,
    checked3: props.actionData.human_resource && props.actionData.human_resource,
  });
  const [status, setStatus] = React.useState(props.actionData.status);
  // let updated_checkbox_state = props.actionData.resources;

  const handleChange = (index, field_name, name, checkbox_index) => event => {
    setState({ ...state, [name]: event.target.checked });
    //updated_checkbox_state[checkbox_index] = event.target.checked;
    props.onUpdateData(index, field_name, event.target.checked);
  };

  const { checked1, checked2, checked3 } = state;

  if (props.actionData.stakeholders.length) {
    stakeholderArray = props.actionData.stakeholders.toString().split(',')
  } else {
    stakeholderArray = [];
  }
  const [personName, setPersonName] = React.useState(stakeholderArray);
  const { formErrors } = props;


  const handleChangeActivity = (index, field_name) => event => {
    console.log(event);
    setActivity(event.target.value)
    props.onUpdateData(index, field_name, event.target.value);
  };


  const handleChangeStakeholder = (index, field_name) => event => {
    console.log(event);
    setPersonName(event.target.value);
    props.onUpdateData(index, field_name, event.target.value);
  };

  const handleChangeResponsibility = (index, field_name) => event => {
    console.log(event);
    setResponsiblity(event.target.value);
    props.onUpdateData(index, field_name, event.target.value);
  };

  const handleChangeContactNumber = (index, field_name) => event => {
    console.log(event);
    const re = /^[0-9\b]+$/;
    if (event.target.value === '' || re.test(event.target.value)) {
      setContactNumber(event.target.value);
      props.onUpdateData(index, field_name, event.target.value);
    }
  };

  const handleChangeRemarks = (index, field_name) => event => {
    console.log(event);
    setRemarks(event.target.value);
    props.onUpdateData(index, field_name, event.target.value);
  };

  const handleChangeSupportingNgoDonor = (index, field_name) => event => {
    console.log(event);
    setSupportingNgoDonor(event.target.value);
    props.onUpdateData(index, field_name, event.target.value);
  };

  const handleDateChange = (index, field_name) => date => {
    setSelectedDate(date);
    console.log("updated deadline date", date);
    let MyDate = new Date(date);
    let _updatedFormat = MyDate.getFullYear() + '-' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-' + ('0' + MyDate.getDate()).slice(-2);
    props.onUpdateData(index, field_name, _updatedFormat);
  };

  const handleReviewDateChange = (index, field_name) => date => {
      setSelectedReviewDate(date);
      console.log("updated review date", date);
      let MyDate = new Date(date);
      let _updatedFormat = MyDate.getFullYear() + '-' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-' + ('0' + MyDate.getDate()).slice(-2);
      props.onUpdateData(index, field_name, _updatedFormat);
  };

  const handleChangeStatus = (index, field_name) => event => {
    console.log(event);
    setStatus(event.target.value)
    props.onUpdateData(index, field_name, event.target.value);
  };

  const removeActivity = (index, event) => {
    props.onChildClick();
  }

  return (
    <div className={classes.singleCard}>
      <Grid container spacing={3}>
        <Grid item xs={3} sm={3} md={1} lg={1}>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            <span className={classes.activitytracker}>{"Activity"}</span>
          </Typography>
        </Grid>
        <Grid item xs={6} sm={6} md={10} lg={10}>
        </Grid>
        <Grid item xs={3} sm={3} md={1} lg={1}>
          <DeleteIcon onClick={(event) => removeActivity(event)} className={classes.deleteAction}></DeleteIcon>
        </Grid>
      </Grid>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel htmlFor="outlined-age-native-simple" id="Activity" error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Activity' + props.index).length > 0}>Activity Indicator</InputLabel>
            <Select
              labelId="Activity"
              id="Activity-Select"
              value={activity}
              label="Activity Indicator"
              onChange={handleChangeActivity(props.index, "activityId")}
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Activity' + props.index).length > 0}
              name={props.category + "Activity" + props.index}
            >
              {props.actions && props.actions.map((item) => {
                return <MenuItem key={item.activityId} value={item.activityId}>{item.activityDetail}</MenuItem>
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControl}>
            <InputLabel id="Stakeholder" error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Stakeholder' + props.index).length > 0}>Stakeholder</InputLabel>
            <Select
              labelId="Stakeholder"
              id="Stakeholder-checkbox"
              multiple
              value={personName}
              onChange={handleChangeStakeholder(props.index, 'stakeholders')}
              input={<Input />}
              renderValue={selected => selected.join(', ')}
              MenuProps={MenuProps}
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Stakeholder' + props.index).length > 0}
              name={props.category + "Stakeholder" + props.index}
            >
              {names.map(name => (
                <MenuItem key={name} value={name}>
                  <Checkbox checked={personName.indexOf(name) > -1} />
                  <ListItemText primary={name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl component="fieldset" className={classes.formControl}
            value={checked1 || checked2|| checked3}
            error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Resources' + props.index).length > 0}
            name={props.category + "Resources" + props.index}>
            <FormLabel component="legend">Resources</FormLabel>
            <FormGroup aria-label="position">
              <FormControlLabel
                control={<Checkbox color="primary"
                  checked={checked1}
                  onChange={handleChange(props.index, 'money', "checked1", 0)}
                  value="Money"
                  control={<Checkbox color="primary" />}
                  inputProps={{ 'aria-label': 'primary checkbox' }} />}
                label="Money"
                labelPlacement="end"
              />
              <FormControlLabel
                control={<Checkbox color="primary"
                  checked={checked2}
                  onChange={handleChange(props.index, 'kind', "checked2", 1)}
                  value="Kind"
                  control={<Checkbox color="primary" />}
                  inputProps={{ 'aria-label': 'primary checkbox' }} />}
                label="Kind"
                labelPlacement="end"
              />
              <FormControlLabel
                control={<Checkbox color="primary"
                  checked={checked3}
                  onChange={handleChange(props.index, 'human_resource', "checked3", 2)}
                  value="Human Resource"
                  control={<Checkbox color="primary" />}
                  inputProps={{ 'aria-label': 'primary checkbox' }} />}
                label="Human Resource"
                labelPlacement="end"
              />
            </FormGroup>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControl}>
            <TextField id="Responsibility" value={responsibility}
              onChange={handleChangeResponsibility(props.index, 'responsibility')} label="Monitored By"
              variant="outlined"
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Responsibility' + props.index).length > 0}
              name={props.category + "Responsibility" + props.index} />
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControl}>
            <TextField id="contact-number" onChange={handleChangeContactNumber(props.index, 'contactNumber')} label="Contact Number" value={contactNumber}
              variant="outlined"
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'contact-number' + props.index).length > 0}
              name={props.category + "contact-number" + props.index}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControlDate}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="MM/dd/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Deadline"
                value={selectedDate}
                error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Date' + props.index).length > 0}
                name={props.category + "Date" + props.index}
                onChange={handleDateChange(props.index, 'deadline', selectedDate)}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </Grid>
      </Grid>


      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControlDate}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Review Date"
                        value={selectedReviewDate}
                        error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'ReviewDate' + props.index).length > 0}
                        name={props.category + "ReviewDate" + props.index}
                        onChange={handleReviewDateChange(props.index, 'reviewDate', selectedReviewDate)}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
              </MuiPickersUtilsProvider>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControl}>
            <TextField id="outlined-basic" onChange={handleChangeSupportingNgoDonor(props.index, 'supportingNgoDonor')} label="Supporting NGO/Donor" value={supportingNgoDonor}
              variant="outlined"
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'SupportingNgo' + props.index).length > 0}
              name={props.category + "SupportingNgo" + props.index} />
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <FormControl className={classes.formControl}>
            <TextField id="outlined-basic" onChange={handleChangeRemarks(props.index, 'remarks')} label="Remarks" value={remarks}
              variant="outlined"
              error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Remarks' + props.index).length > 0}
              name={props.category + "Remarks" + props.index} />
          </FormControl>
        </Grid>
       </Grid>
       {props.actionPlanStatus === "APPROVED" ?
       <Grid container spacing={3}>
          <Grid item xs={12} sm={12} md={4} lg={4} display="none">
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel id="Status" htmlFor="outlined-age-native-simple" error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Status' + props.index).length > 0}>Status</InputLabel>
              <Select
                labelId="Status-label"
                id="Status-Select"
                value={status}
                label="Status"
                onChange={handleChangeStatus(props.index, "status")}
                error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === props.category + 'Status' + props.index).length > 0}
                name={props.category + "Status" + props.index}
              >
                <MenuItem value={"PENDING"}>PENDING</MenuItem>
                <MenuItem value={"IN PROGRESS"}>IN PROGRESS</MenuItem>
                <MenuItem value={"COMPLETED"}>COMPLETED</MenuItem>
              </Select>
            </FormControl>
          </Grid>
      </Grid> : null}
    </div>
  );
}
