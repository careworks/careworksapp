import React, { Component } from "react";
import {
  Button,
  CircularProgress,
  Snackbar,
  Grid,
  Typography,
} from "@material-ui/core";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {
  getActions,
  getCategories,
  submitActionFields,
  getActionPlansBySchoolId,
  approveOrRejectActionPlan,
} from "../../utils/APIUtils";
import { withRouter } from "react-router";
import SimpleSelect from "./Components/ActionPlanDetails/actionCardComponent";
import IconButton from "@material-ui/core/IconButton";
import Add from "@material-ui/icons/Add";
import { Alert } from "@material-ui/lab";
import DateFnsUtils from "@date-io/date-fns";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import FormControl from "@material-ui/core/FormControl";

class ActionPlan extends Component {
  constructor(props) {
    super(props);
    console.log("props inside the action plan component", props);
    let MyDate = new Date();
    this._updatedFormat =
      MyDate.getFullYear() +
      "-" +
      ("0" + (MyDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + MyDate.getDate()).slice(-2);

    this.state = {
      activities: [],
      schoolenvActions: [],
      classroomEnvActions: [],
      healthActions: [],
      stakeholderEnvActions: [],
      otherActivitiesActions: [],
      schoolEnv: [],
      classroomEnv: [],
      health: [],
      stakeholderEnv: [],
      otherActivities: [],
      actionStateArray: [],
      isLoading: true,
      categories: [],
    };
    this.expanded = true;
  }

  category = this.props.category;
  actions = [];

  componentWillMount() {}

  componentDidUpdate() {
    console.log("oncomponentupdate", this.state);
  }

  componentDidMount() {
    getCategories().then(response => {
      console.log("categories", response);
      this.setState({ categories: response });
    });
    if (this.props.match.params.na_Flag == "Y") {
      var category = this.props.category;
      this.getActionFieldData(category);
      if (this.props.match.params.school_id) {
        console.log("school id is ", this.props.match.params.school_id);
        this.getActionPlansBySchool(this.props.match.params.school_id);
      }
    }
  }

  getActionFieldData = () => {
    getActions().then(response => {
      console.log("activities", response);
      this.setState({ activities: response });
    });
  };

  getActionPlansBySchool = school_id => {
    getActionPlansBySchoolId(school_id).then(response => {
      console.log("action plan data by school id", response);
      if (response.length) {
        for (var i = 0; i < response.length; i++) {
          response[i].actionMasters.map(
            action => (action["unique_Id"] = this.uuidv4()),
          );
          console.log("after adding unique property ", response);
          let previousState = this.state.actionStateArray;
          let tempState = this.state.actionStateArray;
          tempState.push({
            financialYear: response[i].financialYear,
            actionplanId: response[i].actionplanId,
            status: response[i].status,
            actions: response[i].actionMasters,
          });
          this.setState({ actionStateArray: tempState });
          console.log("state array values", this.state.actionStateArray);
        }
      }
      this.setState({ isLoading: false });
    });
  };

  uuidv4() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c === "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  addAction = (categoryId, index) => {
    console.log("add button clicked");
    let previousState = this.state.actionStateArray;
    previousState[index].actions.push({
      unique_Id: this.uuidv4(),
      actionmasterId: null,
      activityId: "",
      categoryId: categoryId,
      stakeholders: "",
      money: false,
      kind: false,
      human_resource: false,
      responsibility: "",
      building_id: null,
      deadline: this._updatedFormat,
      remarks: null,
      reviewDate: this._updatedFormat,
      supportingNgoDonor: null,
    });
    this.setState({ actionStateArray: previousState });
  };

  handleDeleteAction = (parentIndex, index) => {
    console.log("delete action propagated to parent", index);
    let previousState = this.state.actionStateArray;
    console.log("before removing value ", previousState);
    previousState[parentIndex].actions.splice(index, 1);
    console.log("after removing value ", previousState);
    this.setState({ actionStateArray: previousState });
  };

  checkformError = index => {
    let tempFormErrors = [...this.state.formError];
    for (let i = 0; i < this.actionPlanForm.length; i++) {
      const elem = this.actionPlanForm[i];
      //console.log("the elements are", elem.getAttribute('name'));
      //console.log("contact-number" + i);
      //console.log("actionplan form length", this.actionPlanForm.length);
      //console.log("name of fields", elem.getAttribute('name'))

      if (
        elem.getAttribute("name") === "financialyear" &&
        !elem.getAttribute("value")
      ) {
        tempFormErrors.push(elem.getAttribute("name"));
      }

      for (
        let j = 0;
        j < this.state.actionStateArray[index].actions.length;
        j++
      ) {
        var categoryName = this.state.actionStateArray[index].actions[j]
          .categoryId;
        if (
          elem.getAttribute("name") === categoryName + "Activity" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "Stakeholder" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "Responsibility" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }
        if (elem.getAttribute("name") === categoryName + "Resources" + j) {
          console.log("cheers", elem.getAttribute("value"));
          if (
            elem.getAttribute("value") === false ||
            elem.getAttribute("value") === "false" ||
            elem.getAttribute("value") === null
          ) {
            tempFormErrors.push(elem.getAttribute("name"));
          }
        }
        if (
          elem.getAttribute("name") === categoryName + "contact-number" + j &&
          elem.getAttribute("value").length != 10
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "Date" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "ReviewDate" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "SupportingNgo" + j &&
          !elem.getAttribute("value").length > 0
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "Remarks" + j &&
          !elem.getAttribute("value").length > 0
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }

        if (
          elem.getAttribute("name") === categoryName + "Status" + j &&
          !elem.getAttribute("value")
        ) {
          tempFormErrors.push(elem.getAttribute("name"));
        }
      }

      /*for (let j = 0; j < this.state.classroomEnv.length; j++) {
          if (elem.getAttribute('name') === "classroomEnv" + 'Activity' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + 'Stakeholder' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + 'Responsibility' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "classroomEnv" + "Resources" + j){
            console.log("cheers" , elem.getAttribute('value'))
            if(elem.getAttribute('value') === false || elem.getAttribute('value') === "false" || elem.getAttribute('value') === null)  {
              tempFormErrors.push(elem.getAttribute('name'));
            }
           }
          if (elem.getAttribute('name') === "classroomEnv" + "contact-number" + j && elem.getAttribute('value').length != 10) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + "Date" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + "ReviewDate" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + "SupportingNgo" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + "Remarks" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "classroomEnv" + "Status" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
        }


        for (let j = 0; j < this.state.health.length; j++) {
          if (elem.getAttribute('name') === "health" + 'Activity' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "health" + 'Stakeholder' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "health" + "Resources" + j){
            console.log("cheers" , elem.getAttribute('value'))
            if(elem.getAttribute('value') === false || elem.getAttribute('value') === "false" || elem.getAttribute('value') === null)  {
              tempFormErrors.push(elem.getAttribute('name'));
            }
          }
          if (elem.getAttribute('name') === "health" + 'Responsibility' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "health" + "contact-number" + j && elem.getAttribute('value').length != 10) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "health" + "Date" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "health" + "ReviewDate" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "health" + "SupportingNgo" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "health" + "Remarks" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "health" + "Status" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
        }

        for (let j = 0; j < this.state.stakeholderEnvActions.length; j++) {
          if (elem.getAttribute('name') === "stakeholderEnvActions" + 'Activity' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "stakeholderEnvActions" + 'Stakeholder' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + "Resources" + j){
            console.log("cheers" , elem.getAttribute('value'))
            if(elem.getAttribute('value') === false || elem.getAttribute('value') === "false" || elem.getAttribute('value') === null)  {
              tempFormErrors.push(elem.getAttribute('name'));
            }
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + 'Responsibility' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + "contact-number" + j && elem.getAttribute('value').length != 10) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "stakeholderEnvActions" + "Date" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + "ReviewDate" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + "SupportingNgo" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "stakeholderEnvActions" + "Remarks" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "stakeholderEnvActions" + "Status" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
        }

        for (let j = 0; j < this.state.otherActivitiesActions.length; j++) {
          if (elem.getAttribute('name') === "otherActivitiesActions" + 'Activity' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "otherActivitiesActions" + 'Stakeholder' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + "Resources" + j){
            console.log("cheers" , elem.getAttribute('value'))
            if(elem.getAttribute('value') === false || elem.getAttribute('value') === "false" || elem.getAttribute('value') === null)  {
              tempFormErrors.push(elem.getAttribute('name'));
            }
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + 'Responsibility' + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + "contact-number" + j && elem.getAttribute('value').length != 10) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "otherActivitiesActions" + "Date" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + "ReviewDate" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + "SupportingNgo" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
          if (elem.getAttribute('name') === "otherActivitiesActions" + "Remarks" + j && !elem.getAttribute('value').length > 0) {
            tempFormErrors.push(elem.getAttribute('name'));
          }

          if (elem.getAttribute('name') === "otherActivitiesActions" + "Status" + j && !elem.getAttribute('value')) {
            tempFormErrors.push(elem.getAttribute('name'));
          }
        }*/
    }
    console.log("retur value", tempFormErrors);
    return tempFormErrors;
  };

  validateForm = (index, status) => {
    console.log("form validation called");
    this.setState(
      {
        formError: [],
      },
      () => {
        this.setState(
          {
            isLoading: true,
            formError: this.checkformError(index),
          },
          () => {
            if (this.state.formError.length > 0) {
              this.setState(
                {
                  dialog: true,
                  //saveStatus: 'These are invalid : ' + this.state.formError.map(item => lodash.startCase(item)),
                  saveStatus:
                    "Fields highlighted in red are invalid, please enter a valid value to proceed",
                  severe: "error",
                  isLoading: false,
                },
                console.log("updated form error array", this.state.formError),
              );
            }
            if (this.state.formError.length === 0) {
              this.triggerSave(index, status);
            }
          },
        );
      },
    );

    //this.triggerSave();
  };

  triggerSave = (index, status) => {
    let payload = {
      schoolId: this.props.match.params.school_id,
      financialYear: this.state.actionStateArray[index].financialYear
        ? this.state.actionStateArray[index].financialYear
        : new Date().getFullYear(),
      actionplanId: this.state.actionStateArray[index].actionplanId
        ? this.state.actionStateArray[index].actionplanId
        : null,
      status: status ? status : null,
      actionMasters: this.state.actionStateArray[index].actions,
    };

    console.log("final payload", payload);
    payload.actionMasters.map(action => delete action["unique_Id"]);
    submitActionFields(payload).then(
      response => {
        console.log(response);
        this.props.history.push("/app/schools");
      },
      error => {
        console.log(error);
      },
    );
  };

  triggerApproveOrReject = (index, status) => {
    let payload = {
      actionplanId: this.state.actionStateArray[index].actionplanId
        ? this.state.actionStateArray[index].actionplanId
        : null,
      status: status ? status : null,
    };
    console.log("payload for approve or reject", payload);
    approveOrRejectActionPlan(payload).then(
      response => {
        console.log(response);
        this.props.history.push("/app/schools");
      },
      error => {
        console.log(error);
      },
    );
  };

  updateData = (index, field_name, value, parentIndex) => {
    this.state.actionStateArray[parentIndex].actions[index][field_name] =
      field_name === "stakeholders" ? value.toString() : value;
  };

  triggerCancel = () => {
    this.props.history.push("/app/schools");
  };

  handleChangeFinancialYear = (index, val) => event => {
    console.log(new Date(event).getFullYear().toString());
    let previousState = this.state.actionStateArray;
    previousState[index].financialYear = new Date(event)
      .getFullYear()
      .toString();
    this.setState({ actionStateArray: previousState });
  };

  addNewActionPlan = index => {
    console.log("add action plan clicked");
    let previousState = this.state.actionStateArray;
    previousState.push({
      schoolId: this.props.match.params.school_id,
      financialYear: new Date().getFullYear().toString(),
      actionplanId: null,
      status: "IN PROGRESS",
      actions: [],
    });
    this.setState({ actionStateArray: previousState });
  };

  render() {
    const mystyle = {
      button: {
        marginRight: "10px",
      },
    };

    const renderActionPlan = this.props.match.params.na_Flag == "Y";
    return renderActionPlan ? (
      <form ref={form => (this.actionPlanForm = form)}>
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={this.state.dialog}
          autoHideDuration={10000}
          onClose={this.handleDialogClose}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.handleDialogClose}
            severity={this.state.severe}
          >
            {this.state.saveStatus}
          </Alert>
        </Snackbar>
        <React.Fragment>
          <Grid container spacing={3}>
            <Grid item xs={6} sm={6} md={6} lg={6}>
              <Typography variant="h3" color="textSecondary" gutterBottom>
                {this.props.match.params.school_Name} - Action Plan(s)
              </Typography>
            </Grid>

            <Grid
              item
              xs={6}
              sm={6}
              md={6}
              lg={6}
              style={{
                marginBottom: "10px",
                justifyContent: "flex-end",
                display: "flex",
              }}
            >
              <Button
                variant="contained"
                style={mystyle.button}
                color="primary"
                onClick={e => {
                  if (
                    window.confirm(
                      "Are you sure you want to add new action plan?",
                    )
                  )
                    this.addNewActionPlan(this.state.actionStateArray.length);
                }}
              >
                New Action Plan
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={e => {
                  if (window.confirm("Are you sure you want to Cancel?"))
                    this.triggerCancel();
                }}
              >
                Cancel
              </Button>
            </Grid>
          </Grid>
          {this.state.isLoading ? (
            <CircularProgress
              size="5rem"
              style={{
                marginLeft: "40%",
                marginBottom: "20%",
                marginTop: "10%",
              }}
            />
          ) : (
            this.state.actionStateArray.length > 0 &&
            this.state.actionStateArray.map((item, parentIndex) => {
              return (
                <ExpansionPanel defaultExpanded={this.expanded}>
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Grid container spacing={1}>
                      <Grid item xs={6} sm={6} md={6} lg={6}>
                        <Typography variant="h3">
                          {
                            this.state.actionStateArray[parentIndex]
                              .financialYear
                          }{" "}
                          - ({this.state.actionStateArray[parentIndex].status})
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        md={6}
                        lg={6}
                        style={{ justifyContent: "flex-end", display: "flex" }}
                      >
                        {sessionStorage.getItem("role").includes("ADMIN") &&
                        this.state.actionStateArray[parentIndex].status ==
                          "SUBMITTED" ? (
                          <Button
                            variant="contained"
                            style={mystyle.button}
                            color="primary"
                            onClick={e => {
                              if (
                                window.confirm(
                                  "Do you want to proceed with Approve?",
                                )
                              )
                                this.triggerApproveOrReject(
                                  parentIndex,
                                  "APPROVED",
                                );
                            }}
                          >
                            Approve
                          </Button>
                        ) : null}
                        {sessionStorage.getItem("role").includes("ADMIN") &&
                        this.state.actionStateArray[parentIndex].status ==
                          "SUBMITTED" ? (
                          <Button
                            variant="contained"
                            style={mystyle.button}
                            color="primary"
                            onClick={e => {
                              if (
                                window.confirm(
                                  "Do you want to proceed with Reject?",
                                )
                              )
                                this.triggerApproveOrReject(
                                  parentIndex,
                                  "REJECTED",
                                );
                            }}
                          >
                            Reject
                          </Button>
                        ) : null}
                        {this.state.actionStateArray[parentIndex].status ==
                          "IN PROGRESS" ||
                        this.state.actionStateArray[parentIndex].status ==
                          "REJECTED" ||
                        (sessionStorage.getItem("role").includes("ADMIN") &&
                          this.state.actionStateArray[parentIndex].status ==
                            "APPROVED") ? (
                          <Button
                            variant="contained"
                            style={mystyle.button}
                            color="primary"
                            onClick={e => {
                              let currentStatus =
                                this.state.actionStateArray[parentIndex]
                                  .status == "APPROVED"
                                  ? "APPROVED"
                                  : "IN PROGRESS";
                              this.validateForm(parentIndex, currentStatus);
                            }}
                          >
                            Save
                          </Button>
                        ) : null}
                        {this.state.actionStateArray[parentIndex].status ==
                          "IN PROGRESS" ||
                        this.state.actionStateArray[parentIndex].status ==
                          "REJECTED" ? (
                          <Button
                            variant="contained"
                            style={mystyle.button}
                            color="primary"
                            onClick={e => {
                              if (
                                window.confirm(
                                  "Do you want to proceed with Submit?",
                                )
                              )
                                this.validateForm(parentIndex, "SUBMITTED");
                            }}
                          >
                            Submit
                          </Button>
                        ) : null}
                      </Grid>
                    </Grid>
                  </ExpansionPanelSummary>
                  <div
                  justify="space-around"
                  style={{
                    paddingBottom: "10px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                  }}
                  >
                  <Grid container spacing={1}>
                    <Grid item xs={2} sm={2} md={2} lg={2}>
                      <FormControl>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            margin="normal"
                            openTo="year"
                            views={["year"]}
                            id="date-picker-inline"
                            label="Financial Year"
                            value={
                              this.state.actionStateArray[parentIndex]
                                .financialYear
                                ? this.state.actionStateArray[parentIndex]
                                    .financialYear
                                : new Date().getFullYear().toString()
                            }
                            error={
                              this.state.formError &&
                              this.state.formError.length > 0 &&
                              this.state.formError.filter(
                                i => i === "financialyear",
                              ).length > 0
                            }
                            name="financialyear"
                            onChange={this.handleChangeFinancialYear(
                              parentIndex,
                            )}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                          />
                        </MuiPickersUtilsProvider>
                      </FormControl>
                    </Grid>
                  </Grid>
                  </div>
                  <div
                    justify="space-around"
                    style={{
                      paddingBottom: "10px",
                      paddingLeft: "10px",
                      paddingRight: "10px",
                    }}
                  >
                    {this.state.categories.length > 0 &&
                      this.state.categories.map((item, index) => {
                        return (
                          <ExpansionPanel defaultExpanded={index === 0}>
                            <ExpansionPanelSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                            >
                              <Typography variant="h6">
                                {item.categoryDetails}
                              </Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                              <Grid container spacing={3}>
                                <Grid item xs={12}>
                                  {this.state.activities.length > 0 &&
                                    this.state.actionStateArray[
                                      parentIndex
                                    ].actions.map((action, actionIndex) => {
                                      if (
                                        action.categoryId === item.categoryId
                                      ) {
                                        return (
                                          <SimpleSelect
                                            key={item.unique_Id}
                                            actionData={action}
                                            onChildClick={() =>
                                              this.handleDeleteAction(
                                                parentIndex,
                                                actionIndex,
                                              )
                                            }
                                            index={actionIndex}
                                            actions={this.state.activities.filter(
                                              activity =>
                                                activity.categoryId ===
                                                item.categoryId,
                                            )}
                                            formErrors={this.state.formError}
                                            actionPlanStatus={
                                              this.state.actionStateArray[
                                                parentIndex
                                              ].status
                                            }
                                            category={item.categoryId} //{item.categoryDetails.replace(/\s+/g, '')}
                                            onUpdateData={(
                                              _index,
                                              field_name,
                                              value,
                                            ) =>
                                              this.updateData(
                                                _index,
                                                field_name,
                                                value,
                                                parentIndex,
                                              )
                                            }
                                          />
                                        );
                                      }
                                    })}
                                </Grid>
                                <Grid item xs={6}>
                                  {}
                                  <IconButton
                                    onClick={() =>
                                      this.addAction(
                                        item.categoryId,
                                        parentIndex,
                                      )
                                    }
                                  >
                                    <Add />
                                  </IconButton>
                                </Grid>
                              </Grid>
                            </ExpansionPanelDetails>
                          </ExpansionPanel>
                        );
                      })}
                  </div>
                </ExpansionPanel>
              );
            })
          )}
        </React.Fragment>
      </form>
    ) : (
      <div style={{ width: "100%" }}>
        <Alert variant="outlined" severity="warning">
          <span style={{ fontSize: "large" }}>
            {" "}
            Need Assesment for School -{" "}
            <b> {this.props.match.params.school_Name} </b> is not approved yet!{" "}
          </span>
        </Alert>
        <br />
        <Button
          variant="contained"
          color="secondary"
          onClick={e => {
            this.triggerCancel();
          }}
        >
          Back
        </Button>
      </div>
    );
  }
}

export default withRouter(ActionPlan);
