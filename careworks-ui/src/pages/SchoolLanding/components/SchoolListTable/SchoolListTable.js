import React from 'react';
import {withRouter} from "react-router";
import MUIDataTable from "mui-datatables";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Assignment from "@material-ui/icons/Assignment";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import NoteAdd from "@material-ui/icons/NoteAdd";
import lodash from "lodash";
import PlaylistAddTwoToneIcon from '@material-ui/icons/PlaylistAddTwoTone';
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {MuiThemeProvider} from "@material-ui/core";

const SchoolListTable = props => {
  const {schoolList, history} = props;

  function customToolbar(selectedRows, displayData, setSelectedRows) {
    return (
      <>
        <Tooltip title={"Add School"} onClick={event =>
          history.push('/app/school')
        }>
          <IconButton>
            <Add/>
          </IconButton>
        </Tooltip>
      </>
    )
  }

  function onOpenSchool(rowData, rowMetaData) {
    history.push('/app/school/' + rowData[0])
  }

  function onOpenActionPlan(rowData) {
    history.push('/app/actionplan/' + rowData[0] + '/' + rowData[2] + '/' + rowData[13])
  }

  function onQuotations(rowData) {
    history.push('/app/quotations/' + rowData[0] + '/' + rowData[2])
  }

  function onOpenNA(rowData) {
    const schoolData = {
      school_id: rowData[0],
      na_Flag: rowData[13],
      school_Name: rowData[2],
      schoolStatus : rowData[11]
    };
    history.push('/app/assessment/' + schoolData.school_id + '/' + schoolData.na_Flag + '/' + schoolData.school_Name +
    '/' + schoolData.schoolStatus
    )
  }

  const getTableTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableHeadCell: {
          root: {
            textAlign: 'justify',
            fontWeight: 'bold'
          }
        },
        MUIDataTableBodyCell: {
          root: {
            textAlign: 'justify'
          }
        }
      }
    });

  return (
    <>
      <MuiThemeProvider theme={getTableTheme()}>
      <MUIDataTable
        data={schoolList.map(school =>
          [school.school_id,
            school.diseCode,
            school.school_Name,
            school.yearOfEstablishment,
            school.na_Action_Date,
            school.onboardingyear,
            school.category, school.total, school.cluster, school.block, school.city,
            lodash.startCase(school.status ? school.status.toLowerCase() : "Created"),
            school.updated_at, lodash.startCase(school.na_Flag),lodash.startCase(school.ap_status),lodash.startCase(school.quot_status)])}
        columns={[
          {
            name: "Sl.No.",
            options: {
              filter: false
            }
          },
          {
            name: "Dise Code",
            options: {
              filter: false
            }
          }
          , {
            name: "Name",
            options: {
              filter: false
            }
          }
          , "Year of Establishment","Assessment Date","On Boarding Year","Category",
          {
            name: "Total Student Strength",
            options: {
              filter: false
            }
          }, "Cluster", "Block", "City", "Profile Status"
          ,
          {
            name: "Updated At",
            options: {
              display: 'false',
              filter: false
            }
          },
          {
            name: "Assessment Status",
            options: {
              display: 'false'
            }
          },
          {
            name: "Action Plan Status",
            options: {
              display: 'false'
            }
          },
          {
            name: "Quotation Status",
            options: {
              display: 'false'
            }
          },
          {
            name: "Actions",
            options: {
              filter: false,
              sort: false,
              empty: true,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div style={{maxHeight: 45, width: 220, maxLines: 1}}>
                    <Tooltip title={"Open School Profile"} onClick={event => {
                      event.preventDefault();
                      onOpenSchool(tableMeta.rowData)
                    }}>
                      <IconButton>
                        <Edit/>
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={"Open Need Assessment"} onClick={event => {
                      event.preventDefault();
                      onOpenNA(tableMeta.rowData)
                    }}>
                      <IconButton>
                        <Assignment/>
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={"Open Action Plan"} onClick={event => {
                      event.preventDefault();
                      onOpenActionPlan(tableMeta.rowData)
                    }}>
                      <IconButton>
                        <PlaylistAddTwoToneIcon/>
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={"Open Create Quotations"} onClick={event => {
                      event.preventDefault();
                      onQuotations(tableMeta.rowData)
                    }}>
                      <IconButton>
                        <NoteAdd/>
                      </IconButton>
                    </Tooltip>
                  </div>
                );
              }
            }
          }
        ]}
        options={{
          selectableRows: false,
          responsive: "scroll",
          filterType: "multiselect",
          customToolbar: customToolbar,
          setRowProps: (row) => {
            return {
              style: {
                justifyContent: 'center',
                alignContent: 'center'
              }
            };
          },
          //onRowClick: onOpenSchool
        }}
        title={null}/>
      </MuiThemeProvider>
    </>
  );
};

export default withRouter(SchoolListTable);
