import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router";
import {getAllSchoolStatus} from '../../../../utils/APIUtils';
import Card from "@material-ui/core/Card";
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import lodash from 'lodash';
import Typography from "@material-ui/core/Typography";

const SchoolStatusCard = props => {
  let [schoolStatus, setSchoolStatus] = useState([]);
  let [error, setError] = useState(null);

  const loadAllSchoolStatus = () => {
    getAllSchoolStatus().then(response => {
      if (response)
        setSchoolStatus(response);
    }).catch(err => {
      setError(err);
    });
  }

  useEffect(() => {
    loadAllSchoolStatus();
  }, []);


  const colorMap = {
    'ON BOARDED': '#33691E',
    'SUBMITTED': '#ff6f00',
    'IN PROGRESS': '#FFB300',
    'INPROGRESS': '#FFB300',
    'IN_PROGRESS': '#FFB300',
    'REJECTED': '#B71C1C',
    'APPROVED': '#388E3C',
    'CREATED' : '#ff4081',
    'PENDING': '#FF3D00'
  }

  return (
    <>
      <Typography variant="h3">{error ? 'ERROR ' + error.message + ' Please contact tech team' : null}</Typography>
        <Grid container spacing={2} direction="row" justify="center">
          {Object.keys(schoolStatus).map((type) =>
            (
              <Grid item>
                <Card raised style={{background: '#A5D6A7',marginTop: '0px'}}>
                  <CardHeader style={{"textAlign": "center", height: '2vh',marginTop: '0px' }} titleTypographyProps={{variant:'h5' }} title={type}/>
                  <CardContent>
                    <Grid container spacing={2} direction="row" justify="center">
                      {schoolStatus[type].map((schoolStatus) => (
                        <Grid item>
                          <Card style={{background: colorMap[schoolStatus.status], color: "#FFF", marginTop: '0px'}}>
                            <CardHeader style={{"textAlign": "center"}}
                                        titleTypographyProps={{variant: 'subtitle1'}}
                                        title={schoolStatus.count}
                                        subheaderTypographyProps={{variant: 'subtitle1', color: "#FFF"}}
                                        subheader={schoolStatus.status ? lodash.startCase(schoolStatus.status.toLowerCase()) : null}
                            />
                          </Card>
                        </Grid>
                      ))}
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            )
          )}
        </Grid>
    </>
  );
};

export default withRouter(SchoolStatusCard);
