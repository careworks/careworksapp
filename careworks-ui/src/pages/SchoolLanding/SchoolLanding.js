import React, {useEffect, useState} from 'react';
import {getAllProfiles} from '../../utils/APIUtils';
import {CircularProgress, Grid} from "@material-ui/core";
import {SchoolListTable,SchoolStatusCard} from "./components";
import {Button, Typography} from "../../components/Wrappers";

export default function SchoolLanding(props) {
  let [schoolList, setSchoolList] = useState([]);
  let [isLoading, setIsLoading] = useState(true);
  let [error, setError] = useState(null);

  useEffect(() => {
    if (isLoading)
      loadAllSchools();
  },[]);

  function loadAllSchools() {
    getAllProfiles().then(response => {
      if (response)
        setSchoolList(response);
      setIsLoading(false);
    }).catch(err => {
      setError(err);
      setIsLoading(false)
    });
  }

  return (
    <>
      <Typography variant="h3">{error ? 'ERROR ' + error.message + ' Please contact tech team' : null}</Typography>
      <Grid
        container
        spacing={4}
      >
        <Grid item xs={12}><Typography align="center" variant="h1">Schools Dashboard</Typography></Grid>
        <Grid item xs={12}>
          <SchoolStatusCard schoolList={schoolList} />
        </Grid>
        <Grid
          xs={12}
          item
          align={'right'}
        >
          <Button color="primary" variant="contained" onClick={() => {
            props.history.push('/app/school')
          }}>
            New School
          </Button>
        </Grid>
        <Grid
          item
          xs={12}
        >
          {isLoading ?
            <CircularProgress size="5rem" style={{marginLeft: '40%', marginBottom: '20%', marginTop: '10%'}}/> :
            <SchoolListTable schoolList={schoolList}/>}
        </Grid>
      </Grid>
    </>
  );
}
