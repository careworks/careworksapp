import React, {Fragment} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Input from '@material-ui/core/Input';
import DateFnsUtils from '@date-io/date-fns';
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent';
import lodash from 'lodash';

const useStyles = makeStyles(theme => ({
  formControl: {
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const SchoolBasicDetails = props => {
  const {schoolData, handleChange, formErrors} = props;
  const classes = useStyles();

  return (
    <Fragment>
      <Grid container spacing={3} align={'justify'}>
        <Grid item xs={12} align={'justify'}>
          <Typography variant={"h6"}>{schoolData.updatedAt && schoolData.status ? " Status : " + lodash.startCase(schoolData.status.toLowerCase()) + " and Last Updated : " + schoolData.updatedAt: null}</Typography>
        </Grid>
        <Grid item xs={12}>
          <Card>
            <CardContent>
              <Typography className={classes.title} color="textSecondary" gutterBottom>
                Basic School Data
              </Typography>
              <Grid container spacing={3}>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="schoolName"
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'schoolName').length > 0}
                             value={schoolData.schoolName ? schoolData.schoolName : ''} name="schoolName"
                             label="School Name"
                             onChange={(evt) => handleChange(evt)} fullWidth/>
                </Grid>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="cluster" onChange={(evt) => handleChange(evt)}
                             value={schoolData.cluster ? schoolData.cluster : ''} name="cluster" label="School Cluster"
                             fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'cluster').length > 0}/>
                </Grid>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="block" onChange={(evt) => handleChange(evt)}
                             value={schoolData.block ? schoolData.block : ''}
                             name="block" label="School Block" fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'block').length > 0}/>
                </Grid>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="diseCode" onChange={(evt) => handleChange(evt)}
                             value={schoolData.diseCode ? schoolData.diseCode : ''}
                             name="diseCode" label="Dise Code" fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'diseCode').length > 0}/>
                </Grid>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="emailId" onChange={(evt) => handleChange(evt)}
                             value={schoolData.emailId ? schoolData.emailId : ''}
                             name="emailId" label="Email ID" fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'emailId').length > 0}/>
                </Grid>
                <Grid item xs={4}>
                  <TextField required autoComplete="nope" id="studentCount" onChange={(evt) => handleChange(evt)}
                             type="number"
                             value={schoolData.studentCount ? schoolData.studentCount : ''}
                             name="studentCount" label="Student Count" fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'studentCount').length > 0}/>
                </Grid>
                <Grid item xs={4}>
                  <TextField id="otherInfo" onChange={(evt) => handleChange(evt)}
                             value={schoolData.otherInfo ? schoolData.otherInfo : ''} name="otherInfo"
                             label="Other Info" fullWidth/>
                </Grid>
              </Grid>
            </CardContent>
          </Card></Grid>
        <Grid item xs={12}>
          <Card><CardContent>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              School Address
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="addressLine1"
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'addressLine1').length > 0}
                           onChange={(evt) => handleChange(evt)}
                           name="addressLine1" value={schoolData.addressLine1 ? schoolData.addressLine1 : ''}
                           label="Address Line 1" fullWidth/>
              </Grid>
              <Grid item xs={4}>
                <TextField id="addressLine2" onChange={(evt) => handleChange(evt)} name="addressLine2"
                           value={schoolData.addressLine2 ? schoolData.addressLine2 : ''}
                           label="Address Line 2" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'addressLine2').length > 0}/>
              </Grid>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="streetAddress" onChange={(evt) => handleChange(evt)}
                           value={schoolData.streetAddress ? schoolData.streetAddress : ''}
                           name="streetAddress" label="Street Address" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'streetAddress').length > 0}/>
              </Grid>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="city" onChange={(evt) => handleChange(evt)}
                           value={schoolData.city ? schoolData.city : ''}
                           name="city" label="City" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'city').length > 0}/>
              </Grid>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="state" onChange={(evt) => handleChange(evt)}
                           value={schoolData.state ? schoolData.state : ''}
                           name="state" label="State" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'state').length > 0}/>
              </Grid>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="postalCode" onChange={(evt) => handleChange(evt)}
                           type="number"
                           value={schoolData.postalCode ? schoolData.postalCode : ''}
                           name="postalCode" label="Postal Code" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'postalCode').length > 0}/>
              </Grid>
              <Grid item xs={4}>
                <TextField required autoComplete="nope" id="country" onChange={(evt) => handleChange(evt)}
                           value={schoolData.country ? schoolData.country : (schoolData.country = 'India')}
                           name="country" label="Country" fullWidth
                           error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'country').length > 0}/>
              </Grid>
            </Grid></CardContent>
          </Card> </Grid>
        <Grid item xs={12}>
          <Card><CardContent>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              Other Details
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker required autoComplete="nope" format="yyyy"
                                      id="date-picker-dialog"
                                      maxDate={new Date()}
                                      openTo="year"
                                      views={["year"]}
                                      label="Established Year"
                                      value={schoolData.establishedDate ? schoolData.establishedDate : new Date()}
                                      onChange={(evt) => handleChange(evt)} KeyboardButtonProps={{
                    'aria-label': 'Change Established Year'
                  }}/>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={4}>
                <FormControl required autoComplete="nope" className={classes.formControl} fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'mediums').length > 0}>
                  <InputLabel id="mediumSelect">Medium</InputLabel>
                  <Select labelId="mediumSelect" id="mediums" name='mediums' multiple
                          value={schoolData.mediums ? schoolData.mediums : []} onChange={(evt) => handleChange(evt)}
                          input={<Input/>}
                          renderValue={selected => selected.join(', ')} MenuProps={MenuProps}>
                    {schoolData.mediumsList && schoolData.mediumsList.map(item => (
                      <MenuItem key={item.label} value={item.label}>
                        <Checkbox checked={schoolData.mediums ? schoolData.mediums.indexOf(item.label) > -1 : false}/>
                        <ListItemText primary={item.label}/>
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl required autoComplete="nope" className={classes.formControl} fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'location').length > 0}>
                  <InputLabel id="locationSelect">Location</InputLabel>
                  <Select labelId="locationSelect" id="location" name="location"
                          value={schoolData.location ? schoolData.location : ''} onChange={(evt) => handleChange(evt)}>
                    {schoolData.locationList && schoolData.locationList.map(loc => (
                      <MenuItem key={loc.label} value={loc.label}>{loc.label}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl required autoComplete="nope" className={classes.formControl} fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'category').length > 0}>
                  <InputLabel id="categorySelect">Category</InputLabel>
                  <Select labelId="categorySelect" id="category" name="category"
                          value={schoolData.category ? schoolData.category : ''} onChange={(evt) => handleChange(evt)}>
                    {schoolData.categoryList && schoolData.categoryList.map(cat => (
                      <MenuItem key={cat.label} value={cat.label}>{cat.label}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl required autoComplete="nope" className={classes.formControl} fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'type').length > 0}>
                  <InputLabel id="typeSelect">Type</InputLabel>
                  <Select labelId="typeSelect" id="type" name="type" value={schoolData.type ? schoolData.type : ''}
                          onChange={(evt) => handleChange(evt)}>
                    {schoolData.typeList && schoolData.typeList.map(type => (
                      <MenuItem key={type.label} value={type.label}>{type.label}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl required autoComplete="nope" className={classes.formControl} fullWidth
                             error={formErrors && formErrors.length > 0 && formErrors.filter(i => i === 'type').length > 0}>
                  <InputLabel id="typeSelect">Managed By</InputLabel>
                  <Select labelId="typeSelect" id="managedBy" name="managedBy"
                          value={schoolData.managedBy ? schoolData.managedBy : ''}
                          onChange={(evt) => handleChange(evt)}>
                    {schoolData.managedByList && schoolData.managedByList.map(type => (
                      <MenuItem key={type.label} value={type.label}>{type.label}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              {schoolData.managedBy === 'Other' ? <Grid item xs={4}>
                <TextField required autoComplete="nope" id="otherManaged" onChange={(evt) => handleChange(evt)}
                           value={schoolData.otherManaged ? schoolData.otherManaged : ''} name="otherManaged"
                           label="Managed By" fullWidth/>
              </Grid> : <></>}
            </Grid></CardContent>
          </Card> </Grid>
      </Grid>
    </Fragment>
  )
}

export default SchoolBasicDetails;
