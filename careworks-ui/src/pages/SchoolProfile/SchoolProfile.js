import React from 'react';
import {CardActions, CircularProgress, Snackbar, Tab, Tabs, Typography} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import {CREATE_COMPLETED, CREATED} from '../../common/constants'
import {getFormattedDate, trimErrorMessage} from '../../utils/Utils'

import {
  deleteEduDept,
  deleteSchoolClass,
  deleteSchoolClub,
  deleteStakeHolder,
  deleteTeachersDetails,
  getClassDetailsBySchool,
  getEducationDeptDetailsBySchool,
  getSchoolClubDetailsBySchool,
  getSchoolDetailsForId,
  getStakeholderDetailsBySchool,
  getTeacherDetailsBySchool,
  saveEduDept,
  saveSchoolClass,
  saveSchoolClub,
  saveSchoolV1,
  saveStakeHolder,
  saveTeachersDetails,
  submitSchool
} from '../../utils/APIUtils';
import PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import lodash from "lodash";
import SchoolBasicDetails from './components/SchoolBasicDetails'
import DataTable from "../AdminPanel/components/DataTable";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import LinearProgress from "@material-ui/core/LinearProgress";
import withStyles from "@material-ui/core/styles/withStyles";
import Map from "./components/Map";
import CardHeader from "@material-ui/core/CardHeader";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const StyledTabs = withStyles({
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    '& > div': {
      maxWidth: 40,
      width: '100%',
      backgroundColor: '#635ee7',
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{children: <div/>}}/>);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    marginRight: theme.spacing(1),
    '&:focus': {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

class SchoolProfile extends React.Component {

  state = {
    activeTabId: 0,
    dialog: false,
    isFormLoaded: false,
    schoolId: this.props.match.params && this.props.match.params.schoolId ? this.props.match.params.schoolId : 0,
    result: '',
    schools: [],
    schoolData: {},
    selectFieldsList: [],
    severe: "success",
    searchQuery: '',
    schoolActive: PropTypes.any,
    formEnable: false,
    form: null,
    saveStatus: '',
    formResponse: PropTypes.any,
    isSubmit: false,
    schoolStatus: CREATED,
    schoolName: 'Create New School',
    isFormValid: true,
    isSaveButtonClicked: true,
    isLoading: false,
    formError: [],
    nextTabId: 0,
  };

  formatMangedBy(schoolData) {
    if (schoolData.managedBy && !schoolData.managedByList.map(item => item.label).includes(schoolData.managedBy)) {
      schoolData['otherManaged'] = schoolData.managedBy;
      schoolData.managedBy = 'Other';
    }
    if (!schoolData.managedByList.map(item => item.label).includes('Other')) {
      schoolData.managedByList.push({
        id: schoolData.managedByList.length,
        label: 'Other',
        value: null
      });
    }
    return schoolData;
  }

  componentDidMount() {
    getSchoolDetailsForId(this.state.schoolId).then(response => {
      let schoolData = {...response};
      schoolData = this.formatMangedBy(schoolData);
      this.setState({schoolData, isFormLoaded: true, schoolName: schoolData.schoolName})
    });
  }

  handleDialogClose = () => {
    this.setState(
      {
        dialog: false
      }
    )
  };

  savePrevTabData(prevTabId) {
    this.setState({isLoading: true})
    switch (prevTabId) {
      case 0:
        this.validateAndSave();
        break;
      case 1:
        this.refs.educationDept.getRowData();
        break;
      case 2:
        this.refs.stakeHolders.getRowData();
        break;
      case 3:
        this.refs.classDetails.getRowData();
        break;
      case 4:
        this.refs.teachersDetails.getRowData();
        break;
      case 5:
        this.refs.schoolClub.getRowData();
        break;
      default:
        this.saveSchoolDataV1()
        return;
    }
  }

  validateAndSave = () => {
    this.setState({
      isLoading: true,
      formError: this.checkBasicFormErrors()
    }, () => this.saveSchoolDataV1());
  }

  checkBasicFormErrors = () => {
    let tempFormErrors = [...this.state.formError]
    if (!this.spForm) {
      return ['name']
    }
    console.log("validating form")
    for (let i = 0; i < this.spForm.length; i++) {
      const elem = this.spForm[i];
      if (elem.getAttribute('name') === 'emailId' && elem.getAttribute('value') && !elem.getAttribute('value').includes('@')) {
        tempFormErrors.push(elem.getAttribute('name'));
      }
      if (elem.nodeName.toLowerCase() !== 'button' && !elem.validity.valid) {
        if (tempFormErrors.filter(item => item === elem.getAttribute('name')).length === 0) {
          tempFormErrors.push(elem.getAttribute('name'));
        }
      }

      if (elem.getAttribute('type') === 'hidden') {
        let siblingDiv = elem.previousSibling;
        if (siblingDiv && elem.value.length === 0 &&
          tempFormErrors.filter(item => item === siblingDiv.getAttribute('id')).length === 0) {
          tempFormErrors.push(siblingDiv.getAttribute('id'));
        }
      }
    }
    return tempFormErrors;
  }

  saveSchoolDataV1() {
    if (this.state.formError && this.state.formError.length > 0) {
      // there are form validation errors
      this.setState({
        dialog: true,
        saveStatus: 'These are invalid : ' + this.state.formError.map(item => lodash.startCase(item)),
        severe: 'error',
        isLoading: false
      }, () => {
        console.log('validation errors', this.state.formError)
      });
    } else {
      // form is valid. perform submission
      let tempSchoolData = this.state.schoolData;

      if (tempSchoolData.managedBy === 'Other') {
        tempSchoolData.managedBy = tempSchoolData['otherManaged'];
      }

      let status = tempSchoolData.school_status
      let estDate = tempSchoolData.establishedDate
      console.log('saving');
      saveSchoolV1({
        ...tempSchoolData,
        status: status ? status : 'IN_PROGRESS',
        establishedDate: estDate ? estDate : new Date()
      }).then(response => {
        this.setState({
          schoolId: response.schoolId,
          schoolData: this.formatMangedBy(response),
          isLoading: false
        }, () => {
          this.setDataSavedInGrid('School Basic Data saved successfully.')
        });
      }).catch(error => {
        this.setDataSavedInGrid("Data Could not be saved\nError : " + error.code + "\nDescription : " + error.message, 'error');
      });
    }
  }

  finalSubmit = () => {
    if (this.state.schoolStatus && this.state.schoolStatus !== CREATE_COMPLETED) {
      submitSchool(this.state.schoolId, CREATE_COMPLETED).then(response => {
        if (response) {
          this.setState({
            schoolStatus: CREATE_COMPLETED
          });
          this.setDataSavedInGrid("School Profile Submitted Successfully!")
          setTimeout(() => this.goBack(), 1000)
        }
      }).catch(err => {
        console.log(err);
        this.setDataSavedInGrid("Error : " + err.message, "error");
      });
    }
  };

  setGridLoaded = () => {
    this.setState({isFormLoaded: true});
  };

  setDataSavedInGrid = (message, severe) => {
    if (!severe) {
      severe = "success";
      this.setState({isFormLoaded: false})
      const nextTab = this.state.nextTabId
      this.setState({activeTabId: nextTab})
    } else {
      message = trimErrorMessage(message)
    }
    this.setState({
      isLoading: false,
      dialog: true,
      saveStatus: message ? message : '',
      severe: severe
    }, () => this.setGridLoaded());
  };

  handleSchoolProfileDataChange = (evt) => {
    let tempSchoolData = {...this.state.schoolData}
    let tempFormError = [...this.state.formError]
    if (evt && evt.target && evt.target.name) {
      let name = evt.target.name;
      tempSchoolData[name] = evt.target.value
      if (!evt.target.value || evt.target.value.length === 0) {
        if (tempFormError.filter(it => it === name).length === 0) {
          tempFormError.push(name);
        }
        this.setState({formError: tempFormError});
      } else {
        if (tempFormError.filter(it => it === name).length > 0) {
          this.setState({formError: tempFormError.filter(it => it !== name)});
        }
      }
    } else if (Date.parse(evt)) {
      tempSchoolData['establishedDate'] = getFormattedDate(new Date(evt));
    }
    this.setState({
      schoolData: tempSchoolData
    });
  }

  goBack() {
    window.history.back();
  }

  getSchoolBasicDataView() {
    return (
      <>
        <form autoComplete="nope" ref={form => this.spForm = form}>
          <input type="hidden" value="prayer"/>
          {this.state.schoolData &&
          <Card>
            <CardHeader titleTypographyProps={{align: 'center'}} title={"School Basic Details"}/>
            <CardContent>
              <SchoolBasicDetails schoolData={this.state.schoolData}
                                  handleChange={(evt) => this.handleSchoolProfileDataChange(evt)}
                                  formErrors={this.state.formError}/>
            </CardContent>
            <CardActions style={{justifyContent: 'center'}}>
              {this.state.isLoading ?
                <CircularProgress size={26} style={{marginRight: '3%', marginLeft: '3%'}}/> :
                <Button color="primary" variant="contained"
                        onClick={() => this.validateAndSave()}
                >Save</Button>}
            </CardActions>
          </Card>}
        </form>
      </>
    )
  }

  ignoreHeadersList = ['schoolId', 'sectionId', 'studentDetailsId', 'educationDepartmentId', 'stakeHolderId', 'teacherGroupId', 'schoolClubId', 'updatedAt'];

  ignoreFieldList = ['socialDivision', 'gender'];

  numberFieldList = ['contactDetails', 'contactNumber', 'memberCount', 'formationYear', 'male', 'female', 'teacherCount', 'totalMembers'];

  conditionalRender(activeTabId) {
    switch (activeTabId) {
      case 0:
        return this.getSchoolBasicDataView()
      case 1:
        return (
          <DataTable title={"Education Department"} ref="educationDept" isGridLoaded={this.setGridLoaded}
                     isDataSaved={this.setDataSavedInGrid}
                     numberFields={this.numberFieldList}
                     saveButton={true}
                     loaderFunc={() => {
                       return getEducationDeptDetailsBySchool(this.state.schoolId);
                     }}
                     saveFunc={(data, id) => {
                       console.log("saving edu data");
                       return saveEduDept(data, id);
                     }}
                     deleteFunc={(ids) => {
                       return deleteEduDept(ids);
                     }}
                     resetFunc={() => {
                       return getEducationDeptDetailsBySchool(1);
                     }}
                     ignoreHeaders={this.ignoreHeadersList}
                     schoolId={this.state.schoolId}
                     uniqueId={'educationDepartmentId'}
                     rowId={'educationDepartmentId'}
                     status={this.state.schoolData.status}
          />
        )
      case 2:
        return (<DataTable title={"Stakeholders Details"} ref="stakeHolders" isGridLoaded={this.setGridLoaded}
                           isDataSaved={this.setDataSavedInGrid}
                           numberFields={this.numberFieldList}
                           saveButton={true}
                           loaderFunc={() => {
                             return getStakeholderDetailsBySchool(this.state.schoolId);
                           }}
                           saveFunc={(data, id) => {
                             return saveStakeHolder(data, id);
                           }}
                           deleteFunc={(ids) => {
                             return deleteStakeHolder(ids);
                           }}
                           resetFunc={() => {
                             return getStakeholderDetailsBySchool(1);
                           }}
                           ignoreHeaders={this.ignoreHeadersList}
                           uniqueId={'stakeHolderId'}
                           schoolId={this.state.schoolId}
                           rowId={'stakeHolderId'}
                           status={this.state.schoolData.status}
        />);
      case 3:
        return (<DataTable title={"Class Details"} ref="classDetails" isGridLoaded={this.setGridLoaded}
                           isDataSaved={this.setDataSavedInGrid}
                           numberFields={this.numberFieldList}
                           saveButton={true}
                           loaderFunc={() => {
                             return getClassDetailsBySchool(this.state.schoolId);
                           }}
                           saveFunc={(data, id) => {
                             return saveSchoolClass(data, id);
                           }}
                           deleteFunc={(ids) => {

                             return deleteSchoolClass(ids);
                           }}
                           resetFunc={() => {
                             return getClassDetailsBySchool(1);
                           }}
                           nestedCol={this.ignoreFieldList.concat('standard', 'medium', 'sectionName')}
                           dataField={'studentCount'}
                           nestedData={'studentDetails'}
                           ignoreHeaders={this.ignoreHeadersList.concat(this.ignoreFieldList)}
                           uniqueId={'studentDetailsId'}
                           schoolId={this.state.schoolId}
                           rowId={'sectionId'}
                           status={this.state.schoolData.status}
        />);
      case 4:
        return (<DataTable title={"Teachers Details"} ref="teachersDetails" isGridLoaded={this.setGridLoaded}
                           isDataSaved={this.setDataSavedInGrid}
                           numberFields={this.numberFieldList}
                           saveButton={true}
                           loaderFunc={() => {
                             return getTeacherDetailsBySchool(this.state.schoolId);
                           }}
                           saveFunc={(data, id) => {
                             return saveTeachersDetails(data, id);
                           }}
                           deleteFunc={(ids) => {

                             return deleteTeachersDetails(ids);
                           }}
                           resetFunc={() => {
                             return getTeacherDetailsBySchool(1);
                           }}
                           ignoreHeaders={this.ignoreHeadersList}
                           uniqueId={'teacherGroupId'}
                           rowId={'teacherGroupId'}
                           schoolId={this.state.schoolId}
                           status={this.state.schoolData.status}
        />);
      case 5:
        return (<DataTable title={"School Clubs Details"} ref="schoolClub" isGridLoaded={this.setGridLoaded}
                           isDataSaved={this.setDataSavedInGrid}
                           numberFields={this.numberFieldList}
                           saveButton={true}
                           loaderFunc={() => {
                             return getSchoolClubDetailsBySchool(this.state.schoolId);
                           }}
                           saveFunc={(data, id) => {
                             return saveSchoolClub(data, id);
                           }}
                           deleteFunc={(ids) => {

                             return deleteSchoolClub(ids);
                           }}
                           resetFunc={() => {
                             return getSchoolClubDetailsBySchool(1);
                           }}
                           ignoreHeaders={this.ignoreHeadersList}
                           uniqueId={'schoolClubId'}
                           schoolId={this.state.schoolId}
                           rowId={'schoolClubId'}
                           status={this.state.schoolData.status}
        />);
      case 6:
        return (
          <Map
            apiKey={this.state.schoolData.mapsApiKey}
            google={this.props.google}
            center={{lat: this.state.schoolData.latitude, lng: this.state.schoolData.longitude}}
            setData={async (lat, lang) => {
              let schoolData = this.state.schoolData
              schoolData.latitude = lat
              schoolData.longitude = lang
              await this.setState({schoolData: schoolData})
              this.saveSchoolDataV1()
            }}
            height='800px'
            zoom={15}
          />
        );
      default:
        return (<div/>);
    }
  }

  render() {
    return (
      <>
        <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                  open={this.state.dialog} autoHideDuration={10000} onClose={this.handleDialogClose}>
          <Alert onClose={this.handleDialogClose} severity={this.state.severe}>
            {this.state.saveStatus}
          </Alert>
        </Snackbar>
        {this.state.isFormLoaded ?
          <>
            <Typography align="center"
                        variant={"h1"}>{lodash.startCase(this.state.schoolData.schoolName ? this.state.schoolData.schoolName : 'OnBoard New School')}</Typography>
            {this.state.isLoading ? <LinearProgress/> : <>
              <Button style={{
                float: "right", flexGrow: 1,
                marginLeft: 36,
                marginRight: 12,
              }}
                      color="secondary" variant="contained"
                      onClick={() => this.goBack()}
              >Cancel</Button>
              <Button style={{float: "right"}}
                      color="primary" variant="contained"
                      disabled={!this.state.schoolId}
                      onClick={() => this.finalSubmit()}
              >On Board</Button>
              <StyledTabs
                value={this.state.activeTabId}
                onChange={(e, id) => {
                  const prevId = this.state.activeTabId
                  this.setState({
                    nextTabId: id
                  }, () => this.savePrevTabData(prevId))
                }}
                indicatorColor="primary"
                textColor="primary"
                scrollButtons={"auto"}
                variant={"scrollable"}
              >
                <StyledTab label="School Basic Details"/>
                <StyledTab disabled={!this.state.schoolId} label="Education Department"/>
                <StyledTab disabled={!this.state.schoolId} label="Stakeholders Details"/>
                <StyledTab disabled={!this.state.schoolId} label="Class Details"/>
                <StyledTab disabled={!this.state.schoolId} label="Teachers Details"/>
                <StyledTab disabled={!this.state.schoolId} label="School Club Details"/>
                <StyledTab disabled={!this.state.schoolId} label="School Map Location"/>
              </StyledTabs>
            </>}
            {this.conditionalRender(this.state.activeTabId)}
          </> : <LinearProgress/>
        }
      </>
    )
  }
}

export default SchoolProfile;
