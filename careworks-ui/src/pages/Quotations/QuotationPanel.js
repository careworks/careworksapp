import React, {useEffect, useState} from 'react';
import {Tab, Tabs} from '@material-ui/core';

import Quotation from './components';
import {getAllActionPlans, getAllQuotations} from "../../utils/APIUtils";
import Typography from "@material-ui/core/Typography";
import MuiAlert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import withStyles from "@material-ui/core/styles/withStyles";
import TabContext from "@material-ui/lab/TabContext";
import TabPanel from "@material-ui/lab/TabPanel";
import ConsolidatedQuotations from "./ConsolidatedQuotations";
import Snackbar from "@material-ui/core/Snackbar";
import {trimErrorMessage} from "../../utils/Utils";
import LinearProgress from "@material-ui/core/LinearProgress";

const StyledTabs = withStyles({
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    '& > div': {
      maxWidth: 40,
      width: '100%',
      backgroundColor: '#635ee7',
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{children: <div/>}}/>);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: theme.typography.pxToRem(20),
    marginRight: theme.spacing(1),
    '&:focus': {
      opacity: 1,
    },
  },
}))((props) => <Tab disableRipple {...props} />);

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function QuotationPanel(props) {

  let [actionPlans, setActionPlans] = useState({});
  let [quotations, setQuotations] = useState([]);
  let [error, setError] = useState(null);
  let [isLoading, setIsLoading] = useState(true);
  let [activePlan, setActivePlan] = useState("");
  const [value, setValue] = useState("ViewQuotation");
  let [dialog, setDialog] = useState(false)
  let [severe, setSevere] = useState("success")
  let [saveStatus, setSaveStatus] = useState("success")
  let [change, setChange] = useState([])

  useEffect(() => {
    if (isLoading) {
      getAllActionPlans(props.match.params.school_id).then(response => {
        setActionPlans(response);
        const plans = Object.keys(response)
        if (plans.length > 0)
          setActivePlan(plans[plans.length - 1])
        setIsLoading(false)
      }).catch(error => {
        setError(error);
        setIsLoading(false)
      });
      getAllQuotations(props.match.params.school_id).then(response => setQuotations(response.map(quot => "" + quot.actionId))).catch(error => setError(error));
    }
  }, [])

  function goBack() {
    window.history.back();
  }

  function setDataSavedInGrid(message, severe) {
    if (!severe) {
      severe = "success";
    } else {
      message = trimErrorMessage(message)
    }
    setIsLoading(false)
    setDialog(true)
    setSaveStatus(message ? message : '')
    setSevere(severe)
  }

  return (
    <>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                open={dialog} autoHideDuration={10000} onClose={() => setDialog(false)}>
        <Alert onClose={() => setDialog(false)} severity={severe}>
          {saveStatus}
        </Alert>
      </Snackbar>
      <Typography variant="h3">{error ? 'ERROR ' + error.message + ' Please contact tech team' : null}</Typography>
      {
        actionPlans && Object.keys(actionPlans).length === 0 ?
          <div style={{width: '100%'}}>
            {isLoading ? <LinearProgress/> : <Alert variant="outlined" severity="warning">
              <span style={{fontSize: 'large'}}> Action Plan for School - <b> {props.match.params.school_Name} </b> is not approved yet! </span>
            </Alert>}
            <br/>
            <Button variant="contained" color="secondary" onClick={() => {
              goBack()
            }}>Back</Button>
          </div> : <>
            <Typography align="center"
                        variant={'h1'}>{props.match.params.school_Name + " Quotations"}</Typography>
            <br/>
            <Button variant="contained" color="secondary" style={{
              float: "right",
              marginRight: 30
            }} onClick={() => {
              goBack()
            }}>Cancel</Button>
            <FormControl required autoComplete="nope" style={{
              minWidth: 120,
              maxWidth: 300,
              height: "60px",
              float: "right",
              marginRight: 30,
            }}>Financial Year :
              <Select label={"Action Plan"}
                      SelectDisplayProps={{style: {fontSize: '25px'}}}
                      value={activePlan}
                      onChange={(evt) => setActivePlan(evt.target.value)}>
                {Object.keys(actionPlans).map(planName => (
                  <MenuItem key={planName} value={planName}>{planName.split(',')[0]}</MenuItem>
                ))}
              </Select>
            </FormControl>
            {activePlan && <TabContext value={value}>
              {activePlan && isLoading ?
                <LinearProgress/> :
                <StyledTabs
                  value={value}
                  onChange={(e, value) => {
                    if (Array.isArray(change) && change.length === 0)
                      setValue(value)
                    else if (Array.isArray(change))
                      setDataSavedInGrid(change.join(',') + " quotation is not saved, please save or cancel or refresh page to discard changes", 'error')
                  }}
                  indicatorColor="primary"
                  textColor="primary"
                  scrollButtons={"auto"}
                  variant={"scrollable"}
                >
                  <StyledTab label={"Summary"} value={"ViewQuotation"}/>
                  {
                    Object.keys(actionPlans[activePlan]).map(category =>
                      <StyledTab label={category} value={category}/>
                    )
                  }
                </StyledTabs>
              }
              {
                Object.keys(actionPlans[activePlan]).map(category =>

                  <TabPanel value={category}>
                    <Quotation title={category} plan={actionPlans[activePlan][category]}
                               schoolId={props.match.params.school_id}
                               quotations={quotations}
                               setDataSavedInGrid={setDataSavedInGrid}
                               isLoading={isLoading}
                               setIsLoading={setIsLoading}
                               change={change}
                               setChange={setChange}
                    />
                  </TabPanel>
                )
              }
              <TabPanel value={"ViewQuotation"}>
                <ConsolidatedQuotations school_id={props.match.params.school_id}
                                        school_Name={props.match.params.school_Name}
                                        plan_id={activePlan.split(',')[1]}
                                        setDataSavedInGrid={setDataSavedInGrid}
                                        isLoading={isLoading}
                                        setIsLoading={setIsLoading}/>
              </TabPanel>
            </TabContext>}
          </>
      }
    </>
  );

}
