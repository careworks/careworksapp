import React from 'react';
import {Button, CircularProgress, TextField, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {deleteQuotation, getQuotations, saveQuotation} from "../../../../utils/APIUtils";
import DataTable from "../../../AdminPanel/components/DataTable";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";

class Quotation extends React.Component {

  ignoreHeadersList = ["actionId", "quotId", "schoolId", "companyName", "updatedAt", "status"];

  constructor(props) {
    super(props);
    this.state = {
      dialog: false,
      saveStatus: null,
      createCompanyName: null,
      quotations: props.quotations
    }
  }

  setGridLoaded = () => {
    this.setState({isFormLoaded: true});
  };

  addNewQuotation(actionId) {
    console.log("creating quotation for ", this.state.createCompanyName)
    saveQuotation([{
      actionId: actionId,
      companyName: this.state.createCompanyName,
      schoolId: this.props.schoolId,
      status: "created"
    }]).then(response => {

      this.props.setDataSavedInGrid("Created Successfully, please refresh the page if table does not appear")
      let tempQuotations = this.state.quotations;
      tempQuotations.push("" + actionId);
      this.setState({quotations: tempQuotations})
      this.setState({createCompanyName: null})
    }).catch(error => {
      this.props.setDataSavedInGrid("Error : " + error.message, 'error')
    })

  }

  handleDialogClose = () => {
    this.setState(
      {
        dialog: false
      }
    )
  };

  numberFields = ['number', 'length', 'breadth', 'depth', 'quantity', 'rate', 'amount']

  render() {
    return (
      <>
        <Grid container spacing={2}>
          {
            this.props.plan.map(action => (
              <Grid item xs={12}>
                <Card raised>
                  <CardHeader titleTypographyProps={{align: 'center'}} title={action['Activity']}/>
                  <CardContent>
                    <Grid container spacing={2}>
                      {this.state.quotations.includes("" + action['actionId']) || this.props.quotations.includes("" + action['actionId']) ? <>
                        <Grid item xs={12} lg={12}
                              xl={12}>
                          <DataTable title={null}
                                     ref={"quotations" + action['actionId']}
                                     numberFields={this.numberFields}
                                     saveButton={true}
                                     isGridLoaded={this.setGridLoaded}
                                     isDataSaved={this.props.setDataSavedInGrid}
                                     loaderFunc={() => {
                                       console.log("loading data")
                                       return getQuotations(action['actionId']);
                                     }}
                                     saveFunc={(data, id, name) => {
                                       this.props.setIsLoading(true)
                                       data.forEach(row => {
                                         row['actionId'] = action['actionId']
                                         row['schoolId'] = id;
                                         row['companyName'] = name;
                                         row['status'] = "in_progress";
                                       })
                                       return saveQuotation(data);
                                     }}
                                     deleteFunc={(ids) => {
                                       console.log("deleting data");
                                       return deleteQuotation(ids);
                                     }}
                                     resetFunc={() => {
                                       return getQuotations(action['actionId']);
                                     }}
                                     ignoreHeaders={this.ignoreHeadersList}
                                     uniqueId={'quotId'}
                                     schoolId={this.props.schoolId}
                                     rowId={'quotId'}
                                     titleColumn={'companyName'}
                                     change={this.props.change}
                                     setChange={this.props.setChange}
                          />
                        </Grid>
                      </> : <><Grid item xs={6}>
                        <TextField label="Company Name" fullWidth required
                                   onChange={event => this.setState({createCompanyName: event.target.value})}/>
                      </Grid>
                        <Grid item>
                          {
                            this.props.isLoading ?
                              <CircularProgress size={26} style={{
                                marginRight: '3%',
                                marginLeft: '3%'
                              }}/> :
                              <Button color="primary" variant="contained"
                                      onClick={() => {
                                        this.props.setIsLoading(true)
                                        this.addNewQuotation(action['actionId'])
                                      }}>Create New Quotation</Button>
                          }
                        </Grid></>}
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            ))
          }
        </Grid>
      </>
    );

  }
}

export default Quotation;
