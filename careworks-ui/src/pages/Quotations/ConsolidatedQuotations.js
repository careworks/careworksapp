import React, {useEffect, useState} from "react";
import {getConsolidatedQuots, statusUpdateQuotation} from "../../utils/APIUtils";
import {CircularProgress, MuiThemeProvider} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import MUIDataTable from "mui-datatables";
import lodash from 'lodash';
import Button from "@material-ui/core/Button";
import moment from "moment";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import classnames from 'classnames';
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  BusinessAnalystRow: {
    '& td': {fontWeight: "bold"}
  },
  NameCell: {
    fontWeight: 900
  }
}));

export default function ConsolidatedQuotations(props) {

  const customStyles = useStyles();

  let [quots, setQuots] = useState([])
  let [error, setError] = useState(null)
  let [loading, setLoading] = useState(true)
  let [status, setStatus] = useState("Not Started")

  useEffect(() => {
    getConsolidatedQuots(props.school_id, props.plan_id).then(response => {
      setError(null)
      setQuots(response)
      if (Array.isArray(response) && response.length > 0) {
        setStatus(lodash.startCase(response[response.length - 1]['status']) + " and Last Updated : " + moment(response[response.length - 1]['updatedAt']).format('DD-MMM-YYYY hh:mm A'))
      }
      setLoading(false)
    }).catch(error => {
      setError(error);
      setLoading(false)
    });
  }, [loading])

  function submitQuotation(actionId) {
    setLoading(true)
    statusUpdateQuotation(actionId, 'submitted').then(response => {
      setLoading(false)
      props.setDataSavedInGrid("Submitted Successfully")
    }).catch(error => {
      setLoading(false)
      props.setDataSavedInGrid("Error : " + error.message, 'error')
    })
  }

  function approveQuotation(actionId) {
    setLoading(true)
    statusUpdateQuotation(actionId, 'approved').then(response => {
      setLoading(false)
      props.setDataSavedInGrid("Approved Successfully")
    }).catch(error => {
      setLoading(false)
      props.setDataSavedInGrid("Error : " + error.message, 'error')
    })

  }

  function rejectQuotation(actionId) {
    setLoading(true)
    statusUpdateQuotation(actionId, 'rejected').then(response => {
      setLoading(false)
      props.setDataSavedInGrid("Rejected Successfully")
    }).catch(error => {
      setLoading(false)
      props.setDataSavedInGrid("Error : " + error.message, 'error')
    })

  }

  const getTableTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableHeadCell: {
          root: {
            border: 'solid 1px #000',
            backgroundColor: 'white',
            textAlign: 'justify',
            fontWeight: 'bold',
          }
        },
        MUIDataTableBodyCell: {
          root: {
            border: 'solid 1px #000',
            backgroundColor: 'white',
            textAlign: 'justify'
          }
        }
      }
    });


  return (
    <>
      <Typography variant="h3">{error ? 'ERROR ' + error.message + ' Please contact tech team' : null}</Typography>
      {loading ?
        <CircularProgress size="5rem" style={{marginLeft: '40%', marginBottom: '20%', marginTop: '10%'}}/> :
        <>
          {
            status === "Not Started" ? <Typography variant="h3">No Quotations</Typography> :
              <>
                <>
                  <br/>
                  {sessionStorage.getItem('role').includes('ADMIN') && <>
                    <Button color="primary" variant="contained"
                            style={{
                              float: "right",
                              marginRight: 30
                            }}
                            onClick={() => {
                              rejectQuotation(props.plan_id)
                            }}>Reject</Button>
                    <Button color="primary" variant="contained"
                            style={{
                              float: "right",
                              marginLeft: 30,
                              marginRight: 30,
                            }}
                            onClick={() => {
                              approveQuotation(props.plan_id)
                            }}>Approve</Button>
                  </>}
                  <Button color="primary" variant="contained"
                          style={{
                            float: "right",
                          }}
                          onClick={() => {
                            submitQuotation(props.plan_id)
                          }}>Submit</Button>
                  <Typography variant={'subtitle1'}>{" Status  : " + status}</Typography>
                  <br/>
                  <br/>
                </>
                <MuiThemeProvider theme={getTableTheme()}>
                <MUIDataTable
                  data={quots.map(value => Object.values(value).splice(3, 11))}
                  columns={Object.keys(quots[0]).map(value => lodash.startCase(value)).splice(3, 11)}
                  options={{
                    selectableRows: false,
                    responsive: "scrollFullHeight",
                    filter: false,
                    rowsPerPage: 100,
                    setRowProps: (row) => {
                      return {
                        className: classnames(
                          {
                            [customStyles.BusinessAnalystRow]: row[0] === null
                          })
                      };
                    },
                    setTableProps: () => {
                      return {
                        padding: 'default',
                        size: 'small'
                      }
                    }
                  }}

                />
                </MuiThemeProvider>
              </>
          }
        </>
      }
    </>
  );

}
