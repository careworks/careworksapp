import axios from 'axios';
import fileSaver from 'file-saver';

const API_BASE_URL = '/api';
const ACCESS_TOKEN = 'id_token';

const request = (options) => {
  const headers = new Headers({
    'Content-Type': 'application/json',
  });

  if (sessionStorage.getItem(ACCESS_TOKEN)) {
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem(ACCESS_TOKEN))
  }

  const defaults = {headers: headers};
  options = Object.assign({}, defaults, options);

  try {
    return fetch(options.url, options)
      .then(response =>
        response.json().then(json => {
          if (!response.ok) {
            return Promise.reject(json);
          }
          return json;
        })
      ).catch(error => {
        console.error("Error Response",error);
        return Promise.reject(error);
      });
  } catch (e) {
    return Promise.reject(e);
  }
};

const fileUploadRequest = (options) => {
  const headers = new Headers({
    'Content-Type': 'multipart/form-data',
  });

  if (sessionStorage.getItem(ACCESS_TOKEN)) {
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem(ACCESS_TOKEN))
  }

  const defaults = {headers: headers};
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options)
    .then(response =>
      response.json().then(json => {
        if (!response.ok) {
          return Promise.reject(json);
        }
        return json;
      })
    ).catch(err => {
        console.error('Error: ', err);
        return Promise.reject(err);
      }
    );
};

export function login(loginRequest) {
  return request({
    url: API_BASE_URL + '/auth/signin',
    method: 'POST',
    body: JSON.stringify(loginRequest)
  });
}

export function signup(signupRequest) {
  return request({
    url: API_BASE_URL + '/auth/signup',
    method: 'POST',
    body: JSON.stringify(signupRequest)
  });
}

export function forgotPassword(username) {
  return request({
    url: API_BASE_URL + '/auth/forgotPassword',
    method: 'POST',
    body: JSON.stringify({login: username})
  });
}

export function updateNewPassword(otp,loginValue,newPassword) {
  return request({
    url: API_BASE_URL + '/auth/updatePassword?otp=' + otp,
    method: 'POST',
    body: JSON.stringify({
      login: loginValue,
      password: newPassword
    })
  });
}

export function checkEmailAvailability(email) {
  return request({
    url: API_BASE_URL + '/user/checkEmailAvailability?email=' + email,
    method: 'GET'
  });
}

export function getAllUsers() {
  return request({
    url: API_BASE_URL + '/user/all',
    method: 'GET'
  });
}

export function roleUpgrade(users, role) {
  return request({
    url: API_BASE_URL + '/users/approve/' + role,
    method: 'POST',
    body: JSON.stringify(users)
  });
}

export function approvePassword(userId) {
  return request({
    url: API_BASE_URL + '/auth/approvePassword/' + userId,
    method: 'POST'
  });
}

export function getAllSchoolMedium() {
  return request({
    url: API_BASE_URL + '/admin/medium',
    method: 'GET'
  });
}

export function getAllSchoolType() {
  return request({
    url: API_BASE_URL + '/admin/type',
    method: 'GET'
  });
}

export function getAllSchoolLocation() {
  return request({
    url: API_BASE_URL + '/admin/location',
    method: 'GET'
  });
}

export function getAllSchoolCategory() {
  return request({
    url: API_BASE_URL + '/admin/category',
    method: 'GET'
  });
}

export function getAllQuestions() {
  return request({
    url: API_BASE_URL + '/admin/questions',
    method: 'GET'
  });
}

export function saveInfo(info,action,type) {
  return request({
    url: API_BASE_URL + '/admin/'+type+'/'+action,
    method: 'POST',
    body: JSON.stringify(info)
  });
}

export function getAllSchoolStatus() {
  return request({
    url: API_BASE_URL + '/school/getAllStatus',
    method: 'GET'
  });
}

//CQ stuff
export function getAllActionPlans(schoolid) {
  return request({
    url: API_BASE_URL + '/quotation/getActionPlans/'+schoolid,
    method: 'GET'
  });
}

export function getConsolidatedQuots(schoolid,planId) {
  return request({
    url: API_BASE_URL + '/quotation/getConsQuotationBySchoolId/'+schoolid+'/'+planId,
    method: 'GET'
  });
}

export function getAllQuotations(schoolid) {
  return request({
    url: API_BASE_URL + '/quotation/getQuotationBySchoolId/'+schoolid,
    method: 'GET'
  });
}

export function getQuotations(actionId) {
  return request({
    url: API_BASE_URL + '/quotation/getQuotationByActionId/'+actionId,
    method: 'GET'
  });
}

export function saveQuotation(quotations) {
  return request({
    url: API_BASE_URL + '/quotation/saveQuotation',
    method: 'POST',
    body: JSON.stringify(quotations)
  });
}

export function statusUpdateQuotation(actionId,status) {
  return request({
    url: API_BASE_URL + '/quotation/updateQuotation/'+status+'/'+actionId,
    method: 'POST'
  });
}

export function deleteQuotation(quotations) {
  return request({
    url: API_BASE_URL + '/quotation/deleteQuotations',
    method: 'DELETE',
    body: JSON.stringify(quotations)
  });
}

//Action Plan V1 Stuff

export function getActionPlansBySchool(schoolId) {
  return request({
    url: API_BASE_URL + '/actionPlanV1/plans/'+schoolId,
    method: 'GET'
  });
}

export function getAllCategories() {
  return request({
    url: API_BASE_URL + '/actionPlanV1/categories',
    method: 'GET'
  });
}

export function getActivitiesByCategory(category) {
  return request({
    url: API_BASE_URL + '/actionPlanV1/activities/'+category,
    method: 'GET'
  });
}

export function getActionPlanById(actionId) {
  return request({
    url: API_BASE_URL + '/actionPlanV1/plan/'+actionId,
    method: 'GET'
  });
}

export function saveActionPlan(actionPLan) {
  return request({
    url: API_BASE_URL + '/actionPlanV1/plans/save',
    method: 'POST',
    body: JSON.stringify(actionPLan)
  });
}

export function deleteActionPlan(actionId) {
  return request({
    url: API_BASE_URL + '/actionPlanV1/plan/'+actionId,
    method: 'DELETE'
  });
}

export function getAllActivities() {
  return request({
    url: API_BASE_URL + '/actionPlanV1/activities',
    method: 'GET'
  });
}

export function getAllProfiles() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const headers = {
    headers: {
      'Authorization': 'Bearer ' + token
    },
    params: {
      completed: ''
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/SchoolProfiles/', headers).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
    return Promise.reject(e);
  }
  // return request({
  //     url: API_BASE_URL + "/school/SchoolProfiles/",
  //     method: 'GET'
  // });
}

export function getSchoolProfileForm() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const headers = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/SchoolProfiles/fields/', headers).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getFullProfile(id) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const headers = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/SchoolProfiles/' + id, headers).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
  // return request({
  //     url: API_BASE_URL + "/school/SchoolProfiles/",
  //     method: 'GET'
  // });
}


export function getCurrentUser() {
  if (!sessionStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/user/me',
    method: 'GET'
  });
}

export function getUserProfile(username) {
  return request({
    url: API_BASE_URL + '/users/' + username,
    method: 'GET'
  });
}

export function getSchoolByName(schoolName) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    },
    params: {
      name: schoolName
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/SchoolProfilesByName/', queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function saveSchool(schoolDetails) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/SchoolProfiles/',
    method: 'POST',
    body: JSON.stringify(schoolDetails)
  };

  return request(opt);
}

export function saveEduDept(eduDept, schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/educationDepartments/All/' + schoolId,
    method: 'POST',
    body: JSON.stringify(eduDept)
  };

  return request(opt);
}

export function saveTeachersDetails(teachersDetails, schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/teacherGroups/' + schoolId,
    method: 'POST',
    body: JSON.stringify(teachersDetails)
  };

  return request(opt);
}

export function saveStakeHolder(stakeHolders, schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/stakeHolders/' + schoolId,
    method: 'POST',
    body: JSON.stringify(stakeHolders)
  };

  return request(opt);
}

export function saveSchoolClub(clubs, schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/clubs/' + schoolId,
    method: 'POST',
    body: JSON.stringify(clubs)
  };

  return request(opt);
}

export function saveSchoolClass(classDetails, schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/ClassDetails/All/' + schoolId,
    method: 'POST',
    body: JSON.stringify(classDetails)
  };

  return request(opt);
}

export function deleteSchoolClass(Ids) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/ClassDetails/',
    method: 'DELETE',
    body: JSON.stringify(Ids)
  };

  return request(opt);
}

export function deleteStakeHolder(Ids) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/stakeHolders/',
    method: 'DELETE',
    body: JSON.stringify(Ids)
  };

  return request(opt);
}

export function deleteSchoolClub(Ids) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/clubs/',
    method: 'DELETE',
    body: JSON.stringify(Ids)
  };

  return request(opt);
}

export function deleteTeachersDetails(Ids) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/teacherGroups/',
    method: 'DELETE',
    body: JSON.stringify(Ids)
  };

  return request(opt);
}

export function deleteEduDept(Ids) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/educationDepartments/',
    method: 'DELETE',
    body: JSON.stringify(Ids)
  };

  return request(opt);
}

export function getEducationDeptDetailsBySchool(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/educationDepartments/All/' + schoolId, queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getClassDetailsBySchool(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/ClassDetails/All/' + schoolId, queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getTeacherDetailsBySchool(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/teacherGroups/' + schoolId, queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getStakeholderDetailsBySchool(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/stakeHolders/' + schoolId, queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error encounterd in school name search');
  }
}

export function getSchoolClubDetailsBySchool(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/clubs/' + schoolId, queryParam).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error(e);
  }
}

export function getFormFields(category) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/formfields/getAllFieldsByCategory?category=' + category,
    method: 'GET'
  });
}


export function getNeedAssessmentSavedData(schoolId, category) {

  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/auth/getNeedAssessmentResponse/' + schoolId + '/' + category,
    method: 'GET'
  });
}

export function saveNeedAssessment(needAssessmentResp, schoolId, category) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/auth/saveNeedAssessment/' + schoolId + '/' + category,
    method: 'POST',
    body: JSON.stringify(needAssessmentResp)
  });
}

export function approveNeedAssessment(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/auth/naApproved/' + schoolId,
    method: 'POST'
  });
}

export function rejectNeedAssessment(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/auth/naRejected/' + schoolId,
    method: 'POST'
  });
}

export function submitNeedAssessment(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + '/auth/submitNA/' + schoolId,
    method: 'POST'
  });
}


export function getActions() {
  /*const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + `/actionplan/getActivities`,
    method: 'GET'
  });*/
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.get(API_BASE_URL + '/actionplan/getActivities', {headers: {'Authorization': 'Bearer ' + token}}).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getCategories() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.get(API_BASE_URL + '/actionplan/getCategories', {headers: {'Authorization': 'Bearer ' + token}}).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}


export function getActionPlanData(actionPlanId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + `/actionplan/getActionPlanDetails/${actionPlanId}`,
    method: 'GET'
  });
}

export function getActionPlansBySchoolId(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + `/actionplan/getActionPlansBySchool/${schoolId}`,
    method: 'GET'
  });
}


export function submitActionFields(formData) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + `/actionplan/saveActionPlan`,
    method: 'POST',
    body: JSON.stringify(formData)
  });
}

export function approveOrRejectActionPlan(formData) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }



  return request({
    url: API_BASE_URL + `/actionplan/updateActionPlanStatus`,
    method: 'POST',
    body: JSON.stringify(formData)
  });
}


export function getActionFields(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  return request({
    url: API_BASE_URL + `actionplan/getActionPlanDetails/${schoolId}`,
    method: 'GET'
  });
}

export function getAllSchoolProfileBasic() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  /*const headers = {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    };*/

  const queryParam = {
    headers: {
      'Authorization': 'Bearer ' + token
    },
    params: {
      completed: 'Y'
    }
  };

  try {
    return axios.get(API_BASE_URL + '/school/SchoolProfiles/', queryParam).then(response => {
      //
      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error while fetching school profiles');
  }
}

export function uploadSchoolEnv(file, schoolId) {

  const config = {
    headers: {
      'content-type': 'multipart/form-data'
    }
  };

  const formData = new FormData();
  formData.append('file', file);

  return fileUploadRequest({
    url: API_BASE_URL + '/file/upload/' + schoolId,
    method: 'POST',
    body: formData,
    config: config
  });
}

export function validateToken() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.post(API_BASE_URL + '/auth/validatetoken', token).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error encounterd in school name search');
  }
}

export function submitSchool(schoolId, schoolStatus) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/school/finalsubmit/' + schoolId + '/' + schoolStatus,
    method: 'POST'
  };

  return request(opt);
}

export function getSchoolStatus(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  const opt = {
    url: API_BASE_URL + '/status/getSchoolStatus/' + schoolId,
    method: 'GET'
  };

  return request(opt);
}

export function getSchoolDetailsForId(id) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  const headers = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  };
  try {
    return axios.get(API_BASE_URL + '/school/' + id, headers).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function saveSchoolV1(schoolDetails) {
    const token = sessionStorage.getItem('id_token');
    if (!token) {
      return Promise.reject('No access token set.');
    }

    const opt = {
      url: API_BASE_URL + '/school/saveschooldata/',
      method: 'POST',
      body: JSON.stringify(schoolDetails)
    };

    return request(opt);
}

export function saveSchoolV1old(schoolDetails) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }

  try{
    return axios.post(API_BASE_URL + '/school/saveschooldata/', schoolDetails, {headers: {'Authorization': 'Bearer ' + token}}).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getDefaultEducationDeptData() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.get(API_BASE_URL + '/school/getdefaulteducationdeptdata', {headers: {'Authorization': 'Bearer ' + token}}).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function getAllCodes() {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.get(API_BASE_URL + '/admin/getAllCodes', {headers: {'Authorization': 'Bearer ' + token}}).then(response => {

      if (response.status === 200) {
        return response.data;
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}

export function exportNaBySchoolId(schoolId) {
  const token = sessionStorage.getItem('id_token');
  if (!token) {
    return Promise.reject('No access token set.');
  }
  try {
    return axios.get(API_BASE_URL + '/file/export/na/' + schoolId,
            {headers: {'Authorization': 'Bearer ' + token} , responseType: 'arraybuffer'}).then(response => {

      if (response.status === 200) {
        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        return fileSaver.saveAs(blob, 'NeedAssessmentResponse.xlsx');
      } else {
        return Promise.reject(response);
      }
    });
  } catch (e) {
    console.error('Error : ', e);
  }
}
