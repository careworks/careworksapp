import moment from "moment";

export function getFormattedDate(date) {
  return moment(date).format("YYYY-MM-DD");
}

export function validateField(value, validateType){
    switch(validateType){
        case 'email':
            return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(value);
        default:
            return true;
    }
}

export function trimErrorMessage(message) {
  if (!message){
    return message
  }
  let messages = message.split('\'');
  let err_message = messages[1];
  if (!err_message) {
    messages = message.split('"');
    err_message = messages[1];
  }
  if (!err_message) {
    err_message = message;
  }
  return err_message.replace('An unexpected internal server error occured', '')
}
