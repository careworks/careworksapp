import React, {useEffect, useState} from "react";
import {AppBar, IconButton, Menu, TextField, Toolbar} from "@material-ui/core";
import {Autocomplete} from '@material-ui/lab';
import lodash from 'lodash'
import {
  ArrowBack as ArrowBackIcon,
  Home as HomeIcon,
  Menu as MenuIcon,
  Person as AccountIcon,
  Search as SearchIcon
} from "@material-ui/icons";
import classNames from "classnames";
// styles
import useStyles from "./styles";
// components
import {Typography} from "../Wrappers";
// context
import {toggleSidebar, useLayoutDispatch, useLayoutState,} from "../../context/LayoutContext";
import {signOut, useUserDispatch} from "../../context/UserContext";
import {getAllProfiles, getCurrentUser} from "../../utils/APIUtils";
import {withRouter} from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useTheme} from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Dialog from "@material-ui/core/Dialog";

function Header(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();
  var layoutDispatch = useLayoutDispatch();
  var userDispatch = useUserDispatch();

  // local
  var [profileMenu, setProfileMenu] = useState(null);
  var [isSearchOpen, setSearchOpen] = useState(false);
  let [userDetails, setUserDetails] = useState({});
  let [open, setOpen] = useState(false);
  let [options, setOptions] = useState([]);
  const [loading,setLoading] = useState(true)

  let [dialog,setDialog] = useState(false)

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  function handleClose() {
    setDialog(false)
  }

  useEffect(() => {
    if (!userDetails.name) {
      getCurrentUser().then(value => setUserDetails(value));
      getAllProfiles().then(response => {
        setLoading(false)
        setOptions(response.map(item => {
          return {id: item.school_id, name: item.school_Name};
        }));
      }).catch(error => {
        setLoading(false)
        console.error(error)
      })
    }
  });

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Dialog
          fullScreen={fullScreen}
          open={dialog}
          onClose={handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Contributors"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              JPMorgan Chase & Co. Force For Good 2020 Team
              <List dense>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:kunal@gmail.com'}>Kunal Kejriwal</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:banani@gmail.com'}>Banani Das</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:pavankeshav2661997@gmail.com'}>Pavan Keshav L</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:arijit@gmail.com'}>Arijit Phukan</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:krishna@gmail.com'}>Krishna Singh</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:yoghessh@gmail.com'}>Yoghessh T D</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:anisha@gmail.com'}>Anisha Basak</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:rajeev@gmail.com'}>Rajeev Tapadar</Typography>
                </ListItem>
                <ListItem>
                  <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:surbhi@gmail.com'}>Surbhi Jain</Typography>
                </ListItem>
              </List>
            </DialogContentText>
          </DialogContent>
        </Dialog>
        {sessionStorage.getItem('role').includes('ADMIN') && <IconButton
          color="inherit"
          onClick={() => toggleSidebar(layoutDispatch)}
          className={classNames(
            classes.headerMenuButton,
            classes.headerMenuButtonCollapse,
          )}
        >
          {layoutState.isSidebarOpened ? (
            <ArrowBackIcon
              classes={{
                root: classNames(
                  classes.headerIcon,
                  classes.headerIconCollapse,
                ),
              }}
            />
          ) : (
            <MenuIcon
              classes={{
                root: classNames(
                  classes.headerIcon,
                  classes.headerIconCollapse,
                ),
              }}
            />
          )}
        </IconButton>}
        <IconButton onClick={() => props.history.push('/app/schools')}>
          <HomeIcon
            classes={{
              root: classNames(
                classes.headerIcon,
                classes.headerIconCollapse,
              ),
            }}
          />
        </IconButton>
        <Typography variant="h6" weight="medium" className={classes.logotype} onClick={() => setDialog(true)}>
          Careworks Foundation
        </Typography>
        <div className={classes.grow}/>
        <div
          className={classNames(classes.search, {
            [classes.searchFocused]: isSearchOpen,
          })}
        >
          <div
            className={classNames(classes.searchIcon, {
              [classes.searchIconOpened]: isSearchOpen,
            })}
            onClick={() => setSearchOpen(!isSearchOpen)}
          >
            <SearchIcon classes={{root: classes.headerIcon}}/>
          </div>
          <form onSubmit={props.onSubmit}>
            <Autocomplete id="autoSuggest"
                          open={open} onOpen={() => {
              setOpen(true);
            }}
                          onClose={() => {
                            setOpen(false);
                          }}
                          getOptionSelected={(option, value) => option.name === value.name}
                          getOptionLabel={option => option.name}
                          options={options}
                          loading={loading}
                          noOptionsText="School not Found. Add School Profile"
                          onChange={(evt, val) => {
                            if (val) {
                              props.history.push('/app/school/' + val.id);
                            }
                          }}
                          aria-haspopup={"false"}
                          classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                          }}
                          placeholder="Search School…"
                          renderInput={params => (
                            <TextField {...params} fullWidth
                                       InputProps={{
                                         disableUnderline: true,
                                         ...params.InputProps,
                                         endAdornment: (
                                           <React.Fragment>
                                             {loading ? <CircularProgress color="inherit" size={20}/> : null}
                                             {params.InputProps.endAdornment}
                                           </React.Fragment>
                                         ),
                                       }}
                            />
                          )}
            />
          </form>
        </div>
        <IconButton
          aria-haspopup="true"
          color="inherit"
          className={classes.headerMenuButton}
          aria-controls="profile-menu"
          onClick={e => setProfileMenu(e.currentTarget)}
        >
          <AccountIcon classes={{root: classes.headerIcon}}/>
        </IconButton>
        <Menu
          id="profile-menu"
          open={Boolean(profileMenu)}
          anchorEl={profileMenu}
          onClose={() => setProfileMenu(null)}
          className={classes.headerMenu}
          classes={{paper: classes.profileMenu}}
          disableAutoFocusItem
        >
          <div className={classes.profileMenuUser}>
            <Typography variant="h3" weight="medium">
              {lodash.startCase(userDetails.name)}
            </Typography>
            <Typography variant="h5" weight="medium">
              {userDetails.role ? lodash.startCase(userDetails.role.toLowerCase().split('_')[1]) : null}
            </Typography>
            <Typography
              className={classes.profileMenuLink}
              component="a"
              color="primary"
            >
              {userDetails.email}
            </Typography>
            <Typography
              className={classes.profileMenuLink}
              color="primary"
              onClick={() => signOut(userDispatch, props.history)}
            >
              Sign Out
              <IconButton>
                <ExitToAppIcon/>
              </IconButton>
            </Typography>
          </div>
        </Menu>
      </Toolbar>
    </AppBar>
  );
}

export default withRouter(Header);
