import {AppBar, Toolbar} from "@material-ui/core";
import React, {useState} from "react";
import useStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {useTheme} from '@material-ui/core/styles';
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

export default function Footer(props) {
  const [dialog, setDialog] = useState(false);
  const classes = useStyles();

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  function handleClose() {
    setDialog(false)
  }

  return (
    <AppBar position="static" className={classes.footBar}>
      <Dialog
        fullScreen={fullScreen}
        open={dialog}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{"JPMorgan Chase & Co. Force For Good 2020 Team"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
          <List dense>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:kunal@gmail.com'}>Kunal Kejriwal</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:banani@gmail.com'}>Banani Das</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:pavankeshav2661997@gmail.com'}>Pavan Keshav L</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:arijit@gmail.com'}>Arijit Phukan</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:krishna@gmail.com'}>Krishna Singh</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:yoghessh@gmail.com'}>Yoghessh T D</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:anisha@gmail.com'}>Anisha Basak</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:rajeev@gmail.com'}>Rajeev Tapadar</Typography>
            </ListItem>
            <ListItem>
              <Typography className={classes.profileMenuLink} component={'a'} href={'mailto:surbhi@gmail.com'}>Surbhi Jain</Typography>
            </ListItem>
          </List>
          </DialogContentText>
        </DialogContent>
      </Dialog>
      <Toolbar variant="dense" className={classes.toolbar}>
        <Grid container spacing={4}>
          <Grid item xs={12}><Typography align="center" onClick={() => setDialog(true)}
                                         className={classes.profileMenuLink}>© 2020 Careworks Foundation
            Contributors</Typography></Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}
