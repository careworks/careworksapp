import React from "react";
import {Route, Switch, withRouter,} from "react-router-dom";
import classnames from "classnames";
// styles
import useStyles from "./styles";
// components
import Header from "../Header";
// pages
// context
import {useLayoutState} from "../../context/LayoutContext";
import SchoolLanding from "../../pages/SchoolLanding";
import AdminPanel from "../../pages/AdminPanel";
import {AssesmentTabs} from "../../pages/NeedAssesment";
import ActionPlan from "../../pages/ActionPlan";
import SchoolProfile from "../../pages/SchoolProfile";
import Sidebar from "../Sidebar";
import QuotationPanel from "../../pages/Quotations";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <>
        <Header history={props.history}/>
        <Sidebar/>
        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened,
          })}
        >
          <div className={classes.fakeToolbar}/>
          <Switch>
            <Route path="/app/schools" component={SchoolLanding}/>
            <Route path="/app/admin" component={AdminPanel}/>
            <Route path="/app/quotations/:school_id?/:school_Name?" component={QuotationPanel}/>
            <Route path="/app/school/:schoolId?" component={SchoolProfile}/>
            <Route path="/app/actionplan/:school_id?/:school_Name?/:na_Flag?" component={ActionPlan}/>
            <Route path="/app/assessment/:school_id?/:na_Flag?/:school_Name/:schoolStatus?" component={AssesmentTabs}/>
          </Switch>
        </div>
      </>
    </div>
  );
}

export default withRouter(Layout);
